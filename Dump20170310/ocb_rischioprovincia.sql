-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ocb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rischioprovincia`
--

DROP TABLE IF EXISTS `rischioprovincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rischioprovincia` (
  `id_rischioprovincia` int(11) NOT NULL AUTO_INCREMENT,
  `provincia` varchar(100) DEFAULT NULL,
  `siglaProvincia` varchar(45) DEFAULT NULL,
  `rischio` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_rischioprovincia`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rischioprovincia`
--

LOCK TABLES `rischioprovincia` WRITE;
/*!40000 ALTER TABLE `rischioprovincia` DISABLE KEYS */;
INSERT INTO `rischioprovincia` VALUES (1,'Agrigento','AG',2),(2,'Alessandria','AL',5),(3,'Ancona','AN',5),(4,'Aosta','AO',3),(5,'Arezzo','AO',5),(6,'Ascoli Piceno','AP',6),(7,'Asti','AT',3),(8,'Avellino','AV',2),(9,'Bari','BA',5),(10,'Barletta-Andria-Trani','BT',3),(11,'Belluno','BL',3),(12,'Benevento','BN',2),(13,'Bergamo','BG',4),(14,'Biella','BI',3),(15,'Bologna','BO',5),(16,'Bolzano','BZ',4),(17,'Brescia','BS',4),(18,'Brindisi','BR',4),(19,'Cagliari','CA',3),(20,'Caltanissetta','CL',3),(21,'Campobasso','CB',2),(22,'Carbonia-Iglesias','CI',2),(23,'Caserta','CE',2),(24,'Catania','CT',3),(25,'Catanzaro','CZ',3),(26,'Chieti','CH',4),(27,'Como','CO',4),(28,'Cosenza','CS',2),(29,'Cremona','CR',5),(30,'Crotone','KR',3),(31,'Cuneo','CN',3),(32,'Enna','EN',3),(33,'Fermo','FM',6),(34,'Ferrara','FE',5),(35,'Firenze','FI',6),(36,'Foggia','FG',3),(37,'Forlì-Cesena','FC',5),(38,'Frosinone','FR',4),(39,'Genova','GE',8),(40,'Gorizia','GO',4),(41,'Grosseto','GR',6),(42,'Imperia','IM',6),(43,'Isernia','IS',3),(44,'La Spezia','AQ',4),(45,'L\'Aquila','SP',3),(46,'Latina','LT',4),(47,'Lecce','LE',4),(48,'Lecco','LC',4),(49,'Livorno','LI',7),(50,'Lodi','LO',4),(51,'Lucca','LU',7),(52,'Macerata','MC',4),(53,'Mantova','MN',4),(54,'Massa-Carrara','MS',5),(55,'Matera','MT',4),(56,'Medio Campidano','VS',2),(57,'Messina','ME',3),(58,'Milano','MI',6),(59,'Modena','MO',6),(60,'Napoli','NA',2),(61,'Novara','NO',4),(62,'Ogliastra','OG',2),(63,'Nuoro','NU',3),(64,'Olbia-Tempio','OT',4),(65,'Oristano','OR',3),(66,'Padova','PD',4),(67,'Palermo','PA',3),(68,'Parma','PR',5),(69,'Pavia','PV',4),(70,'Perugia','PG',10),(71,'Pesaro e Urbino','PU',5),(72,'Pescara','PE',4),(73,'Piacenza','PC',5),(74,'Pisa','PI',5),(75,'Pistoia','PT',4),(76,'Pordenone','PN',3),(77,'Potenza','PZ',2),(78,'Prato','PO',5),(79,'Ragusa','RG',4),(80,'Ravenna','RA',6),(81,'Reggio di Calabria','RC',3),(82,'Reggio nell\'Emilia','RE',5),(83,'Rieti','RI',3),(84,'Rimini','RN',7),(85,'Roma','Roma',5),(86,'Rovigo','RO',3),(87,'Salerno','SA',3),(88,'Sassari','SS',4),(89,'Savona','SV',7),(90,'Siena','SI',5),(91,'Siracusa','SR',4),(92,'Sondrio','SO',4),(93,'Taranto','TA',4),(94,'Teramo','TE',4),(95,'Terni','TR',3),(96,'Torino','TO',4),(97,'Trapani','TP',5),(98,'Trento','TN',4),(99,'Treviso','TV',4),(100,'Trieste','TS',6),(101,'Udine','UD',3),(102,'Varese','VA',4),(103,'Venezia','VE',4),(104,'Verbano-Cusio-Ossola','VB',4),(105,'Vercelli','VC',3),(106,'Verona','VR',5),(107,'Vibo Valentia','VV',2),(108,'Vicenza','VI',4),(109,'Viterbo','VT',3);
/*!40000 ALTER TABLE `rischioprovincia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-10 11:29:15
