\chapter{Funzionalità Chiave}
In questo capitolo, verrà riportata e descritta dettagliatamente una delle funzionalità chiave del sistema OCA.

\section{Elaborazione Statistiche} \label{chiave}

\subsection{Input del Sistema}
La fase di acquisizione dei dati di monitoraggio avviene attraverso una \textit{black-box}, esterna al sistema OCA. La scelta della scatola nera è ricaduta su \textit{Freematics}\footnote{\url{http://freematics.com/}}, un dispositivo hardware che permette di acquisire una varietà di dati, tra cui velocità, accelerazione e posizione GPS, tramite la centralina del veicolo su cui è posto.
\\ \newline
Ogni veicolo di un assicurato della compagnia assicurativa, che aderisce al servizio del sistema OCA, è dotato di un dispositivo \textit{Freematics}. Ogni volta che il veicolo viene messo in moto, il dispositivo comincia a registrare i valori tramite sensori e termina l'acquisizione appena il veicolo è spento. Assumiamo che i dati acquisiti relativi a un tragitto percorso vengano inviati al sistema OCA alla prima accensione di un veicolo; per facilitarne la comprensione, segue un esempio. 

Mario Rossi il giorno 01/01/2001 effettua un tragitto dalle ore 7:30 alle ore 11:00, a bordo di un veicolo targato AA111AA e dotato di scatola nera. Alle 13:30, Mario Rossi riaccende l'automobile per effettuare un secondo tragitto. All'avvio del veicolo, la \textit{black-box} invia i dati al sistema OCA relativi al primo tragitto percorso (quello dalle ore 07:30 alle 11:00). 

\subsubsection{Formato dei Dati} \label{formatofreem}
Il dispositivo \textit{Freematics} acquisisce i dati dal veicolo e li salva su file in un formato 
specifico \footnote{\url{http://freematics.com/pages/software/freematics-data-logging-format/}}. Ciascuna entry è separata da un simbolo di \textit{newline} ($\backslash$n) o tramite un simbolo di spazio.\\\\ \\Il formato di una data entry è il seguente:
\begin{center}
	<Time>,<Data Type>,<Data Value>[,<Data Value 2>,<Data Value 3>]
\end{center}

\textbf{Time} ha due condizioni:
\begin{enumerate}
	\item se comincia con il simbolo $\#$ ed è seguito da un valore numerico, esso indica il tempo assoluto in millisecondi da quando il dispositivo ha cominciato a rilevare dati. Per esempio, $\#$5000 significa che i dati sono stati catturati dopo che il dispositivo era in esecuzione da 5 secondi;
	\item se non comincia con il simbolo $\#$, il valore numerico rappresenta il tempo passato in millisecondi dall'ultima rilevazione. Per esempio, 10 indica 10 millisecondi passati dalla cattura dell'ultima rilevazione, mentre 0 significa che i dati sono stati rilevati durante lo stesso timestamp della precedente rilevazione.
\end{enumerate}

%\textbf{Data Type} è rappresentato come un numero in formato esadecimale che identifica il tipo di dati, in accordo allo standard OBD-II PIDs \footnote{\url{https://en.wikipedia.org/wiki/OBD-II_PIDs}},  e alcuni PIDs definiti custom da Freematics per i dati relativi a GPS, MEMS ed altri dati sensori.
\textbf{Data Type} è rappresentato da un sigla che indica la tipologia del dato rilevato da sensore. 
%I PIDs comuni allo standard sono i seguenti:
%\begin{itemize}
%\item \textit{0x104} - Engine load
%\item \textit{0x105} - Engine coolant temperature
%\item \textit{0x10a} - Fuel pressure
%\item \textit{0x10b} - Intake manifold absolute pressure
%\item \textit{0x10c} - Engine RPM
%\item \textit{0x10d} - Vehicle speed
%\item \textit{0x10e} - Timing advance
%\item \textit{0x10f} - Intake air temperature
%\item \textit{0x110} - MAF air flow rate
%\item \textit{0x111} - Throttle position
%\item \textit{0x11f} - Run time since engine start
%\item \textit{0x121} - Distance traveled with malfunction indicator lamp
%\item \textit{0x12f} - Fuel level input
%\item \textit{0x131} - Distance traveled since codes cleared
%\item \textit{0x133} - Barometric pressure
%\item \textit{0x142} - Control module voltage
%\item \textit{0x143} - Absolute load value
%\item \textit{0x15b} - Hybrid battery pack remaining life
%\item \textit{0x15c} - Engine oil temperature
%\item \textit{0x15e} - Engine fuel rate
%\end{itemize}
In particolare, i \textit{Data Type} utili al sistema OCA sono i seguenti:
\begin{itemize} 
	\item \textit{DTE} - Data UTC (in formato DDMMYY)
	\item \textit{TIM} - Ora UTC (in formato HHMMSSmm)
	\item \textit{LAT} - Latitudine
	\item \textit{LNG} - Longitudine
	%\item \textit{0xC} - Altitude (unità di misura: meters)
	\item \textit{SPD} - Velocità (unità di misura: km/h)
	%\item \textit{0xE} - Course (unità di misura: degree)
	%\item \textit{0xF} - Satellite number
	\item \textit{ACC} - Accelerazione (su tre assi x, y e z)
	%\item \textit{0x21} - Gyroscope data (x/y/z)
	%\item \textit{0x22} - Magitude field data (x/y/z)
	%\item \textit{0x23} - Device temperature (unità di misura: 0.1 Celsius degree)
	%\item \textit{0x24} - Battery voltage (unità di misura: 0.01V)
	%\item \textit{0x30} - Trip distance (unità di misura: meters)
\end{itemize}

\textbf{Data Value} è sempre un valore numerico. Molteplici valori possono essere presentati e divisi da un simbolo di virgola per alcuni tipi di dati (per esempio, gli assi X/Y/Z per ACC).
\\
Un esempio di file .csv nel formato descritto qui sopra è il seguente:
\begin{lstlisting}[frame=tb,language=Matlab,caption={Esempio di file Freematics Packet Data Format},numbers=left,numberstyle=\tiny,tabsize=2,basicstyle=\small\ttfamily]
#02122016_180253,10D,00
0,ACC,-913,18,528
0,GYR,-9,-22,17
111,104,0
0,ACC,-922,6,548
0,GYR,1,-27,17
30,111,16
0,ACC,-922,-9,513
0,GYR,-4,-36,24
29,10F,32
0,ACC,-927,14,573
0,GYR,-14,-11,16
8,TIM,18025300
0,DTE,021216
0,LAT,45523188
0,LNG,9222267
0,ALT,16200
0,SPD,19.69
0,SAT,5
0,ACC,-913,18,528
0,GYR,-9,-22,17
\end{lstlisting}

\subsubsection{Logger GPS per Android}
Siccome non si dispone di una scatola nera Freematics, è stato necessario pensare ad una soluzione per generare dei tragitti di un veicolo che fossero verosimili. La soluzione che è stata adottata è l'utilizzo di un'applicazione per Android chiamata \textit{Logger GPS per Android}\footnote{\url{https://play.google.com/store/apps/details?id=com.mendhak.gpslogger&hl=it}}.
Lo scopo di questa applicazione è la registrazione di dati di un dispositivo mobile, tra i quali le coordinate GPS, la velocità e l'accelerazione. Questi dati vengono salvati in un file in formato \textit{.gpx} sulla scheda SD del cellulare. Una volta concluso un tragitto, è possibile convertire il file .gpx in un file del formato di Freematics tramite l'apposito parser implementato in Java. 
\\
Questo step è stato necessario per simulare il comportamento di un assicurato e, di conseguenza, la presenza di file in output dalla \textit{black-box}, rappresentanti dei tragitti verosimili. La parte dell'implementazione relativa a \textit{Logger GPS per Android} non è tuttavia una componente del sistema OCA.

\subsection{Descrizione della funzionalità}

La funzionalità che si prende in esame è chiamata \textit{Elaborazione Statistiche}, che corrisponde al requisito funzionale OCA.statistiche, illustrato nel capitolo 3 del \textit{Documento dei Requisiti}, sezione 3.3.1.2. 
\\
Le fasi previste per questa funzionalità sono le seguenti: 
\begin{enumerate}
	\item L'automobile invia i dati relativi all'ultimo tragitto.
	\item Il sistema riceve i dati.
	\item Il sistema invia le coordinate geografiche al servizio BDG.
	\item Il Servizio BDG restituisce al sistema il corrispondente indirizzo, il limite di velocità e la tipologia di strada per ogni coppia di coordinate.
	\item Il sistema divide i dati in tronchi e vi associa la tipologia. 
	\item Il sistema calcola i chilometri percorsi, il numero di chilometri percorsi rispettando i limiti di velocità e la velocità media per ogni singolo tronco individuato.
	\item Il sistema somma i chilometri totali percorsi, i chilometri totali percorsi rispettando i limiti di velocità e calcola la media della velocità medie per ogni tipologia di tronco.
	\item Il sistema memorizza in OCB i dati calcolati del tragitto.
	\item Il sistema cancella i dati temporanei relativi al tragitto.
\end{enumerate}

La figura \ref{fig:elaborazionetragittitronchi} mostra il diagramma di sequenza che modella l'interazione tra le componenti circa la funzionalità del calcolo delle statistiche.
\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{"Figure/Elaborazione_Tragitti_Tronchi"}
	\caption{Diagramma di sequenza per il calcolo delle statistiche.}
	\label{fig:elaborazionetragittitronchi}
\end{figure}

I dati sensori restituiti dalla scatola nera sono file .csv che rispettano il formato di Freematics spiegato nella sezione \ref{formatofreem}. \uppercase{è} stato supposto che il nome dei file provenienti dalla scatola nera è nella forma "targa\_data\_ora" (per esempio "AA111AA\_02122016\_062053"). Il sistema riceve i file e li salva in una cartella dedicata. La cartella viene scansita e il sistema processa ogni file individuato, in modo da ottenere un file per ogni tragitto nel seguente formato:
\begin{lstlisting}[frame=tb,numberstyle=\tiny,tabsize=2,basicstyle=\scriptsize\ttfamily]
targa_veicolo
data;lat;long;vel_istantanea;indirizzo;vel_max;tipologia_strada
data;lat;long;vel_istantanea;indirizzo;vel_max;tipologia_strada
data;lat;long;vel_istantanea;indirizzo;vel_max;tipologia_strada
...
data;lat;long;vel_istantanea;indirizzo;vel_max;tipologia_strada
\end{lstlisting}
dove la \texttt{data} è nel formato gg-MM-aa;  \texttt{lat} e \texttt{long} rappresentano rispettivamente la latitudine e la longitudine della rilevazione in gradi decimali fino a sei cifre decimali; \texttt{tipologia\_strada} è una delle tipologie predefinite di OpenStreetMap.  
\\
Il processo di conversione dai file Freematics a questi file intermedi, chiamati "tragitti temporanei", consiste dei seguenti passi:
\begin{enumerate}
	\item viene estratta la targa del veicolo.
	\item viene estratta la data del tragitto.
	\item per ogni rilevazione eseguita nello stesso istante vengono estratte le coordinate, la velocità istantanea e l'ora e queste informazioni vengono memorizzate in un ArrayList della classe Rilevazione.
	\item per ogni rilevazione dell'ArrayList, le coppie di coordinate vengono usate per ottenere le informazioni del tronco.
	\item le rilevazioni relative al tragitto vengono salvate su un file, con la sintassi spiegata precedentemente, in una cartella dedicata.
\end{enumerate}
I tragitti temporanei vengono elaborati tramite i seguenti passi:
\begin{enumerate}
	\item viene estratta la targa
	\item per ogni riga di un file, vengono inizializzati ai valori di default i campi mancanti ed esclusi i valori errati. Se è presente la tipologia della strada, il campo viene convertito in "urbana", "extraurbana" o "autostrada", altrimenti la tipologia viene inferita a partire dalla velocità massima, se presente. Se è presente la tipologia della strada, ma non la velocità massima, la velocità viene inferita a partire dalla tipologia.
	\item le rilevazioni vengono divise in tronchi in base al nome della via e viene restituita una lista di istanze di tipo Tronco, dove ogni singolo tronco ha associata la relativa lista di dati sensore.
	\item per ogni tronco individuato, vengono eseguite le seguenti operazioni:
	\begin{enumerate}
		\item calcolo dei chilometri percorsi
		\item calcolo della velocità media
		\item calcolo dei chilometri percorsi in prudenza
	\end{enumerate}
	\item le informazioni calcolate per ogni singolo tronco vengono aggregate per ottenere il totale dei chilometri percorsi, il totale dei chilometri percorsi in prudenza e la velocità media per tipologia di tronco. 
	\item le statistiche aggregate relative ad un tragitto vengono memorizzate nel database OCB nella tabella Tragitto. Nel caso in cui il database non sia disponibile, vengono salvate su un file temporaneo e verranno poi caricate nel database al successivo inserimento in OCB.
\end{enumerate} 

\subsection{Punti Critici}
Una delle criticità riscontrate durante l'implementazione della parte del software relativa al calcolo delle statistiche è la comunicazione con il servizio di OpenStreetMap. Il programma prevede una richiesta di conversione per ogni coppia di coordinate inviate ad OSM. Il numero di chiamate risulta quindi decisamente elevato e il server OSM non permette in una finestra temporale ridotta un numero elevato di chiamate. Per ovviare a questo problema, si è deciso di salvare, ad ogni chiamata di OSM, le informazioni relative alle coordinate in una cartella apposita, chiamata \textit{OsmCache}. La ricerca viene quindi effettuata prima nella cartella cache e, solo nel caso in cui le coordinate non siano note, viene effettuata la richiesta ad OSM. \\Questa soluzione non può essere tuttavia la soluzione definitiva perché risulta inefficiente in un caso reale, dovuto al fatto che le possibili combinazioni di coordinate sono un numero elevatissimo. Un'alternativa consiste nello scaricare una copia del database di OSM e avvalersi di DBMS con funzionalità geospaziali per eseguire le query di reverse geocoding.

\section{Calcolo del livello di rischio}

La funzionalità che si prende in esame è chiamata \textit{Calcolo del Livello di Rischio}, che corrisponde al requisito funzionale OCA.rischio, illustrato nella sezione 3.3.1.6 nel \textit{Documento dei Requsiti}.
Il calcolo del livello di rischio associato ad un assicurato si suddivide in quattro fasi:
\begin{enumerate}
	\item la prima fase prevede il reperimento delle informazioni utili al calcolo dai database BDA e OCB e richiede l'aggiornamento dei valori delle statistiche cumulate su tutti i tragitti percorsi da un veicolo;
	\item la seconda fase predispone i dati prelevati in modo da essere utilizzabili dalla terza fase;
	\item la terza fase prevede l'applicazione della funzione del calcolo del livello di rischio;
	\item la quarta fase aggiorna il valore del livello di rischio memorizzato nel database.
\end{enumerate}

In particolare, nella prima fase, da BDA vengono prelevate la data di nascita dell'assicurato, il comune di residenza ed il numero di denunce di sinistro in cui la colpa dell'assicurato risulta parziale o totale. Dal database OCB vengono prelevati i valori cumulativi delle statistiche (chilometri totali, chilometri totali percorsi in prudenza, velocità media su urbana, extraurbana e autostrada) di tutti i tragitti percorsi nell'ultimo anno dai veicoli dell'assicurato in esame.
\\
Nella seconda fase viene ricavata l'età dell'assicurato a partire dall'anno di nascita e viene calcolato il livello di prudenza \textit{P} definito come il rapporto tra il numero di chilometri percorsi in prudenza rispetto a quelli totali percorsi, espresso in percentuale.
\\
L'algoritmo che determina il livello di rischio è una combinazione lineare definita come segue:
\begin{align*}
R =& 1,5 * normEta + 1,5 * prov + 0,5 * normKm + 2 * p + 1,5 * normDen + \\ &+ normVelUrb + normVelExtraUrb + normVelAut
\end{align*}
dove

\begin{itemize}
	\item \textit{normEta} rappresenta una normalizzazione delle fasce d'età in valori compresi tra 0 ed 1 che indicano il rischio di fare un incidente in base all'età del conducente. Per esempio, una persona di 20 o di 65 anni avranno una probabilità più alta di provocare un incidente e avranno quindi un valore più alto;
	\item \textit{prov} rappresenta la pericolosità di una determinata provincia dal punto di vista assicurativo. Per esempio, Milano o Roma hanno un rischio più elevato rispetto a città meno trafficate;
	\item \textit{normKm} rappresenta una normalizzazione che segue una funzione lineare e superati i 120.000 chilometri all'anno viene assegnato il valore 1;
	\item \textit{normDen} rappresenta una normalizzazione che segue una funzione lineare e superate le 20 denunce di sinistro in un anno viene assegnato il valore 1;
	\item \textit{p} rappresenta il livello di prudenza;
	\item \textit{normVelUrb} rappresenta una normalizzazione che segue una funzione lineare e superati i 70 km/h di velocità media viene assegnato il valore 1;
	\item \textit{normVelExtraUrb} rappresenta una normalizzazione che segue una funzione lineare e superati i 110 km/h di velocità media viene assegnato il valore 1;
	\item \textit{normVelAut} rappresenta una normalizzazione che segue una funzione lineare e superati i 150 km/h di velocità media viene assegnato il valore 1.
\begin{figure}[h!]
	\centering
	\includegraphics[width=1\textwidth]{Figure/Calcolo_livello_rischio}
	\caption{Diagramma di sequenza per il calcolo del livello di rischio di un assicurato.}
	\label{fig:calcololivellorischio}
\end{figure}
\end{itemize}

\newpage

\section{Visualizzazione}
Per le funzionalità riguardante la visualizzazione, è stata realizzata un'interfaccia grafica utente per permettere all'operatore OPP di interagire con il sistema OCA. L'interfaccia iniziale è mostrata in figura \ref{fig:scelta}. 
\begin{figure}[h]
	\centering
	\includegraphics{Figure/scelta}
	\caption{Schermata iniziale dell'interfaccia al momento dell'avvio.}
	\label{fig:scelta}
\end{figure}

L'interfaccia permette di scegliere le informazioni da ricercare: le informazioni su un assicurato e le informazioni su una polizza.
\subsection{Visualizzazione delle informazioni dell'assicurato}
La ricerca delle informazioni dell'assicurato prevede che un operatore OPP possa ricercare le informazioni di un assicurato di interesse ricercando per il suo nome o cognome oppure per l'id. 

La figura \ref{fig:assicuratoricercato} mostra l'interfaccia che mostra la funzionalità citata. In particolare, viene mostrato un esempio di ricerca per il cognome di un assicurato. 

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.7]{Figure/assicurato_ricercato}
	\caption{Interfaccia grafica utente per la visualizzazione delle informazioni dell'assicurato. La figura mostra la ricerca per cognome.}
	\label{fig:assicuratoricercato}
\end{figure}

Nella parte inferiore della finestra, vengono visualizzate le informazioni. Sulla sinistra viene mostrata una lista degli assicurati che presentano il cognome ricercato e che l'operatore può selezionare. Sulla destra vengono mostrate le informazioni associate all'assicurato selezionato, quali l'id, il nome, il cognome, il livello di rischio, la data di nascita, il comune di residenza, la provincia, la regione e il CAP. Il livello di rischio è il risultato del calcolo del livello che viene richiesto appena viene selezionato l'assicurato di interesse da parte dell'operatore OPP.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{Figure/visualizzazione_infoassicurato}
	\caption{Diagramma di sequenza per la visualizzazione delle informazioni dell'assicurato.}
	\label{fig:visualizzazioneinfoassicurato}
\end{figure}

\subsection{Visualizzazione delle informazioni della polizza}
Per la funzionalità di visualizzazione delle informazioni di una polizza, si prevede che l'interfaccia permetta la ricerca per id della polizza o per nome e cognome dell'intestatario. 

La figura \ref{fig:polizzaperassicurato} mostra l'interfaccia e in particolare viene mostrata la ricerca della polizza per il cognome dell'assicurato. 

Nella parte inferiore della finestra vengono visualizzati i risultati. Nella ricerca per intestatario, a sinistra vengono mostrati gli intestatari che hanno nome e cognome ricercati e che possono essere selezionati dall'operatore OPP. Mentre a destra vengono mostrate le polizze associate all'intestatario selezionato dall'operatore con id della polizza, la data del contratto e la targa del veicolo. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{Figure/polizza_per_assicurato}
	\caption{Interfaccia grafica utente per la visualizzazione delle informazioni della polizza. La figura mostra la ricerca per cognome dell'assicurato.}
	\label{fig:polizzaperassicurato}
\end{figure}

Nella ricerca per id della polizza, a sinistra viene mostrato l'id, la data del contratto e la targa del veicolo della polizza trovata. La figura \ref{fig:polizzaperid} mostra un esempio di ricerca per id della polizza.
\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{Figure/polizza_per_id}
	\caption{Interfaccia grafica utente per la visualizzazione delle informazioni della polizza. La figura mostra la ricerca per id della polizza.}
	\label{fig:polizzaperid}
\end{figure}

Selezionando la polizza di interesse, sia nel caso di ricerca per id sia per la ricerca per intestatario, viene aperta una finestra (si veda la figura \ref{fig:infopolizza}) che mostra le informazioni della polizza selezionata. In particolare, vengono visualizzati i chilometri totali percorsi, i chilometri percorsi rispettando i limiti, la velocità media urbana, la velocità media extraurbana e la velocità media mantenuta in autostrada per la polizza selezionata.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.6]{Figure/visualizzazione_infopolizza}
	\caption{Diagramma di sequenza per la visualizzazione delle informazioni associate ad una polizza}
	\label{fig:visualizzazioneinfopolizza}
\end{figure}
