\chapter{Architettura Concreta e di Deployment}

In questa sezione si presenterà l'architettura di deployment relativa al sistema OCA. Per quanto riguarda l'architettura concreta, si faccia riferimento al {Capitolo 4} del \textit{Documento delle Architetture}, in cui vengono esposte le componenti concrete e i diagrammi di sequenza che mostrano le interazioni tra le componenti individuate. 

% Innanzitutto, l'architettura di deployment (letteralmente \textit{dispiegato, messa in opera}) mostra l'assegnazione di elaborati software concreti (come i file eseguibili) ai nodi di calcolo (qualcosa che dispone di servizi di elaborazione). Esso mostra la distribuzione degli elementi software all'architettura fisica e la comunicazione (solitamente di rete) tra gli elementi fisici. L'elemento base di un diagramma di deployment è un nodo, di cui esistono due tipi differenti: nodo dispositivo oppure nodo ambiente di esecuzione (e.g. un sistema operativo, un sistema di gestione di basi di dati, una macchina virtuale, un browser web).


\section{Diagramma di Deployment}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.35]{Figure/DiagrammaDeployment.jpg}
  \caption{Diagramma di Deployment del sistema OCA}
  \label{fig:DiagrammaDeployment}
\end{figure}

\subsection{Nodi del Diagramma di Deployment}

In figura \ref{fig:DiagrammaDeployment} si riporta il diagramma di deployment del sistema OCA. \'E possibile osservare che il numero di componenti concrete è uguale al numero di componenti logiche definite nella fase di disegno dell'architettura.

 \'E anche possibile notare che il numero di nodi è pari a sei:
\begin{itemize}
\item il nodo \textit{Servizio BDG} rappresenta il servizio che converte le coordinate in una descrizione testuale (via, comune, CAP, provincia) e fornisce anche informazioni sulla tipologia della strada e sul limite di velocità;
\item il nodo \textit{Compagnia di Assicurazione} rappresenta la compagnia di assicurazione che ha installato il sistema OCA;
\item il nodo \textit{Server Centrale} rappresenta il server che contiene le componenti software computazionalmente più pesanti del sistema OCA, ossia il gestore dell'elaborazione dei tragitti, dei tronchi e delle statistiche;
\item il nodo \textit{Operatore OPP} rappresenta una postazione dell'operatore che si occupa di visualizzare informazioni di un assicurato e/o di una sua polizza;
\item il nodo \textit{Operatore OPI} rappresenta una postazione dell'operatore che si occupa di gestire l'assistenza in caso di incidente avvenuto;
\item il nodo \textit{Automobile dell'Assicurato} rappresenta un veicolo di un assicurato su cui è posta la scatola nera.
\end{itemize}

Il nodo \textit{Automobile dell'Assicurato} contiene la \textit{black-box} che a sua volta contiene i sensori TAC, GPS e ACC che rispettivamente rilevano la velocità, la posizione geografica e l'accelerazione del veicolo. Oltre alla \textit{black-box}, è presente la componente software che si occupa di rilevare un'eventuale anomalia nei dati dell'accelerazione del veicolo.
%Il nodo è collegato con l'operatore OPI perchè, nel caso di incidente rilevato, la componente \textit{Gestore Rilevamento Incidente} manderà le informazioni utili al nodo dell'operatore. Inoltre è collegato anche con il server centrale per l'invio degli altri dati di monitoraggio.
 
Il nodo \textit{Server Centrale} contiene tre componenti software che si occupano di:
\begin{enumerate}
\item elaborare i differenti tragitti percorsi da un veicolo (componente \textit{Gestore Elaborazione Tragitti});
\item elaborare i differenti tronchi percorsi da un veicolo (componente \textit{Gestore Elaborazione Tronchi});
\item calcolare il livello di rischio di un assicurato e i relativi parametri necessari per calcolarlo (componente \textit{Gestore Statistiche});
\end{enumerate}

Si noti che nel nodo \textit{Compagnia di Assicurazione} sono riportate solo le componenti della compagnia assicurativa di interesse rispetto al sistema OCA, perciò il nodo contiene solo i datastore \textit{Assicurato BDA}, \textit{Polizza BDA}, \textit{DenunciaDiSinistro BDA}.

Il nodo \textit{Operatore OPI} contiene due componenti software che si occupano di:
\begin{enumerate}
\item elaborare un incidente rilevato ed attivare una connessione telefonica tra operatore OPI e l'assicurato coinvolto nell'incidente con lo scopo di fornire assistenza (componente \textit{Gestore Elaborazione Incidente});
\item fornire assistenza ad un assicurato che richiede all'operatore OPI la posizione geografica in cui si trova, nel caso in cui l'incidente non sia stato rilevato dalla \textit{black-box} (componente \textit{Gestore Assistenza}).
\end{enumerate}

Il nodo \textit{Operatore OPP} contiene la componente software che si occupa di visualizzare i dati relativi ad un assicurato e/o ad una sua polizza (componente \textit{Gestore Visualizzazione}).

\subsection{Relazioni tra i Nodi del Diagramma di Deployment}

Tra i nodi riportati nel diagramma di deployment, esistono diverse relazioni:
\begin{enumerate}
\item la relazione tra i nodi \textit{Automobile dell'Assicurato} ed \textit{Operatore OPI} modella lo scambio di dati che avviene tra i due nodi, quando viene rilevata un'anomalia da parte del nodo \textit{Automobile dell'Assicurato}. Quest'ultimo invia al nodo \textit{Operatore OPI} il segnale dell'avvenuto incidente e le informazioni utili per la gestione dell'incidente.
\item la relazione tra i nodi \textit{Automobile dell'Assicurato} e \textit{Server Centrale} modella l'invio dei dati sensori da parte del nodo \textit{Automobile dell'Assicurato} al nodo \textit{Server Centrale}, affinché li possa elaborare in tragitti;
\item la relazione tra i nodi \textit{Operatore OPI} ed \textit{Assicurato} indica un'interazione tra i due soggetti in merito all'assistenza che l'operatore offre all'assicurato in caso di sinistro;
\item la relazione tra i nodi \textit{Servizio BDG} ed \textit{Operatore OPI} modella lo scambio di dati per la conversione delle coordinate geografiche in descrizione testuale della strada da parte del nodo \textit{Servizio BDG};
\item la relazione tra i nodi \textit{Operatore OPP} e \textit{Server Centrale} modella il flusso di dati che il nodo \textit{Server Centrale} invia all'operatore per la visualizzazione;
\item la relazione tra i nodi \textit{Operatore OPP} e \textit{Compagnia di Assicurazione} modella il flusso di dati che il nodo \textit{Compagnia di Assicurazione} invia all'operatore per la visualizzazione;
\item la relazione tra i nodi \textit{Server Centrale} e \textit{Servizio BDG} indica lo scambio di dati per conversione delle coordinate geografiche in descrizione testuale del tragitto;

\item la relazione tra i nodi \textit{Server Centrale} e \textit{Compagnia di Assicurazione} modella lo scambio di dati per permettere il calcolo del livello di rischio associato ad un assicurato.
\end{enumerate}