\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Architettura del Problema}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Diagramma dei Casi d'Uso}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Modello dei Dati}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Entit\IeC {\`a}}{4}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Associazioni tra Entit\IeC {\`a}}{7}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Generalizzazioni tra Entit\IeC {\`a}}{8}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Aggregazioni tra Entit\IeC {\`a}}{9}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Diagrammi delle Attivit\IeC {\`a}}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Gestione dell'Incidente}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Elaborazione dei Tragitti e dei Tronchi}{13}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Visualizzazione delle Informazioni}{15}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Calcolo delle Statistiche}{16}{subsection.2.3.4}
\contentsline {chapter}{\numberline {3}Architettura Logica}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Analisi Preliminare}{18}{section.3.1}
\contentsline {section}{\numberline {3.2}Soluzione 1: Ottimizzata per Astrazione}{20}{section.3.2}
\contentsline {section}{\numberline {3.3}Soluzione 2: Ottimizzata per Frequenza}{24}{section.3.3}
\contentsline {section}{\numberline {3.4}Soluzione 3: Ottimizzata per Astrazione e per Frequenza}{27}{section.3.4}
\contentsline {section}{\numberline {3.5}Confronto tra le Soluzioni Proposte}{31}{section.3.5}
\contentsline {chapter}{\numberline {4}Architettura Concreta}{34}{chapter.4}
\contentsline {section}{\numberline {4.1}Diagrammi delle Componenti Concrete}{34}{section.4.1}
\contentsline {section}{\numberline {4.2}Diagrammi di Sequenza}{36}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Rilevazione e gestione del sinistro}{36}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Calcolo statistiche e livello di rischio}{38}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Visualizzazioni}{40}{subsection.4.2.3}
