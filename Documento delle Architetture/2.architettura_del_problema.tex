\chapter{Architettura del Problema}

\section{Diagramma dei Casi d'Uso}

Per il diagramma dei casi d'uso, fare riferimento al \textit{Documento di Specifica dei Requisiti}.

\section{Modello dei Dati}

Il modello dei dati mostrato in figura \ref{fig:ModelloDati} ha lo scopo di descrivere la realtà di interesse secondo una rappresentazione di tipo concettuale. Tale modellazione è utile ai fini della comprensione delle entità e delle informazioni che circolano all'interno del sistema OCA ed aiuta a comprendere i diagrammi delle attività presentati nella successiva sezione \ref{DiagrammiAttività}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\textwidth]{Diagrammi/Architettura_del_problema/Modello_Dati.png}
	\caption{Modello dei dati del sistema OCA.}
	\label{fig:ModelloDati}
\end{figure}

\subsection{Entità}

Le entità che sono state individuate e definite nel modello dei dati sono le seguenti:
\begin{itemize}
	\item l'entità \textit{Assicurato} rappresenta un assicurato di una compagnia di assicurazione aderente al servizio offerto dal sistema OCA. L'assicurato è descritto tramite il nome, il cognome, il numero di telefono (necessario ai fini del contatto telefonico tra operatore OPI ed assicurato nella situazione di incidente avvenuto) ed il livello di rischio (un valore numerico intero compreso tra 1 e 10 inclusi); 
	
	\item l'entità \textit{Assicurato BDA} rappresenta un assicurato di una compagnia di assicurazione. Questa entità è presente nella base dati BDA, perciò non è nota la descrizione dell'entità, ma presumibilmente è descritta tramite il nome, il cognome, la data di nascita ed il comune di residenza;
	
	\item l'entità \textit{DenunciaDiSinistro BDA} rappresenta la denuncia di sinistro contenente informazioni relative all'incidente. Quest'ultima è presente nel database BDA, perciò non è nota la descrizione. Non è compito del sistema OCA gestirla, ma l'entità potrebbe essere verosimilmente descritta tramite il luogo geografico dove è avvenuto il sinistro, le autovetture e gli assicurati coinvolti, la data e l'ora in cui è avvenuto l'incidente;

	\item l'entità \textit{Incidente} rappresenta una collisione avvenuta e rilevata dalla scatola nera posta su un veicolo di un assicurato. L'incidente è descritto tramite la data (in formato giorno/mese/anno) e l'ora (in formato ore:minuti:secondi) in cui è avvenuto;

	\item l'entità \textit{Polizza} rappresenta una polizza assicurativa stipulata tra la compagnia di assicurazione e un assicurato aderente al servizio offerto dal sistema OCA. La polizza è descritta tramite la data di stipula del contratto (in formato giorno/mese/anno), utile per la ridefinizione della stessa;

	\item l'entità \textit{Polizza BDA} rappresenta una polizza assicurativa stipulata tra la compagnia di assicurazione e un assicurato. L'entità è presente in BDA, perciò non è nota la descrizione, tuttavia potrebbe essere descritta dalla data di stipula del contratto e dalle condizioni della polizza;

	\item l'entità \textit{InformazioniPolizza} rappresenta alcune delle informazioni di una polizza necessarie per il calcolo del livello di rischio di un assicurato. In particolare, l'entità è descritta tramite i chilometri totali percorsi dall'autovettura, i chilometri totali rispettando i limiti di velocità percorsi dall'autovettura associata alla polizza e la velocità media sostenuta dall'autovettura su strada urbana, extraurbana e autostrada (in formato km/h);
	
	\item l'entità \textit{Veicolo} rappresenta un'autovettura associata ad una polizza di proprietà di un assicurato. Il veicolo è descritto tramite la targa, il modello, la marca, il tipo di autovettura, l'anno di immatricolazione e la potenza erogata dal motore;

	\item l'entità \textit{Tragitto} rappresenta un intero itinerario percorso da un veicolo, da quando il veicolo è stato acceso a quando il veicolo è stato spento. Un tragitto è descritto tramite il numero di chilometri totali percorsi nel tragitto, il numero di chilometri totali percorsi rispettando i limiti di velocità nel tragitto e la velocità media tenuta su strada urbana, extraurbana ed autostrada (in formato km/h);

	\item l'entità \textit{Tronco} rappresenta un singolo tratto di un intero tragitto percorso da un veicolo (per esempio una via urbana). L'entità è descritta dalla tipologia di tronco (che può assumere uno dei valori \textquotedblleft urbano'', \textquotedblleft extraurbano'' e \textquotedblleft autostrada''), il limite di velocità associato, i chilometri totali percorsi, i chilometri totali percorsi rispettando i limiti di velocità e la velocità media tenuta su questo tronco stradale;

	\item l'entità \textit{Strada} rappresenta la carreggiata su cui un veicolo percorre il proprio tragitto. Quest'ultima è caratterizzata dalla tipologia di strada (che può assumere uno dei valori \textquotedblleft urbano'', \textquotedblleft extraurbano'' e \textquotedblleft autostrada''), il limite di velocità (in formato km/h), il nome della via, il comune, il CAP e la provincia in cui si trova la strada;

	\item l'entità \textit{Dato Sensore} rappresenta un dato campionato ed elaborato, dopo l'acquisizione di un dato grezzo da parte di un sensore. Il dato è caratterizzato dalla data e l'ora (in formato giorno/mese/anno ore:minuti:secondi) della rilevazione;

	\item l'entità \textit{Velocità} rappresenta un dato campionato ed elaborato relativo alla velocità di un veicolo. Quest'ultimo è descritto tramite il valore di velocità rilevato dal sensore TAC (in formato km/h) e dalla data e l'ora (in formato giorno/mese/anno ore:minuti:secondi) della rilevazione;

	\item l'entità \textit{Coordinate} rappresenta una coppia di coordinate geografiche associate ad una posizione di un veicolo in un determinato momento, in particolare rappresenta la coppia latitudine e longitudine associata a un veicolo rilevata dal sensore GPS, entrambe espresse in gradi decimali. \uppercase{è} inoltre descritto tramite la data e l'ora (in formato giorno/mese/anno ore:minuti:secondi) della rilevazione;

	\item l'entità \textit{Accelerazione} rappresenta un dato campionato ed elaborato relativo all'accelerazione di un veicolo. L'entità è caratterizzata dal valore dell'accelerazione rilevato dal sensore ACC (in formato m/sec\ap{2}) e dalla data e l'ora (in formato giorno/mese/anno ore:minuti:secondi) della rilevazione.
\end{itemize}

\subsection{Associazioni tra Entità}

Le associazioni che sono state individuate e definite nel modello dei dati sono le seguenti:
\begin{itemize}
	\item l'associazione tra le entità \textit{Assicurato BDA} e \textit{Denuncia di Sinistro} rappresenta una denuncia di sinistro associata ad un assicurato. Ad un assicurato possono essere associate più denunce di sinistro ed ad una denuncia di sinistro è associato un solo assicurato;
	\item l'associazione tra le entità \textit{Assicurato} e \textit{Assicurato BDA} rappresenta la corrispondenza tra l'assicurato memorizzato nella base dati OCB e l'assicurato memorizzato nella base dati BDA. Poiché gli assicurati presenti in OCB sono un sottoinsieme degli assicurati di BDA, ad un \textit{Assicurato BDA} può corrispondere al più un \textit{Assicurato} in OCB e, al contrario, ad un \textit{Assicurato} corrisponde uno e un solo \textit{Assicurato BDA}.
	\item analogamente, l'associazione tra \textit{Polizza} e \textit{Polizza BDA} rappresenta la corrispondenza tra una polizza di un assicurato memorizzato in OCB e una polizza di un assicurato memorizzato in BDA. Le cardinalità sono uguali alla associazione precedentemente descritta e hanno lo stesso significato;
	\item l'associazione tra le entità \textit{Assicurato} ed \textit{Incidente} rappresenta l'avvenimento di un incidente rilevato che coinvolge un assicurato. In particolare, un assicurato può essere coinvolto in 0 o più incidenti e un incidente è associato ad un solo assicurato. La cardinalità dell'associazione descrive il fatto che l'entità \textit{Incidente} rappresenta una collisione rilevata dal sensore, quindi si assume di creare una sua istanza per ogni assicurato, quando avviene un sinistro. Se più assicurati sono coinvolti in uno stesso incidente, verrà creata un'istanza di \textit{Incidente} per ogni assicurato coinvolto aderente al servizio del sistema OCA;
	\item l'associazione tra le entità \textit{Assicurato} e \textit{Polizza} rappresenta la stipulazione di una polizza da parte di un assicurato. Un assicurato può sottoscrivere diverse polizze, mentre ad una polizza corrisponde un solo assicurato. \item analogamente l'associazione tra le entità \textit{Assicurato BDA} e \textit{Polizza BDA} rappresenta la stessa associazione appena illustrata;
	\item l'associazione tra le entità \textit{Polizza} e \textit{InformazioniPolizza} rappresenta la relazione che sussiste tra la polizza ed i dati sul comportamento dei guidatori dell'automobile associata alla polizza, calcolati dal sistema OCA. Tra le due entità esiste un'associazione 1 a 1, poiché le informazioni presenti in \textit{InformazioniPolizza} fanno riferimento ad una singola polizza e, viceversa, una polizza ha associata solo una singola entità \textit{InformazioniPolizza};
	\item l'associazione tra le due entità \textit{Veicolo} e \textit{Incidente} rappresenta il coinvolgimento di un veicolo in un incidente rilevato dalla scatola nera posta sul veicolo. Basandosi sul significato dell'entità \textit{Incidente} illustrato precedentemente, un veicolo può essere coinvolto in zero o più incidenti, mentre ad ogni istanza di \textit{Incidente} è associato un solo veicolo;
	\item l'associazione tra le entità \textit{Incidente} e \textit{Strada} rappresenta il luogo geografico dove è avvenuto un sinistro. In particolare, un incidente avviene in un singolo luogo geografico, mentre una strada può essere il luogo geografico in cui sono avvenuti da zero a più incidenti;
	\item l'associazione tra le entità \textit{Strada} e \textit{Coordinate} descrive la composizione di una strada come un insieme di coppie di coordinate geografiche. Ad una strada sono associate da una a tante coordinate rilevate, mentre una coppia di coordinate identifica al massimo una strada (si escludono, quindi, le coordinate relative a fiumi, laghi, mari e campagne);
	\item l'associazione tra le entità \textit{Veicolo} e \textit{Tragitto} rappresenta il percorso effettuato da un veicolo dall'accensione allo spegnimento dello stesso. Un veicolo può effettuare più tragitti, mentre un tragitto è effettuato da un singolo veicolo;
	\item l'associazione tra le entità \textit{Veicolo} e \textit{Accelerazione} rappresenta l'acquisizione dei dati relativi all'accelerazione di un veicolo compiuta dal sensore ACC. Ad un veicolo sono associate molteplici accelerazioni rilevate dalla scatola nera, mentre una determinata accelerazione è associata ad un singolo veicolo;
	\item l'associazione tra le entità \textit{Veicolo} e \textit{Coordinate} rappresenta la posizione del veicolo al momento di un incidente oppure la posizione istantanea di un veicolo durante un tragitto. Ad un veicolo possono corrispondere più coordinate, mentre una coordinata ha associato un solo veicolo;
	\item l'associazione tra le entità \textit{Tragitto} e \textit{Dato Sensore} rappresenta la struttura di un tragitto percorso da un veicolo, che raccoglie più dati sensore. Un dato sensore specifico è relativo ad un singolo tragitto, mentre ad un tragitto sono associati più dati sensore;
	\item analogamente, l'associazione tra le entità \textit{Tronco} e \textit{Dato Sensore} descrive lo stesso concetto, limitandolo ad un singolo tronco stradale.
\end{itemize}

\subsection{Generalizzazioni tra Entità}

Le entità \textit{Velocità}, \textit{Coordinate} e \textit{Accelerazione} sono dei dati che vengono rilevati dai sensori presenti nella scatola nera e hanno in comune la data e l'ora in cui viene rilevato il dato. Per questo motivo, si è scelto di modellarle come specializzazioni dell'entità \textit{Dato Sensore} sfruttando il costrutto  \textit{Generalizzazione}, offerto dal linguaggio UML.

\subsection{Aggregazioni tra Entità}
Si è deciso di modellare l'entità \textit{Tragitto} come aggregazione dell'entità \textit{Tronco}, tramite l'apposito costrutto UML, poiché un tragitto, che rappresenta l'entità \textit{intero} è costituito da uno o più tronchi differenti, che rappresentano le \textit{parti} dell'intero.

\newpage

\section{Diagrammi delle Attività}

\label{DiagrammiAttività}

Le attività svolte dal sistema OCA sono state individuate mediante l'analisi dei requisiti funzionali descritti nel \textit{Documento di Specifica dei Requisiti} e sono le seguenti:
\begin{enumerate}
\item \textit{Attivazione della connessione telefonica};
\item \textit{Calcolo delle statistiche K, KP, V1, V2 e V3};
\item \textit{Calcolo del livello di rischio di un assicurato};
\item \textit{Conversione delle coordinate in indirizzo};
\item \textit{Divisione dei tragitti in tronchi};
\item \textit{Elaborazione di un tragitto};
\item \textit{Elaborazione dei tronchi};
\item \textit{Richiesta dell'indirizzo di un incidente};
\item \textit{Rilevamento dell'incidente};
\item \textit{Visualizzazione delle informazioni di una polizza};
\item \textit{Visualizzazione delle informazioni di un assicurato}.
\end{enumerate}

Di seguito si mostrano i diagrammi delle attività elencate, con i relativi commenti dove ritenuto necessario. In particolare, si è deciso di differenziare il flusso di controllo ed il flusso di dati; la notazione adottata per i diagrammi di attività è la seguente:
\begin{itemize}
\item la linea di colore \textit{rosso} indica il flusso di controllo;
\item la linea di colore \textit{grigio} indica il flusso di dati.
\end{itemize}

Inoltre, osservando attentamente i diagrammi delle attività riportati in questa sezione, è possibile ottenere informazioni per quanto riguarda la \textit{frequenza} (nel riquadro di colore verde), il \textit{ritardo} (nel riquardo di colore magenta) e la \textit{complessità} (nel riquadro di colore blu) delle azioni presenti in ogni diagramma.

\subsection{Gestione dell'Incidente}

In questa sezione vengono mostrati i diagrammi di attività relativi alle funzionalità di sistema per la gestione di un sinistro. 

%----------------------------------------------------------------%

\textbf{Rilevamento dell'Incidente}

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.8]{Diagrammi/Architettura_del_problema/Rilevamento_Incidente.png}
	\caption{Rilevamento dell'incidente.}
	\label{fig:RilevamentoIncidente}
\end{figure} 

La figura \ref{fig:RilevamentoIncidente} riporta il diagramma che rappresenta il rilevamento dell'avvenimento di un incidente di una autovettura. \\
Una volta al secondo, si prelevano i dati relativi alle accelerazioni salvate in precedenza in un buffer (inviate dalla \textit{black-box}) e si rilevano eventuali anomalie nelle accelerazioni. Nel caso in cui non si rileva un'anomalia, i dati vengono cancellati dal buffer. In caso contrario, si prelevano le informazioni relative al veicolo coinvolto nell'incidente (targa e coordinate geografiche) e si invia un segnale che indica l'avvenimento di un incidente.

%----------------------------------------------------------------%

\textbf{Attivazione della Connessione Telefonica}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Attivazione_Connessione_Telefonica.png}
	\caption{Attivazione della connessione telefonica tra operatore OPI ed assicurato.}
	\label{fig:AttivazioneConnessioneTelefonica}
\end{figure} 

La figura \ref{fig:AttivazioneConnessioneTelefonica} riporta il diagramma che rappresenta l'attivazione della connessione telefonica tra l'operatore OPI e l'assicurato dopo la rilevazione di un sinistro.
\\
Una volta che il sistema riceve il segnale dell'avvenuto incidente, preleva le coordinate geografiche che rappresentano la posizione geografica dell'incidente (salvate in un buffer). A questo punto, si eseguono due attività in parallelo:
\begin{itemize}
\item si prelevano le informazioni dell'assicurato (dal datastore \textit{OCB}), in particolare il numero di telefono necessario per la chiamata telefonica;
\item si convertono le coordinate geografiche che rappresentano la posizione dell'incidente in indirizzo stradale (tramite un'interazione con il servizio \textit{BDG}).
\end{itemize}

Dopo aver elaborato l'incidente con le informazioni reperite, avviene la chiamata telefonica tra operatore OPI ed assicurato coinvolto nell'incidente.

%----------------------------------------------------------------%
\textbf{Richiesta dell'Indirizzo di un Incidente}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Richiesta_Indirizzo_Incidente.png}
	\caption{Richiesta dell'indirizzo di un incidente.}
	\label{fig:RichiestaIndirizzoIncidente}
\end{figure} 

La figura \ref{fig:RichiestaIndirizzoIncidente} riporta il diagramma che modella la richiesta, da parte di un assicurato coinvolto in un incidente, dell'indirizzo stradale in cui è avvenuto l'incidente. 
\\
L'assicurato effettua una telefonata all'operatore OPI richiedendo l'indirizzo dell'incidente avvenuto. Per soddisfare la richiesta, si prelevano i dati relativi al veicolo associato all'assicurato coinvolto nel sinistro e si richiede la traduzione della posizione geografica in indirizzo stradale. Una volta ottenuta questa informazione, l'operatore OPI la comunica all'assicurato tramite la chiamata telefonica.

%----------------------------------------------------------------%

\textbf{Conversione delle Coordinate Geografiche in Indirizzo Stradale}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Conversione_Coordinate_Indirizzo.png}
	\caption{Conversione delle coordinate geografiche in indirizzo stradale.}
	\label{fig:ConversioneCoordinateGeograficheIndirizzoStradale}
\end{figure} 

La figura \ref{fig:ConversioneCoordinateGeograficheIndirizzoStradale} riporta il diagramma che rappresenta la conversione delle coordinate geografiche (che indicano la posizione di un incidente) in indirizzo stradale.
\\
Ogni volta che l'operatore OPI richiede la traduzione da coordinate geografiche ad indirizzo stradale, si prelevano le coordinate in cui si trova il veicolo e si convertono in indirizzo stradale, tramite il servizio offerto da \textit{BDG}.  Infine si restituisce il risultato e si invia un segnale che indica l'avvenuta traduzione.

%----------------------------------------------------------------%

\subsection{Elaborazione dei Tragitti e dei Tronchi}

In questa sezione vengono mostrati i diagrammi delle attività relativi alle funzionalità di sistema per l'elaborazione dei tragitti e dei tronchi.

%----------------------------------------------------------------%

\textbf{Divisione dei Tragitti in Tronchi}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Divisione_Tragitti_In_Tronchi.png}
	\caption{Divisione dei tragitti in tronchi.}
	\label{fig:DivisioneTragittiInTronchi}
\end{figure}

La figura \ref{fig:DivisioneTragittiInTronchi} riporta il diagramma che rappresenta la divisione in tronchi di un tragitto inviato dalla scatola nera. \\
Alla ricezione di un nuovo tragitto, si eseguono le seguenti azioni in parallelo:
\begin{itemize}
\item si prelevano le coordinate geografiche relative al tragitto considerato;
\item si prelevano le velocità relative al tragitto considerato.
\end{itemize}

Una volta che si hanno a disposizione questi dati, si effettua una separazione delle coordinate geografiche e delle velocità consecutive in base al tronco, tramite il servizio offerto da \textit{BDG} che converte le coordinate geografiche in una descrizione testuale della strada di cui fanno parte le coordinate. Infine, si spedisce un segnale che indica la fine della divisione dei dati sensori in tronchi.

%----------------------------------------------------------------%

\textbf{Elaborazione dei Tronchi}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Elaborazione_Tronchi.png}
	\caption{Elaborazione dei tronchi.}
	\label{fig:ElaborazioneTronchi}
\end{figure}

La figura \ref{fig:ElaborazioneTronchi} riporta il diagramma che rappresenta l'elaborazione dei singoli tronchi che compongono un tragitto. Questa attività può essere eseguita solo dopo la suddivisione di un tragitto nei suoi relativi tronchi.
\\
Appena ricevuto il segnale che indica la fine della separazione delle coordinate geografiche e delle velocità in tronchi, si prelevano questi dati e si eseguono tre attività in parallelo, per ogni tronco trovato:
\begin{itemize}
\item calcolo del numero dei chilometri percorsi;
\item calcolo della velocità media;
\item calcolo del numero di chilometri percorsi in prudenza.
\end{itemize}
Si salvano i risultati ottenuti in modo persistente e si controlla se sono stati elaborati tutti i tronchi relativi ad un tragitto. In caso affermativo, si invia un segnale che indica la fine dell'elaborazione dei tronchi; altrimenti, si ripetono le attività appena descritte per tutti i tronchi rimanenti.

%----------------------------------------------------------------%

\textbf{Elaborazione di un Tragitto}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Elaborazione_Tragitto.png}
	\caption{Elaborazione di un tragitto.}
	\label{fig:ElaborazioneTragitto}
\end{figure}

La figura \ref{fig:ElaborazioneTragitto} riporta il diagramma che rappresenta l'elaborazione di un tragitto, dopo aver elaborato i singoli tronchi che lo compongono. \\
Una volta ricevuto il segnale che indica la fine dell'elaborazione dei singoli tronchi, si prelevano i tronchi ed in parallelo vengono eseguite le seguenti attività:
\begin{itemize}
\item calcolo del numero di chilometri percorsi;
\item calcolo del numero di chilometri percorsi in prudenza;
\item calcolo della velocità media per la tipologia \textquotedblleft autostrada'';
\item calcolo della velocità media per la tipologia \textquotedblleft extraurbana'';
\item calcolo della velocità media per la tipologia \textquotedblleft urbana'';
\end{itemize}

Si salvano i risultati in modo persistente.

\subsection{Visualizzazione delle Informazioni}

In questa sezione vengono mostrati i diagrammi di attività relativi alle funzionalità di sistema per la visualizzazione di informazioni delle polizze e degli assicurati da parte di un operatore OPP.

%----------------------------------------------------------------%

\textbf{Visualizzazione delle Informazioni di una Polizza}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Visualizzazione_Informazioni_Polizza.png}
	\caption{Visualizzazione delle informazioni di una polizza.}
	\label{fig:VisualizzazioneInformazioniPolizza}
\end{figure}

La figura \ref{fig:VisualizzazioneInformazioniPolizza} riporta il diagramma che rappresenta la visualizzazione delle informazioni associate ad una polizza relativa ad un assicurato. \\
L'operatore OPP seleziona l'assicurato di cui vuole conoscere le informazioni. Scelto l'assicurato, si sceglie la polizza di cui si vuole avere informazioni. Si eseguono le seguenti attività in parallelo:
\begin{itemize}
\item si prelevano i dati relativi alla polizza dal datastore BDA;
\item si prelevano le informazioni relative alla polizza dal datastore OCB.
\end{itemize}

Le informazioni vengono visualizzate. Nel caso in cui l'operatore OPP voglia visualizzare le informazioni relative ad altre polizze dello stesso assicurato, ripete le operazioni appena descritte selezionando un'altra polizza.

%----------------------------------------------------------------%

\textbf{Visualizzazione dei Dati di un Assicurato e del Livello di Rischio}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Visualizzazione_Informazioni_Assicurato_Livello_Rischio.png}
	\caption{Visualizzazione delle informazioni di un assicurato e del livello di rischio.}
	\label{fig:VisualizzazioneDatiAssicuratoLivelloRischio}
\end{figure}

La figura \ref{fig:VisualizzazioneDatiAssicuratoLivelloRischio} riporta il diagramma che rappresenta la visualizzazione dei dati di un assicurato ed il livello di rischio associato. \\
L'operatore OPP seleziona l'assicurato di cui vuole conoscere i dati. A questo punto, si eseguono queste attività in parallelo:
\begin{itemize}
\item si invia un segnale per richiedere il calcolo del livello di rischio dell'assicurato selezionato;
\item si prelevano i dati relativi all'assicurato dal datastore BDA.
\end{itemize}

\uppercase{è} possibile visualizzare questi dati, anche in momenti differenti, poiché la visualizzazione di dati anagrafici dell'assicurato impiega pochi secondi, mentre la richiesta della visualizzazione del livello di rischio dell'assicurato può richiedere fino a qualche minuto.

\subsection{Calcolo delle Statistiche}

In questa sezione vengono mostrati i diagrammi di attività relativi alle funzionalità di sistema per il calcolo delle statistiche e del livello di rischio di un assicurato.
%----------------------------------------------------------------%

\textbf{Calcolo del Livello di Rischio di un Assicurato}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Calcolo_Livello_Rischio.png}
	\caption{Calcolo del livello di rischio di un assicurato.}
	\label{fig:CalcoloLivelloRischio}
\end{figure}

La figura \ref{fig:CalcoloLivelloRischio} riporta il diagramma che rappresenta il calcolo del livello di rischio associato ad un assicurato. 
\\
Ricevuta la richiesta del calcolo, si effettuano le seguenti fasi in parallelo:
\begin{itemize}
\item si prelevano le denunce di sinistro associate all'assicurato considerato memorizzate in \textit{BDA};
\item si prelevano il comune di residenza e l'età dell'assicurato memorizzati in \textit{BDA};
\item si prelevano tutte le polizze associate all'assicurato e si invia un segnale che richiede il calcolo delle statistiche (\textit{K}, \textit{KP}, \textit{V1}, \textit{V2} e \textit{V3}) relative alle polizze. Terminato il calcolo delle statistiche, si prelevano queste informazioni e si fanno le somme dei chilometri e le medie delle velocità tra tutte le polizze associate ad un assicurato; 
\end{itemize}

Una volta concluse queste tre fasi, si calcola il livello di rischio associato all'assicurato, si salva in modo persistente e si invia un segnale che indica la conclusione del calcolo del livello di rischio di un assicurato.

%----------------------------------------------------------------%


\textbf{Calcolo delle Statistiche K, KP, V1, V2 e V3}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Diagrammi/Architettura_del_problema/Determinazione_K_KP_V1_V2_V3.png}
	\caption{Calcolo delle statistiche K, KP, V1, V2 e V3.}
	\label{fig:CalcoloStatisticheK_KP_V1_V2_V3}
\end{figure}

La figura \ref{fig:CalcoloStatisticheK_KP_V1_V2_V3} riporta il diagramma che rappresenta il calcolo delle statistiche K, KP, V1, V2 e V3.
\\
Ricevuta la richiesta del calcolo delle statistiche, si eseguono le seguenti attività in parallelo per tutti i tragitti associati ad ogni polizza dell'assicurato:
\begin{itemize}
\item la somma dei chilometri percorsi (K), seguita dalla somma del numero di chilometri percorsi in prudenza (KP);
\item la media delle velocità medie per tipologia di tronco \textquotedblleft urbano'' (V1);
\item la media delle velocità medie per tipologia di tronco \textquotedblleft extraurbano'' (V2);
\item la media delle velocità medie per tipologia di tronco \textquotedblleft autostrada'' (V3);
\end{itemize}

Alla fine di ogni attività, avviene il salvataggio in modo persistente del risultato ottenuto. Infine, si invia un segnale che indica la conclusione del calcolo delle statistiche.
