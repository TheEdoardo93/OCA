package elaborazioneTragitto;
import util.DBConnection;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 * Questa classe consente di testare la correttezza dei metodi dedicati 
 * all'elaborazione dei tragitti percorsi dai veicoli di un assicurato
 * iscritto al sistema OCA.
 * 
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class ElaborazioneTragittiTest {

	//Percorsi delle cartelle utilizzate per i test.
	static String freematics_elaborati = "Test_Freematics_elaborati/";
	static String freematics = "Test_Freematics/";
	static String tragitti_temporanei = "Test_Tragitti_temporanei/";
	static String tragitti_noDB = "Test_tragitti_noDB/";
	static String osmCache = "OsmCache/";

	//Cartelle utilizzate per i test.
	static File folder_freem_elab;
	static File folder_freem;
	static File folder_tragitti_temp;
	static File folder_tragitti_noDB;

	//Dichiarazione ed istanziazione di un oggetto di classe 'ElaborazioneTragitti'.
	ElaborazioneTragitti et = new ElaborazioneTragitti();
	static String[] cartelle = new String[5];

	@ClassRule
	public static TemporaryFolder tmp_folder = new TemporaryFolder();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		try {
			//esecuzione di uno script batch (Windows) per accendere il database MySQL
			Runtime.getRuntime().exec("cmd  /c start \"\" \"Test/bat/start.bat.lnk\"");
			
			Thread.sleep(15000); //attendo 15 secondi per proseguire
		} catch (IOException e) {
			System.err.println("Attenzione: problemi nello stoppare il database MySQL! (IOException)");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.err.println("Attenzione: problemi nello stoppare il database MySQL! (InterruptedException)");
			e.printStackTrace();
		}

		//Creazione delle cartelle necessarie per i test.
		folder_freem_elab = tmp_folder.newFolder(freematics_elaborati);
		folder_freem = tmp_folder.newFolder(freematics);
		folder_tragitti_temp = tmp_folder.newFolder(tragitti_temporanei);	
		folder_tragitti_noDB = tmp_folder.newFolder(tragitti_noDB);


		cartelle[0] = folder_freem.getAbsolutePath() + "/";
		cartelle[1] = folder_freem_elab.getAbsolutePath() + "/";
		cartelle[2] = folder_tragitti_temp.getAbsolutePath() + "/";
		cartelle[3] = folder_tragitti_noDB.getAbsolutePath() + "/";
		cartelle[4] = osmCache;

		System.out.println("E' stato eseguito il metodo setupBeforeClass()!");
	}

	@Before
	public void setUp() throws Exception {

		//Cancellazione di tutti i file contenuti in tutte le cartelle definite.
		for (File f : folder_freem_elab.listFiles()) {
			f.delete();
		}
		for (File f : folder_freem.listFiles()) {
			f.delete();
		}
		for (File f : folder_tragitti_temp.listFiles()) {
			f.delete();
		}
		for (File f : folder_tragitti_noDB.listFiles()) {
			f.delete();
		}
		
		System.out.println("E' stato eseguito il metodo setUp()!");
	}

	@Test
	public final void testElaboraDatiSensori1() { //cartella vuota
		int numeroFreemElab = folder_freem_elab.listFiles().length;

		et.elaboraDatiSensori(cartelle);
		
		//il numero dei file nella cartella Freematics Elaborati prima e dopo resta uguale
		assertEquals(numeroFreemElab, folder_freem_elab.listFiles().length);

		System.out.println("E' stato eseguito il test 1 (zero righe)!");
	}

	@Test
	public final void testElaboraDatiSensori2() { //file zero rilevazioni

		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		et.elaboraDatiSensori(cartelle);	
		File fileZeroRighe = new File(folder_freem.getAbsolutePath()+ "/" + "AL490MB_02122016_180255");

		assertEquals(false, fileZeroRighe.exists()); // verifica che il file � stato cancellato

		System.out.println("E' stato eseguito il test 2 (Cartella Zero Righe)!");
	}

	@Test
	public final void testElaboraDatiSensori3() { //file una rilevazione

		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		int km = -1;

		try {
			conn2 = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = "SELECT kmPercorsi FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				countBD_post = res.getString(1);
			}

			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();

			while (res2.next()) {
				if (res2.getInt(1) == 0) {
					km = 0;
				}
			}

			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(pre + nFreemElaborati, post);	// aggiunti tanti tragitti quanti i file elaborati
		assertEquals(0, km); // c'� un tragitto i cui kmTotali sono uguali a zero (file 1 rilevazione)
		
		System.out.println("E' stato eseguito il test 3 (file una rilevazione)!");
	}

	@Test
	public final void testElaboraDatiSensori4(){ //molti file, molte rilevazioni, zero vie distinte

		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		try {
			conn2 =new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = "SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				countBD_post = res.getString(1);
			}

			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();			

			while(res2.next() && !op) {
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") == null
						&& res2.getString("velocitaMediaSuAutostrada") == null;
			}

			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(pre + nFreemElaborati, post);	// aggiunti tanti tragitti quanti i file elaborati
		assertEquals(true, op); // c'� un tragitto che ha velocit� urbana diversa da null e le altre uguali a null
		
		System.out.println("E' stato eseguito il test 4 (Molti file, molte rilevazioni, zero vie distinte)!");
	}

	@Test
	public final void testElaboraDatiSensori5(){ //molti file, molte rilevazioni, due vie distinte

		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 =new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = "SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				countBD_post = res.getString(1);
			}

			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();			

			while(res2.next() && !op) {
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") == null
						&& res2.getString("velocitaMediaSuAutostrada") != null;
			}

			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(pre + nFreemElaborati, post);	// aggiunti tanti tragitti quanti i file elaborati
		assertEquals(true, op); // c'� un tragitto che ha velocit� urbana e autostrada diverse da null e l'altra uguale a null
		
		System.out.println("E' stato eseguito il test 3 (Molti file, molte rilevazioni, due vie distinte)!");
	}

	@Test
	public final void testElaboraDatiSensori6(){ //molti file, molte rilevazioni, due vie distinte, stessa tipologia

		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 =new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = "SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				countBD_post = res.getString(1);
			}

			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();			

			while(res2.next() && !op) {
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") == null
						&& res2.getString("velocitaMediaSuAutostrada") == null;
			}

			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(pre + nFreemElaborati, post);	// aggiunti tanti tragitti quanti i file elaborati
		assertEquals(true, op);
		
		System.out.println("E' stato eseguito il test 5 (Molti file, molte rilevazioni, due vie distinte, stessa tipologia)!");
	}

	@Test
	public final void testElaboraDatiSensori7(){ //molti file, molte rilevazioni, due vie distinte, una con tipologia "non disponibile" e una diversa

		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		boolean v = false;
		try {
			conn2 =new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = "SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				countBD_post = res.getString(1);
			}

			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();			

			double vel =-1;
			while(res2.next() && !v) {
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") == null
						&& res2.getString("velocitaMediaSuAutostrada") == null;

				if (op == true) {
					vel = res2.getDouble("velocitaMediaSuUrbana");
				}

				if ((vel > 15.56) && (vel < 15.58)) {
					v = true;
				}
			}

			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(pre + nFreemElaborati, post);	// aggiunti tanti tragitti quanti i file elaborati
		assertEquals(true, v);
		
		System.out.println("E' stato eseguito il test 7 (molti file, molte rilevazioni, due vie distinte, una con tipologia \"non disponibile\" e una diversa)!");
	}

	@Test
	public final void testElaboraDatiSensori8(){ //database non attivo
		
		try {
			//esecuzione di uno script batch (Windows) per stoppare il database MySQL
			Runtime.getRuntime().exec("cmd  /c start \"\" \"Test/bat/stop.bat.lnk\"");
			
			Thread.sleep(15000); //attendo 15 secondi per proseguire
		} catch (IOException e) {
			System.err.println("Attenzione: problemi nel stoppare il database MySQL! (IOException)");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.err.println("Attenzione: problemi nel stoppare il database MySQL! (InterruptedException)");
			e.printStackTrace();
		}		
		
		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file"
						+ "sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		int nFileTragittiNoDB_prev = folder_tragitti_noDB.listFiles().length;
		
		et.elaboraDatiSensori(cartelle);

		int nFileTragittiNoDB_post =folder_tragitti_noDB.listFiles().length;

		try {
			//esecuzione di uno script batch (Windows) per accendere il database MySQL
			Runtime.getRuntime().exec("cmd  /c start \"\" \"Test/bat/start.bat.lnk\"");
			
			Thread.sleep(10000); //attendo 10 secondi per proseguire
		} catch (IOException e) {
			System.err.println("Attenzione: problemi nel stoppare il database MySQL! (IOException)");
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.err.println("Attenzione: problemi nel stoppare il database MySQL! (InterruptedException)");
			e.printStackTrace();
		}
		
		assertEquals(nFileTragittiNoDB_prev + folder_freem_elab.listFiles().length, nFileTragittiNoDB_post);
		
		System.out.println("E' stato eseguito il test 8 (database non attivo)!");
	}

	@Test
	public final void testElaboraDatiSensori9() { // un file, molte rilevazioni, molte vie distinte, tipologie tutte diverse (vale anche per il test 16)

		File source = new File("Test/Freematics_unFile/AL490MB_02122016_180253.csv");
		File dest = new File(folder_freem.getAbsolutePath()+ "/AL490MB_02122016_180253.csv");

		try {
			Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
			e.printStackTrace();
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		try {
			conn2 = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = " SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT 1";

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				countBD_post = res.getString(1);
			}
			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();
			
			if (res2.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") != null
						&& res2.getString("velocitaMediaSuAutostrada") != null;
			}
			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev); 
		int post = Integer.parseInt(countBD_post); 

		assertEquals(post, pre + 1);
		assertEquals(true, op); // tutte le velocit� medie diverse da null
		
		System.out.println("E' stato eseguito il test 9 (un file, molte rilevazioni, molte vie distinte, tipologie tutte diverse)!");
	}

	@Test
	public final void testElaboraDatiSensori10(){ //un file, molte rilevazioni, molte vie distinte, due tipologie diverse, velocit� massima non presente, velocit� = 0

		File source = new File("Test/Freematics_unFile/CN879BE_05122016_180038.csv");
		File dest = new File(folder_freem.getAbsolutePath()+ "/CN879BE_05122016_180038.csv");

		try {
			Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
			e.printStackTrace();
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		try {
			conn2 = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = " SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT 1";

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if(res.next()){
				countBD_post = res.getString(1);
			}
			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();
			
			if (res2.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") != null
						&& res2.getString("velocitaMediaSuAutostrada") == null;
			}
			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev); 
		int post = Integer.parseInt(countBD_post); 

		assertEquals(post, pre + 1);
		assertEquals(true, op); // tutte le velocit� medie diverse da null

		System.out.println("E' stato eseguito il test 10 (un file, molte rilevazioni, molte vie distinte,"
				+ "due tipologie diverse, velocit� massima non presente, velocit� = 0)!");
	}

	@Test
	public final void testElaboraDatiSensori11(){ //molti file, molte rilevazioni, tipologie tutte "non disponibili", molte vie distinte, velocit� = 0, velocit� massima non presente

		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 =new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = "SELECT kmPercorsi, velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				countBD_post = res.getString(1);
			}

			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();			
			
			while (res2.next() && !op) {
				if (res2.getDouble("kmPercorsi") > 0) {
					op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") != null
						&& res2.getString("velocitaMediaSuAutostrada") != null;
				}
			}

			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(pre + nFreemElaborati, post);	// aggiunti tanti tragitti quanti i file elaborati
		assertEquals(true, op);
		
		System.out.println("E' stato eseguito il test 11 (molti file, molte rilevazioni, tipologie tutte \"non disponibili\","
				+ "molte vie distinte, velocit� = 0, velocit� massima non presente)!");
	}

	@Test
	public final void testElaboraDatiSensori12() { //molti file, molte rilevazioni, molte vie distinte, tipologie tutte diverse, velocit� > 0, velocit� massima presente
		
		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}
		
		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di diversi tragitti*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 = new DBConnection().getOcbConnection();
			String query = "SELECT COUNT(*) FROM tragitto";

			String query2 = " SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				countBD_post = res.getString(1);
			}
			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();
			
			while(res2.next() && !op) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				
				op = res2.getString("velocitaMediaSuUrbana") != null && res2.getString("velocitaMediaSuExtraurbana") != null
						&& res2.getString("velocitaMediaSuAutostrada") != null; //perch� tutte le tipologie sono diverse
			}
			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(post, pre + nFreemElaborati);		
		assertEquals(true, op);
		
		System.out.println("E' stato eseguito il test 12 (molti file, molte rilevazioni, molte vie distinte,"
				+ "tipologie tutte diverse, velocit� > 0, velocit� massima presente)!");
	}

	@Test
	public final void testElaboraDatiSensori13(){ //molti file, molte rilevazioni, molte vie distinte, due tipologie diverse, velocit� = 0, velocit� massima presente
		
		File freematics_folder = new File("Test/Freematics_moltiFile/");

		for (File source : freematics_folder.listFiles()) {
			File dest = new File(folder_freem.getAbsolutePath()+ "/" + source.getName());

			try {
				Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
				e.printStackTrace();
			}
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		int nFreemElaborati = folder_freem_elab.listFiles().length;
		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 =new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = "SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT " + nFreemElaborati;

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				countBD_post = res.getString(1);
			}

			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();			

			while (res2.next() && !op) {
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") != null
						&& res2.getString("velocitaMediaSuAutostrada") == null;
			}
			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(pre + nFreemElaborati, post); //aggiunti tanti tragitti quanti i file elaborati
		assertEquals(true, op);	
		
		System.out.println("E' stato eseguito il test 13 (molti file, molte rilevazioni, molte vie distinte,"
				+ "due tipologie diverse, velocit� = 0, velocit� massima presente)!");
	}

	@Test
	public final void testElaboraDatiSensori14(){ //un file, molte rilevazioni, molte vie distinte, tipologie due diverse, velocit� > 0, velocit� massima presente

		File source = new File("Test/Freematics_unFile/CN879BE_05122016_175556.csv");
		File dest = new File(folder_freem.getAbsolutePath()+ "/CN879BE_05122016_175556.csv");

		try {
			Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
			e.printStackTrace();
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = " SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT 1";

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				countBD_post = res.getString(1);
			}
			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();
			
			if (res2.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				op = res2.getString("velocitaMediaSuUrbana") == null
						&& res2.getString("velocitaMediaSuExtraurbana") != null
						&& res2.getString("velocitaMediaSuAutostrada") != null; //perch� ho due tipologie
			}
			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev); 
		int post = Integer.parseInt(countBD_post); 

		assertEquals(post, pre + 1);
		assertEquals(true, op); 
		
		System.out.println("E' stato eseguito il test 14 (un file, molte rilevazioni, molte vie distinte,"
				+ "tipologie due diverse, velocit� > 0, velocit� massima presente)!");
	}

	@Test
	public final void testElaboraDatiSensori15(){ //un file, molte rilevazioni, molte vie distinte, tutte "non disponibili", velocit� massima non presente

		File source = new File("Test/Freematics_unFile/CN879BE_12122016_062217.csv");
		File dest = new File(folder_freem.getAbsolutePath()+ "/CN879BE_12122016_062217.csv");

		try {
			Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
			e.printStackTrace();
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = " SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT 1";

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				countBD_post = res.getString(1);
			}
			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();
			
			if (res2.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				op = res2.getString("velocitaMediaSuUrbana") == null
						&& res2.getString("velocitaMediaSuExtraurbana") == null
						&& res2.getString("velocitaMediaSuAutostrada") == null; //perch� la velocit� max non � presente quindi afferisco la tipologia
			}
			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev); 
		int post = Integer.parseInt(countBD_post); 

		assertEquals(post, pre + 1);
		assertEquals(true, op); 
		
		System.out.println("E' stato eseguito il test 15 (un file, molte rilevazioni, molte vie distinte,"
				+ "tutte \"non disponibili\", velocit� massima non presente)!");	
	}
	
	@Test 
	public final void elaboraDatiSensori16(){ //un file, molte rilevazioni, molte vie distinte, tutte diverse, velocit� massima presente

		
		File source = new File("Test/Freematics_unFile/AL490MB_07122016_153639.csv");
		File dest = new File(folder_freem.getAbsolutePath()+ "/AL490MB_07122016_153639.csv");

		try {
			Files.copy(source.toPath(),dest.toPath(),StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.println("Attenzione: IOException durante l'operazione di copia del file sorgente nel file destinazione!");
			e.printStackTrace();
		}

		Connection conn = null;
		String countBD_prev = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				prima di effettuare l'elaborazione di un tragitto*/
				countBD_prev = res.getString(1);
			}
			conn.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		et.elaboraDatiSensori(cartelle);		

		Connection conn2 = null;
		String countBD_post = null;
		boolean op = false;
		
		try {
			conn2 = new DBConnection().getOcbConnection();
			String query = " SELECT COUNT(*) FROM tragitto";
			String query2 = " SELECT velocitaMediaSuUrbana, velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada"
					+ " FROM tragitto ORDER BY id_tragitto DESC LIMIT 1";

			PreparedStatement preparedStmt = conn2.prepareStatement(query);
			ResultSet res = preparedStmt.executeQuery();
			
			if (res.next()) {
				countBD_post = res.getString(1);
			}
			PreparedStatement preparedStmt2 = conn2.prepareStatement(query2);
			ResultSet res2 = preparedStmt2.executeQuery();
			
			if (res2.next()) {
				/*numero di record nella tabella Tragitto del database OCB
				dopo aver effettuato l'elaborazione di un tragitto*/
				op = res2.getString("velocitaMediaSuUrbana") != null
						&& res2.getString("velocitaMediaSuExtraurbana") != null
						&& res2.getString("velocitaMediaSuAutostrada") != null; //perch� tutte le tipologie sono diverse
			}
			conn2.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			System.err.println("SQLState: " + ex.getSQLState());
			System.err.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException e) {
			System.err.println("Attenzione: ClassNotFoundException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Attenzione: IOException!");
			e.printStackTrace();
		}

		int pre = Integer.parseInt(countBD_prev);
		int post = Integer.parseInt(countBD_post);

		assertEquals(post, pre + 1);		
		assertEquals(true, op);
		
		System.out.println("E' stato eseguito il test 16 (un file, molte rilevazioni, molte vie distinte,"
				+ "tutte diverse, velocit� massima presente)!");
	}
	
	@After
	public void tearDown() {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		
	}
}