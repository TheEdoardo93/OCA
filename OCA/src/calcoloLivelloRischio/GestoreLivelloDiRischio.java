package calcoloLivelloRischio;

import java.time.LocalDateTime;

import dataMapper.LivelloDiRischioMapper;
import util.InformazioniAssicurato;

/**
 * Questa classe si occupa di definire l'algoritmo per il calcolo del livello
 * di rischio associato ad un assicurato ed il salvataggio in modo persistente
 * del valore calcolato nel database OCB.
 */

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class GestoreLivelloDiRischio {

	/**
	 * Questo metodo si occupa di aggiornare i parametri (chilometri totali percorsi,
	 * chilometri totali percorsi in prudenza, velocit� media su strada urbana,
	 * velocit� media su strada extraurbana, velocit� media su autostrada) per tutte le
	 * polizze associate ad un assicurato ricevuto in input.
	 * Inoltre, questo metodo preleva i parametri salvati nel database OCB e
	 * nel database BDA utili per il calcolo del livello di rischio per poi poter
	 * calcolare il valore del livello di rischio dell'assicurato.
	 * 
	 * @param id_assicurato int
	 * @return livello di rischio
	 */
	public int restituisciAggiornaLivelloRischio(int id_assicurato) {

		boolean risultatoAggiornamento = aggiornaParametri(id_assicurato);

		if (risultatoAggiornamento == false) {
			System.err.println("Attenzione: l'aggiornamento dei parametri � avvenuto senza successo!");
		}

		InformazioniAssicurato ia = prelevaParametri(id_assicurato);

		ia.setRischio(calcolaLivelloDiRischio(ia));

		if (salvaLivelloDiRischio(ia) == false) {
			System.err.println("Attenzione: il salvataggio del livello di rischio dell'assicurato con"
					+ "id = " + id_assicurato + " � avvenuto senza successo!");
		}
		
		return ia.getRischio();
	}

	private boolean salvaLivelloDiRischio(InformazioniAssicurato ia) {
		
		LivelloDiRischioMapper ldrm = new LivelloDiRischioMapper();
		boolean risultatoSalvataggio = ldrm.saveLivelloRischioOcb(ia);

		return risultatoSalvataggio;
	}

	/**
	 * Questo metodo si occupa di prelevare i parametri utili per il calcolo del livello di rischio
	 * di un assicurato (data di nascita dell'assicurato, comune di residenza, numero di
	 * denunce di sinistro in cui � coinvolto l'assicurato, numero di chilometri totali percorsi,
	 * livello di prudenza dell'assicurato, velocit� media su strada urbana,
	 * velocit� media su strada extraurbana, velocit� media su autostrada)
	 * dal database OCB e dal database BDA.
	 * 
	 * @param id_assicurato int
	 * @return InformazioniAssicurato
	 */
	private InformazioniAssicurato prelevaParametri(int id_assicurato) {

		InformazioniAssicurato ia = new InformazioniAssicurato(id_assicurato);

		LivelloDiRischioMapper ldrm = new LivelloDiRischioMapper();

		ia = ldrm.findInformazioniAssicurato(ia.getIdAssicurato());

		return ia;
	}

	/**
	 * Questo metodo si occupa di calcolare il valore del livello di rischio R di un assicurato
	 * il cui identificativo viene passato come parametro.
	 * Il valore R viene calcolato come una combinazione lineare dei 6 parametri ricevuti in input
	 * al metodo come parametro, i cui pesi associati ad ogni parametro sono stati definiti
	 * tramite un meccanismo di "soglia".
	 * 
	 * @param ia InformazioniAssicurato
	 * @return livello di rischio
	 */
	private int calcolaLivelloDiRischio(InformazioniAssicurato ia) {

		/*Calcolo del valore "Denunce" nella combinazione lineare*/
		double Denunce = calcolaPesoDenunce(ia.getNumeroDenunce());

		/*Calcolo del valore "normEta" nella combinazione lineare*/
		LocalDateTime dataOdierna = LocalDateTime.now(); //seleziono la data odierna
		int annoOdierno = dataOdierna.getYear(); //prendo l'anno della data odierna
		int annoNascitaAssicurato = Integer.parseInt(ia.getDataNascita().substring(0, 4)); //seleziono l'anno di nascita dell'assicurato
		int eta = annoOdierno - annoNascitaAssicurato; //et� dell'assicurato
		ia.setEt�(eta);
		double normEta = calcolaPesoEta(eta);

		/*Calcolo del valore "normKm" nella combinazione lineare*/
		double normKm= calcolaPesoKm(ia.getChilometriTotali());

		/*Calcolo del valore "p" nella combinazione lineare*/
		double p = 1 - ia.getLivelloPrudenza()/100;

		/*Calcolo del valore "normVu" nella combinazione lineare*/
		double normVu = calcolaPesoVelocitaUrbana(ia.getVelocit�MediaSuUrbana());

		/*Calcolo del valore "normVe" nella combinazione lineare*/
		double normVe = calcolaPesoVelocitaExtraurbana(ia.getVelocit�MediaSuExtraurbana());

		/*Calcolo del valore "normVa" nella combinazione lineare*/
		double normVa = calcolaPesoVelocitaAutostrada(ia.getVelocit�MediaSuAutostrada());

		/*Calcolo del valore "prov" nella combinazione lineare*/
		double prov = ia.getRischioProvincia()/10;

		/*Combinazione lineare per il calcolo del valore del livello di rischio*/

		int R = (int) (Math.round(normEta*1.5 + prov*1.5 + normKm * 0.5 + p *2 + Denunce * 1.5
				+ normVa + normVe + normVu));

		return R;
	}

	/**
	 * Questo metodo si occupa di calcolare il peso da associare al parametro
	 * "normVu" nella combinazione lineare.
	 * 
	 * @param vu double
	 * @return pesoVu double
	 */
	private double calcolaPesoVelocitaUrbana(double vu) {

		double pesoVu = 0;

		if (vu <= 50) {
			pesoVu = 0;
		} else {
			if (vu <= 70) {
				pesoVu = 0.5;
			} else {
				pesoVu = 1;
			}
		}
		return pesoVu;
	}

	/**
	 * Questo metodo si occupa di calcolare il peso da associare al parametro
	 * "normVe" nella combinazione lineare.
	 * 
	 * @param ve double
	 * @return pesoVe double
	 */
	private double calcolaPesoVelocitaExtraurbana(double ve) {
		
		double pesoVe = 0;
		
		if (ve <= 90) {
			pesoVe = 0;
		} else {
			if (ve <= 110) {
				pesoVe = 0.5;
			} else {
				pesoVe = 1;
			}
		}
		return pesoVe;
	}

	/**
	 * Questo metodo si occupa di calcolare il peso da associare al parametro
	 * "normVa" nella combinazione lineare.
	 * 
	 * @param va double
	 * @return pesoVa double
	 */
	private double calcolaPesoVelocitaAutostrada(double va) {
		
		double pesoVa = 0;
		
		if (va <= 130) {
			pesoVa = 0;
		} else {
			if (va <= 150) {
				pesoVa = 0.5;
			} else {
				pesoVa = 1;
			}
		}
		return pesoVa;
	}

	/**
	 * Questo metodo si occupa di calcolare il peso da associare al parametro
	 * "normKm" nella combinazione lineare.
	 * 
	 * @param km double
	 * @return normKm double
	 */
	private double calcolaPesoKm(double km) {
		
		double normKm = 0;
		
		if (km > 120000) {
			normKm = 1;
		} else {
			normKm = km/120000;
		}
		return normKm;
	}

	/**
	 * Questo metodo si occupa di calcolare il peso da associare al parametro
	 * "normEta" nella combinazione lineare.
	 * 
	 * @param eta int
	 * @return normEta double
	 */
	private double calcolaPesoEta(int eta) {
		
		double normEta = 0;
		
		if ((eta < 25) || (eta > 60)) {
			normEta = 1;
		} else {
			if ((eta < 30) || (eta > 45)) {
				normEta = 0.5;
			} else {
				normEta = 0;
			}
		}
		return normEta;
	}

	/**
	 * Questo metodo si occupa di calcolare il peso da associare al parametro
	 * "normVe" nella combinazione lineare.
	 * 
	 * @param denunce int
	 * @return peso_denunce double
	 */
	private double calcolaPesoDenunce(int denunce) {
		
		double peso_denunce = 0;
		
		switch (denunce) {
		case 0:
		case 1:
			peso_denunce = 0;
			break;
		case 2: 
		case 3:
			peso_denunce = 0.2;
			break;
		case 4: 
		case 5: 
		case 6: 
		case 7: 
			peso_denunce = 0.4;
			break;
		case 8: 
		case 9: 
		case 10: 
		case 11: 
		case 12: 
			peso_denunce = 0.6;
			break;
		case 13: 
		case 14: 
		case 15: 
		case 17: 
		case 18: 
		case 19: 
			peso_denunce = 0.8;
			break;
		case 20: 
			peso_denunce = 1;
			break;
		default: 
			peso_denunce = 1;
		}
		return peso_denunce;
	}

	/**
	 * Questo metodo si occupa di aggiornare i parametri utili per il calcolo del
	 * livello di rischio di un assicurato in modo persistente nel database OCB.
	 * 
	 * @param id_assicurato int
	 * @return risultatoAggiornamento boolean
	 */
	private boolean aggiornaParametri(int id_assicurato) {

		LivelloDiRischioMapper ldrm = new LivelloDiRischioMapper();
		boolean risultatoAggiornamento = ldrm.updateParametri(id_assicurato);

		return risultatoAggiornamento;
	}
}
