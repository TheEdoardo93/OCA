package gui;
import dataMapper.PolizzaMapper;
import util.Assicurato;
import util.Polizza;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

/**
 * Questa classe definisce l'intefaccia grafica utente che permette
 * la ricerca di informazioni di una polizza.
 * 
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class PolizzaGUI {

	/** Questo metodo restituisce la finestra utile per la ricerca e 
	 * visualizzazione delle informazioni relative a una polizza.
	 * @return JFrame
	 */
	public JFrame creaFinestraPolizza() {

		int x = 0;
		int y = 0;
		JFrame finestra = new JFrame("Ricerca per polizza");
		finestra.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		JRadioButton cerca_per_id = new JRadioButton("Cerca polizza per id");
		JRadioButton cerca_per_nome = new JRadioButton("Cerca polizza per intestatario");
		ButtonGroup gruppo_radio_button = new ButtonGroup();
		cerca_per_id.setSelected(true);
		gruppo_radio_button.add(cerca_per_id);
		gruppo_radio_button.add(cerca_per_nome);

		c.gridx = x;
		c.gridy = y;
		c.gridwidth=1;
		finestra.add(cerca_per_id, c);
		finestra.add(cerca_per_nome);

		JPanel pannello_id = new JPanel(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
		TitledBorder border = new TitledBorder("Id Polizza");
		pannello_id.setBorder(border);

		JLabel id_etichetta = new JLabel("Id");
		c1.gridx = x;
		c1.gridy = y;

		c1.insets = new Insets(20, 5, 20, 5);
		pannello_id.add(id_etichetta, c1);

		JTextField id_testo = new JTextField(20);
		c1.gridx = x+1;
		c1.gridy = y;
		c1.insets = new Insets(20, 5, 20, 5);
		pannello_id.add(id_testo, c1);

		c.gridx=x;
		c.gridy=y+1;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(pannello_id,c);

		JPanel pannello_assicurato = new JPanel(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();
		TitledBorder border_assicurato = new TitledBorder("Assicurato");
		pannello_assicurato.setBorder(border_assicurato);

		JLabel cognome = new JLabel("Cognome");
		c2.gridx = 0;
		c2.gridy = 0;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(cognome, c2);

		JTextField cognome_assicurato = new JTextField(20);
		cognome_assicurato.setEditable(false);
		c2.gridx = 1;
		c2.gridy = 0;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(cognome_assicurato, c2);

		JLabel nome = new JLabel("Nome");
		c2.gridx = 0;
		c2.gridy = 1;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(nome, c2);

		JTextField nome_assicurato = new JTextField(20);
		nome_assicurato.setEditable(false);
		c2.gridx = 1;
		c2.gridy = 1;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(nome_assicurato, c2);

		c.gridx=x+1;
		c.gridy=y+1;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(pannello_assicurato,c);

		JButton cerca = new JButton("Cerca");
		c.gridx = x;
		c.gridy = y+3;
		c.gridwidth=4;
		finestra.add(cerca, c);

		JPanel pannello_risultati = new JPanel(new GridBagLayout());
		GridBagConstraints c3 = new GridBagConstraints();

		/*Lista selezione risultati*/
		DefaultTableModel modello_tabella = new DefaultTableModel();
		JTable lista_risultati = new JTable(modello_tabella);
		lista_risultati.setDefaultEditor(Object.class, null);
		lista_risultati.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane listScroller = new JScrollPane(lista_risultati);
		listScroller.setViewportView(lista_risultati);
		listScroller.setPreferredSize(new Dimension(300, 400));

		c3.gridx = 0;
		c3.gridy = 0;
		c3.gridheight=2;
		pannello_risultati.add(listScroller, c3);

		/*Lista descrizione risultati*/
		DefaultTableModel modello_tabella_descrizione = new DefaultTableModel();
		JTable lista_descrizione = new JTable(modello_tabella_descrizione);
		lista_descrizione.setDefaultEditor(Object.class, null);
		lista_descrizione.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane listScroller2 = new JScrollPane(lista_descrizione);
		listScroller2.setViewportView(lista_descrizione);
		listScroller2.setPreferredSize(new Dimension(400, 380));

		c3.gridx=1;
		c3.gridy=0;
		c3.gridheight=1;
		pannello_risultati.add(listScroller2, c3);

		JTextField conteggio_polizze = new JTextField(36);
		conteggio_polizze.setEditable(false);

		c3.gridx=1;
		c3.gridy=1;
		pannello_risultati.add(conteggio_polizze, c3);

		c.gridx=x;
		c.gridy=y+4;
		c.gridwidth=4;
		c.insets = new Insets(10, 10, 10, 10);
		finestra.add(pannello_risultati, c);

		/*Metodi actionListener()*/

		cerca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				/*Questo metodo consente di controllare se l'utente non ha inserito nessuna delle 3 informazioni
				 * (id polizza, cognome intestatario, nome intestatario)*/
				boolean informazioni = controlloNessunaInformazioneInserita(id_testo, cognome_assicurato, nome_assicurato);

				if (!informazioni) {

					modello_tabella_descrizione.setRowCount(0);
					modello_tabella_descrizione.setColumnCount(0);
					conteggio_polizze.setText("");

					if (!(id_testo.getText().equals(""))) {
						PolizzaMapper pm = new PolizzaMapper();
						Polizza p = pm.find(id_testo.getText());

						String risultato [] = new String [3];

						modello_tabella.setRowCount(0);
						modello_tabella.setColumnCount(0);
						modello_tabella.addColumn("Id");
						modello_tabella.addColumn("Data Contratto");
						modello_tabella.addColumn("Targa Veicolo");

						if (p == null) {
							//metodo per controllare se la query ha restituito 0 risultati oppure almeno 1 risultato
							finestraZeroRisultatiIDPolizza(id_testo);
						}
						else {
							risultato [0]= p.getIdPolizza();
							risultato [1] = p.getDataContratto();
							risultato [2] = p.getTargaVeicolo();
							modello_tabella.addRow(risultato);
						}
					}
					else {
						PolizzaMapper pm = new PolizzaMapper();
						ArrayList<Assicurato> assicurati = pm.find(nome_assicurato.getText(), cognome_assicurato.getText());

						String risultato [] = new String [3];
						modello_tabella.setRowCount(0);
						modello_tabella.setColumnCount(0);
						modello_tabella.addColumn("Id");
						modello_tabella.addColumn("Cognome");
						modello_tabella.addColumn("Nome");

						if (assicurati == null) {
							//metodo per controllare se la query ha restituito 0 risultati oppure almeno 1 risultato
							finestraZeroRisultatiCognomeNomeAssicurato(cognome_assicurato, nome_assicurato);
						}
						else {
							for (Assicurato a : assicurati) {
								risultato[0] = ((Integer) a.getIdAssicurato()).toString();
								risultato [1] = a.getCognome();
								risultato [2] = a.getNome();
								modello_tabella.addRow(risultato);
								risultato = new String [3];
							}
						}
					}
				}
			}
		});

		cerca_per_id.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				id_testo.setEditable(true);
				cognome_assicurato.setText("");
				cognome_assicurato.setEditable(false);
				nome_assicurato.setText("");
				nome_assicurato.setEditable(false);
			}
		});

		cerca_per_nome.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cognome_assicurato.setEditable(true);
				nome_assicurato.setEditable(true);
				id_testo.setText("");
				id_testo.setEditable(false);
			}
		});

		lista_risultati.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

				String selezione = modello_tabella.getDataVector().elementAt(lista_risultati.getSelectedRow()).toString();

				if (cerca_per_nome.isSelected()) {
					String [] parametri = selezione.split(",");

					ArrayList<Polizza> polizze = new PolizzaMapper().find(new Assicurato(Integer.parseInt(parametri[0].substring(1))));

					String res [] = new String [3];

					modello_tabella_descrizione.setRowCount(0);
					modello_tabella_descrizione.setColumnCount(0);
					modello_tabella_descrizione.addColumn("Id polizza");
					modello_tabella_descrizione.addColumn("Data del contratto");
					modello_tabella_descrizione.addColumn("Targa del veicolo");

					for(Polizza p : polizze){
						res [0] = p.getIdPolizza();
						res [1] = p.getDataContratto();
						res [2] = p.getTargaVeicolo();
						modello_tabella_descrizione.addRow(res);
						res = new String [3];
					}
					conteggio_polizze.setText("Numero di polizze: " + modello_tabella_descrizione.getRowCount());
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					String selezione = "";
					if (cerca_per_id.isSelected()) {
						selezione = modello_tabella.getDataVector().elementAt(lista_risultati.getSelectedRow()).toString();
						JFrame finestra = new InfoPolizzaGUI().creaFinestraInfoPolizza(selezione);
						Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
						finestra.setLocation(dim.width/4-finestra.getSize().width/4, dim.height/10-finestra.getSize().height/10);
						finestra.setSize(new Dimension(500, 300));
						finestra.setVisible(true);
					}
				}
			}
			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

		});

		lista_descrizione.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e1) {

				if (e1.getClickCount() == 2) {
					if (cerca_per_nome.isSelected()) {
						String selezione = modello_tabella_descrizione.getDataVector().elementAt(lista_descrizione.getSelectedRow()).toString();
						JFrame finestra = new InfoPolizzaGUI().creaFinestraInfoPolizza(selezione);
						Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
						finestra.setLocation(dim.width/4-finestra.getSize().width/4, dim.height/10-finestra.getSize().height/10);
						finestra.setSize(new Dimension(500, 300));
						finestra.setVisible(true);
					}
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {

			}

			@Override
			public void mouseExited(MouseEvent arg0) {

			}

			@Override
			public void mousePressed(MouseEvent arg0) {

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {

			}
		});
		return finestra; 
	}

	/** Questo metodo apre una finestra nel caso in cui non sono 
	 * stati trovati risultati tramite la ricerca per nome e cognome
	 * dell'assicurato.
	 * @param cognome_assicurato JTextField
	 * @param nome_assicurato JTextField
	 */
	private void finestraZeroRisultatiCognomeNomeAssicurato(JTextField cognome_assicurato,
															JTextField nome_assicurato) {

		/*Creo una nuova finestra di errore*/
		JFrame finestra_zero_risultati = new JFrame("Attenzione! Nessun risultato trovato!");
		finestra_zero_risultati.setSize(new Dimension(650, 100));
		finestra_zero_risultati.setLayout(new GridBagLayout());
		finestra_zero_risultati.setLocationRelativeTo(null); //per centrare la finestra
		finestra_zero_risultati.setVisible(true);

		JPanel pannello = new JPanel();

		JLabel etichetta_attenzione = new JLabel();

		if (cognome_assicurato.getText().equalsIgnoreCase("") && (!(nome_assicurato.getText().equalsIgnoreCase("")))) { 
			//cognome vuoto e nome non vuoto
			etichetta_attenzione.setText("Attenzione! Non esiste nessuna polizza il cui intestatario ha nome = " + nome_assicurato.getText() + " !");
		} else {
			if ((!(cognome_assicurato.getText().equalsIgnoreCase(""))) && (nome_assicurato.getText().equalsIgnoreCase(""))) { 
				//cognome non vuoto e nome vuoto
				etichetta_attenzione.setText("Attenzione! Non esiste nessuna polizza il cui intestatario ha cognome = " + cognome_assicurato.getText() + " !");
			} else {
				if ((!(cognome_assicurato.getText().equalsIgnoreCase(""))) && (!(nome_assicurato.getText().equalsIgnoreCase("")))) {
					//cognome non vuoto e nome non vuoto
					etichetta_attenzione.setText("Attenzione! Non esiste nesssuna polizza il cui intestatario ha cognome = " + cognome_assicurato.getText() +
												 " e nome = " + nome_assicurato.getText() + " !");
				}
			}
		}

		etichetta_attenzione.setVisible(true);
		etichetta_attenzione.setLocation(0,0);
		pannello.add(etichetta_attenzione);

		
		finestra_zero_risultati.add(pannello);

	}

	/** Questo metodo controlla che almeno un campo testuale sia stato inserito 
	 * per la ricerca.
	 * @param id_testo JTextField 
	 * @param cognome_assicurato JTextField
	 * @param nome_assicurato JTextField
	 * @return boolean
	 */
	private boolean controlloNessunaInformazioneInserita(JTextField id_testo, JTextField cognome_assicurato,
														 JTextField nome_assicurato) {

		boolean controllo_info = false;
		if (id_testo.getText().equalsIgnoreCase("") && (cognome_assicurato.getText().equalsIgnoreCase("") &&
				(nome_assicurato.getText().equalsIgnoreCase("")))) {

			controllo_info = true;

			/*Il bottone 'cerca' � stato premuto e contemporaneamente le 3 JTextField 'id_testo',
			 * 'cognome_assicurato' e 'nome_assicurato' sono vuote*/
			System.err.println("Le 3 JTextField sono vuote contemporaneamente!");

			/*Creo una nuova finestra di errore*/
			JFrame finestra_errore_JTextFieldVuote = new JFrame("Attenzione!");
			finestra_errore_JTextFieldVuote.setSize(new Dimension(500, 100));
			finestra_errore_JTextFieldVuote.setLayout(new GridBagLayout());
			finestra_errore_JTextFieldVuote.setLocationRelativeTo(null); //per centrare la finestra
			finestra_errore_JTextFieldVuote.setVisible(true);

			JPanel pannello = new JPanel();

			JLabel etichetta_attenzione = new JLabel();
			etichetta_attenzione.setText("Attenzione! Non � stata inserita nessuna informazione richiesta!");
			etichetta_attenzione.setVisible(true);
			etichetta_attenzione.setLocation(0,0);
			pannello.add(etichetta_attenzione);

		
		
			finestra_errore_JTextFieldVuote.add(pannello);

		}
		return controllo_info;
	}

	/** Questo metodo apre una finestra nel caso in cui non sono 
	 * stati trovati risultati tramite la ricerca per id polizza
	 * dell'assicurato.
	 * @param id_testo JTextField
	 */
	private void finestraZeroRisultatiIDPolizza(JTextField id_testo) {

		/*Creo una nuova finestra di errore*/
		JFrame finestra_zero_risultati = new JFrame("Attenzione! Nessun risultato trovato!");
		finestra_zero_risultati.setSize(new Dimension(750, 100));
		finestra_zero_risultati.setLayout(new GridBagLayout());
		finestra_zero_risultati.setLocationRelativeTo(null); //per centrare la finestra
		finestra_zero_risultati.setVisible(true);

		JPanel pannello = new JPanel();

		JLabel etichetta_attenzione = new JLabel();
		etichetta_attenzione.setText("Attenzione! Non esiste nesssuna polizza con ID = " + id_testo.getText() + " !");
		etichetta_attenzione.setVisible(true);
		etichetta_attenzione.setLocation(0,0);
		pannello.add(etichetta_attenzione);


		finestra_zero_risultati.add(pannello);

	
	}
}
