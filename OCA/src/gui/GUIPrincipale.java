package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Questa classe definisce l'intefaccia grafica utente che permette
 * la ricerca di informazioni per assicurato o per polizza.
 */

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class GUIPrincipale {

	/** Questo metodo fa partire l'interfaccia grafica dell'operatore.
	 * @param args String percorsi delle cartelle
	 */
	public static void main(String[] args) {

		/*Definizione dei widget della GUI*/
		JFrame scelta_iniziale = new JFrame("Ricerca");
		scelta_iniziale.setSize(new Dimension(350, 150));
		scelta_iniziale.setLayout(new GridBagLayout());
		scelta_iniziale.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		GridBagConstraints c = new GridBagConstraints();

		JLabel scelta_etichetta =  new JLabel("Cosa vuoi ricercare?");
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.insets = new Insets(5, 5, 5, 5);
		c.anchor = GridBagConstraints.PAGE_START;
		scelta_iniziale.add(scelta_etichetta,c);

		JButton assicurato = new JButton("Assicurato");
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		c.insets = new Insets(5, 5, 5, 5);
		c.anchor = GridBagConstraints.LINE_START;
		scelta_iniziale.add(assicurato,c);

		JButton polizza = new JButton("Polizza");
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(5, 5, 5, 5);
		c.anchor = GridBagConstraints.LINE_END;
		scelta_iniziale.add(polizza,c);

		/*Metodi actionListener() per il bottone Assicurato*/
		assicurato.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame finestra = new AssicuratoGUI().creaFinestraAssicurato();
				Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
				finestra.setLocation(dim.width/4-finestra.getSize().width/4,
									 dim.height/10-finestra.getSize().height/10);
				finestra.setSize(new Dimension(900, 900));
				finestra.setVisible(true);
			}
		});

		/*Metodi actionListener() per il bottone Polizza*/
		polizza.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame finestra = new PolizzaGUI().creaFinestraPolizza();
				Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
				finestra.setLocation(dim.width/4-finestra.getSize().width/4,
									 dim.height/10-finestra.getSize().height/10);
				finestra.setSize(new Dimension(900, 900));
				finestra.setVisible(true);
			}
		});
		scelta_iniziale.setVisible(true);
	}

}
