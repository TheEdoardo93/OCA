package gui;
import dataMapper.InfoPolizzaMapper;
import util.InfoPolizza;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Questa classe definisce l'intefaccia grafica utente che permette
 * la ricerca di informazioni per infopolizza.
 */

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class InfoPolizzaGUI {
	
	/** Questo metodo crea una finestra con le informazioni relative
	 * alla selezione.
	 * @param selezione String
	 * @return JFrame
	 */
	public JFrame creaFinestraInfoPolizza(String selezione) {
		
		/*Definizione dei widget per la GUI*/
		int x = 0;
		int y = 0;
		JFrame finestra = new JFrame();
		finestra.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		JLabel cognomeNomeAssicurato = new JLabel();
		JLabel targaVeicolo = new JLabel();
		JLabel kmTotali_etichetta = new JLabel("Chilometri totali:");
		JLabel kmPrudenti_etichetta = new JLabel("Chilometri prudenti:");
		JLabel velMediaUrbana_etichetta = new JLabel("VelocitÓ media urbana:");
		JLabel velMediaExtraUrbana_etichetta= new JLabel("VelocitÓ media extraurbana:");
		JLabel velMediaAutostrada_etichetta = new JLabel("VelocitÓ media autostrada:");
		JTextField kmTotali_testo = new JTextField(5);
		JTextField kmPrudenti_testo = new JTextField(5);
		JTextField velMediaUrbana_testo = new JTextField(5);
		JTextField velMediaExtraUrbana_testo = new JTextField(5);
		JTextField velMediaAutostrada_testo = new JTextField(5);

		kmTotali_testo.setEditable(false);
		kmPrudenti_testo.setEditable(false);
		velMediaUrbana_testo.setEditable(false);
		velMediaExtraUrbana_testo.setEditable(false);
		velMediaAutostrada_testo.setEditable(false);

		c.gridx = x;
		c.gridx = y;
		c.insets = new Insets(5,5,5,5);
		c.anchor = GridBagConstraints.LINE_END;
		finestra.add(cognomeNomeAssicurato, c);

		c.gridx = x;
		c.gridx = y+1;
		c.insets = new Insets(5,5,5,5);
		finestra.add(targaVeicolo, c);

		c.gridx = x;
		c.gridy = y+2;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(kmTotali_etichetta, c);

		c.gridx = x+1;
		c.gridy = y+2;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(kmTotali_testo, c);

		c.gridx=x;
		c.gridy=y+3;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(kmPrudenti_etichetta,c);

		c.gridx=x+1;
		c.gridy=y+3;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(kmPrudenti_testo,c);

		c.gridx=x;
		c.gridy=y+4;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(velMediaUrbana_etichetta,c);

		c.gridx=x+1;
		c.gridy=y+4;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(velMediaUrbana_testo,c);

		c.gridx=x;
		c.gridy=y+5;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(velMediaExtraUrbana_etichetta,c);

		c.gridx=x+1;
		c.gridy=y+5;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(velMediaExtraUrbana_testo,c);

		c.gridx=x;
		c.gridy=y+6;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(velMediaAutostrada_etichetta,c);

		c.gridx=x+1;
		c.gridy=y+6;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(velMediaAutostrada_testo,c);

		String [] parametri = selezione.split(",");

		InfoPolizza infopolizza = new InfoPolizzaMapper().find(parametri[0].substring(1));

		if (infopolizza != null) {
			cognomeNomeAssicurato.setText("Cognome e nome: " + infopolizza.getCognomeNome());
			targaVeicolo.setText("Targa veicolo: " + infopolizza.getTarga());
			kmTotali_testo.setText(infopolizza.getChilometriTotali());
			kmPrudenti_testo.setText(infopolizza.getChilometriTotaliPrudenza());
			velMediaUrbana_testo.setText(infopolizza.getVelocitÓMediaUrbana());
			velMediaExtraUrbana_testo.setText(infopolizza.getVelocitÓMediaExtraUrbana());
			velMediaAutostrada_testo.setText(infopolizza.getVelocitÓMediaAutostrada());
			finestra.setTitle("Informazioni della polizza numero " + parametri[0].substring(1));
		}
		return finestra; 
	}
}
