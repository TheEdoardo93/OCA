package gui;
import dataMapper.AssicuratoMapper;
import util.Assicurato;
//import calcolastatistiche.LivelloDiRischio;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import calcoloLivelloRischio.GestoreLivelloDiRischio;

/**
 * Questa classe definisce l'intefaccia grafica utente che permette
 * la ricerca di informazioni di un assicurato.
 */

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class AssicuratoGUI {

	/*Definizione dei widget per la finestra Assicurato*/
	int ultimo_assicurato_selezionato = -1;

	/** Questo metodo apre l'interfaccia grafica relativa alla visualizzazione
	 * delle informazioni dell'assicurato.
	 * @return JFrame
	 */
	public JFrame creaFinestraAssicurato() {
		int x = 0;
		int y = 0;
		JFrame finestra = new JFrame("Ricerca per assicurato");
		finestra.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		/*Radio button della finestra Assicurato*/
		JRadioButton cerca_per_id = new JRadioButton("Cerca assicurato per id");
		JRadioButton cerca_per_nome = new JRadioButton("Cerca assicurato per nome e cognome");

		ButtonGroup gruppo_radio_button = new ButtonGroup();
		cerca_per_id.setSelected(true);
		gruppo_radio_button.add(cerca_per_id);
		gruppo_radio_button.add(cerca_per_nome);
		c.gridx = x;
		c.gridy = y;
		c.gridwidth=1;

		finestra.add(cerca_per_id, c);
		finestra.add(cerca_per_nome);

		/*JPanel identificativo dell'assicurato*/
		JPanel pannello_id = new JPanel(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
		TitledBorder border = new TitledBorder("Id Assicurato");
		pannello_id.setBorder(border);

		JLabel id_etichetta = new JLabel("Id");
		c1.gridx = x;
		c1.gridy = y;
		c1.insets = new Insets(20, 5, 20, 5);
		pannello_id.add(id_etichetta, c1);

		JTextField id_testo = new JTextField(20);
		c1.gridx = x+1;
		c1.gridy = y;
		c1.insets = new Insets(20, 5, 20, 5);
		pannello_id.add(id_testo, c1);

		c.gridx=x;
		c.gridy=y+1;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(pannello_id,c);

		/*JPanel nome e cognome dell'assicurato*/
		JPanel pannello_assicurato = new JPanel(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();
		TitledBorder border_assicurato = new TitledBorder("Assicurato");
		pannello_assicurato.setBorder(border_assicurato);

		JLabel cognome = new JLabel("Cognome");
		c2.gridx = 0;
		c2.gridy = 0;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(cognome, c2);

		JTextField cognome_assicurato = new JTextField(20);
		cognome_assicurato.setEditable(false);
		c2.gridx = 1;
		c2.gridy = 0;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(cognome_assicurato, c2);

		JLabel nome = new JLabel("Nome");
		c2.gridx = 0;
		c2.gridy = 1;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(nome, c2);

		JTextField nome_assicurato = new JTextField(20);
		nome_assicurato.setEditable(false);
		c2.gridx = 1;
		c2.gridy = 1;
		c2.insets = new Insets(5, 5, 5, 5);
		pannello_assicurato.add(nome_assicurato, c2);

		c.gridx=x+1;
		c.gridy=y+1;
		c.insets = new Insets(5, 5, 5, 5);
		finestra.add(pannello_assicurato,c);

		/*JButton cerca*/
		JButton cerca = new JButton("Cerca");
		c.gridx = x;
		c.gridy = y+3;
		c.gridwidth=4;
		finestra.add(cerca, c);

		JPanel pannello_risultati = new JPanel(new GridBagLayout());
		GridBagConstraints c3 = new GridBagConstraints();

		/*Lista selezione risultati*/
		DefaultTableModel modello_tabella = new DefaultTableModel();
		JTable lista_risultati = new JTable(modello_tabella);
		lista_risultati.setDefaultEditor(Object.class, null);
		lista_risultati.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane listScroller = new JScrollPane(lista_risultati);
		listScroller.setViewportView(lista_risultati);
		listScroller.setPreferredSize(new Dimension(200, 400));

		c3.gridx = 0;
		c3.gridy = 0;
		pannello_risultati.add(listScroller, c3);

		/*Lista descrizione risultati*/
		DefaultListModel<String> modello_lista_descrizione = new DefaultListModel<String>();
		JList <String> lista_descrizione = new JList <String>(modello_lista_descrizione);
		lista_descrizione.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane listScroller2 = new JScrollPane(lista_descrizione);
		listScroller2.setViewportView(lista_descrizione);
		listScroller2.setPreferredSize(new Dimension(400, 400));

		c3.gridx=1;
		c3.gridy=0;
		pannello_risultati.add(listScroller2, c3);

		c.gridx=x;
		c.gridy=y+4;
		c.gridwidth=4;
		c.insets = new Insets(10, 10, 10, 10);
		finestra.add(pannello_risultati, c);

		/*Metodi actionListener()*/

		cerca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				/*Questo metodo consente di controllare se l'utente non ha inserito nessuna delle 3 informazioni
				(id assicurato, cognome assicurato, nome assicurato)*/
				boolean controllo_info = controlloNessunaInformazioneInserita(id_testo, cognome_assicurato,
						nome_assicurato);

				if (!controllo_info) {

					AssicuratoMapper am = new AssicuratoMapper();

					modello_lista_descrizione.removeAllElements();

					if (!(id_testo.getText().equals(""))) {
						Assicurato a = am.find(id_testo.getText());

						modello_tabella.setRowCount(0);
						modello_tabella.setColumnCount(0);
						modello_tabella.addColumn("Id");
						modello_tabella.addColumn("Cognome");
						modello_tabella.addColumn("Nome");

						if (a == null) {
							//metodo per controllare se la query ha restituito 0 risultati oppure almeno 1 risultato
							finestraZeroRisultatiIDAssicurato(id_testo); 
						}
						else {
							String[] risultato = new String[3];

							risultato[0] = ((Integer)a.getIdAssicurato()).toString();
							risultato [1] = a.getCognome();
							risultato [2] = a.getNome();
							modello_tabella.addRow(risultato);
						}
					}
					else {

						ArrayList<Assicurato> assicurati = new AssicuratoMapper().find(nome_assicurato.getText(),
								cognome_assicurato.getText());

						String risultato [] = new String [3];

						modello_tabella.setRowCount(0);
						modello_tabella.setColumnCount(0);
						modello_tabella.addColumn("Id");
						modello_tabella.addColumn("Cognome");
						modello_tabella.addColumn("Nome");

						if (assicurati == null) {
							finestraZeroRisultatiCognomeNomeAssicurato(cognome_assicurato, nome_assicurato);
						}
						else {

							for (Assicurato a : assicurati) {
								risultato[0] = ((Integer)a.getIdAssicurato()).toString();
								risultato [1] = a.getCognome();
								risultato [2] = a.getNome();
								modello_tabella.addRow(risultato);
								risultato = new String [3];
							}
						}
					}
				}
			}

		});

		/*Radio button della ricerca per identificativo dell'assicurato*/
		cerca_per_id.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				id_testo.setEditable(true);
				cognome_assicurato.setText("");
				cognome_assicurato.setEditable(false);
				nome_assicurato.setText("");
				nome_assicurato.setEditable(false);
			}
		});

		/*Radio button della ricerca per nome e cognome dell'assicurato*/
		cerca_per_nome.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cognome_assicurato.setEditable(true);
				nome_assicurato.setEditable(true);
				id_testo.setText("");
				id_testo.setEditable(false);
			}
		});

		/*Metodo mouseListener()*/
		lista_risultati.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

				String selezione = modello_tabella.getDataVector().elementAt(lista_risultati.getSelectedRow()).toString();

				String [] parametri = selezione.split(",");

				int idAssicurato = Integer.parseInt(parametri[0].substring(1));

				if (ultimo_assicurato_selezionato != idAssicurato) {

					ultimo_assicurato_selezionato=idAssicurato;

					GestoreLivelloDiRischio rischio = new GestoreLivelloDiRischio();

					int livelloDiRischio = rischio.restituisciAggiornaLivelloRischio(idAssicurato);

					modello_lista_descrizione.removeAllElements();

					Assicurato a = new AssicuratoMapper().getInfo(ultimo_assicurato_selezionato);

					modello_lista_descrizione.addElement("Id Assicurato: " + a.getIdAssicurato());
					modello_lista_descrizione.addElement("Nome: " + a.getNome());					
					modello_lista_descrizione.addElement("Cognome: " + a.getCognome());
					modello_lista_descrizione.addElement("Livello di rischio: " + livelloDiRischio);
					modello_lista_descrizione.addElement("Data di nascita: " + a.getDataNascita());
					modello_lista_descrizione.addElement("Comune: " + a.getComuneResidenza());
					modello_lista_descrizione.addElement("Provincia: " + a.getProvincia());
					modello_lista_descrizione.addElement("Regione: " + a.getRegione());
					modello_lista_descrizione.addElement("CAP: " + a.getCap());
					modello_lista_descrizione.addElement("Telefono: " + a.getTelefono());
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}
		});
		return finestra; 
	}

	/** Questo metodo controlla che almeno un campo testuale non sia vuoto.
	 * @param id_testo JTextField
	 * @param cognome_assicurato JTextField
	 * @param nome_assicurato JTextField
	 * @return boolean
	 */
	private boolean controlloNessunaInformazioneInserita(JTextField id_testo, JTextField cognome_assicurato,
			JTextField nome_assicurato) {

		boolean informazioni = false;

		if (id_testo.getText().equalsIgnoreCase("") && (cognome_assicurato.getText().equalsIgnoreCase("") &&
				(nome_assicurato.getText().equalsIgnoreCase("")))) {

			informazioni = true;

			/*Il bottone 'cerca' � stato premuto e contemporaneamente le 3 JTextField 'id_testo', 'cognome_assicurato' e
			 * 'nome_assicurato' sono vuote*/

			/*Creo una nuova finestra di errore*/
			JFrame finestra_errore_JTextFieldVuote = new JFrame("Attenzione!");
			finestra_errore_JTextFieldVuote.setSize(new Dimension(500, 100));
			finestra_errore_JTextFieldVuote.setLayout(new GridBagLayout());
			finestra_errore_JTextFieldVuote.setLocationRelativeTo(null); //per centrare la finestra
			finestra_errore_JTextFieldVuote.setVisible(true);

			JPanel pannello = new JPanel();

			JLabel etichetta_attenzione = new JLabel();
			etichetta_attenzione.setText("Attenzione! Non � stata inserita nessuna informazione richiesta!");
			etichetta_attenzione.setVisible(true);
			etichetta_attenzione.setLocation(0,0);
			pannello.add(etichetta_attenzione);
			finestra_errore_JTextFieldVuote.add(pannello);

			/*Se viene premuto il bottone, allora si chiude la finestra appena creata*/
			//chiudiFinestraZeroRisultati(finestra_errore_JTextFieldVuote, bottone_uscita);
		}
		return informazioni;
	}


	/** Questo metodo apre una finestra che avvisa che non sono stati restituiti
	 * dei risultati nel caso di ricerca per cognome e nome di un assicurato
	 * @param cognome_assicurato JTextField
	 * @param nome_assicurato JTextField
	 */
	private void finestraZeroRisultatiCognomeNomeAssicurato(JTextField cognome_assicurato, JTextField nome_assicurato) {
		
		/*Creo una nuova finestra di errore*/
		JFrame finestra_zero_risultati = new JFrame("Attenzione! Nessun risultato trovato!");
		finestra_zero_risultati.setSize(new Dimension(750, 100));
		finestra_zero_risultati.setLayout(new GridBagLayout());
		finestra_zero_risultati.setLocationRelativeTo(null); //per centrare la finestra
		finestra_zero_risultati.setVisible(true);

		JPanel pannello = new JPanel();

		JLabel etichetta_attenzione = new JLabel();

		if (cognome_assicurato.getText().equalsIgnoreCase("") && (!(nome_assicurato.getText().equalsIgnoreCase("")))) { 
			//cognome vuoto e nome non vuoto
			etichetta_attenzione.setText("Attenzione! Non esiste nessun assicurato con nome = " + nome_assicurato.getText() + " !");
		} else {
			if ((!(cognome_assicurato.getText().equalsIgnoreCase(""))) && (nome_assicurato.getText().equalsIgnoreCase(""))) {
				//cognome non vuoto e nome vuoto
				etichetta_attenzione.setText("Attenzione! Non esiste nessun assicurato con cognome = " + cognome_assicurato.getText() + " !");
			} else {
				if ((!(cognome_assicurato.getText().equalsIgnoreCase(""))) && (!(nome_assicurato.getText().equalsIgnoreCase("")))) {
					//cognome non vuoto e nome non vuoto
					etichetta_attenzione.setText("Attenzione! Non esiste nesssun assicurato con cognome = " + cognome_assicurato.getText() +
							" e nome = " + nome_assicurato.getText() + " !");
				}
			}
		}

		etichetta_attenzione.setVisible(true);
		etichetta_attenzione.setLocation(0,0);
		pannello.add(etichetta_attenzione);

		finestra_zero_risultati.add(pannello);

	}

	/** Questo metodo apre una finestra che avvisa che non sono stati restituiti
	 * dei risultati nel caso di ricerca per id di un assicurato
	 * @param id_testo String
	 */
	private void finestraZeroRisultatiIDAssicurato(JTextField id_testo) {

		/*Creo una nuova finestra di errore*/
		JFrame finestra_zero_risultati = new JFrame("Attenzione! Nessun risultato trovato!");
		finestra_zero_risultati.setSize(new Dimension(450, 100));
		finestra_zero_risultati.setLayout(new GridBagLayout());
		finestra_zero_risultati.setLocationRelativeTo(null); //per centrare la finestra
		finestra_zero_risultati.setVisible(true);

		JPanel pannello = new JPanel();

		JLabel etichetta_attenzione = new JLabel();
		etichetta_attenzione.setText("Attenzione! Non esiste nessun assicurato con ID = " + id_testo.getText() + " !");
		etichetta_attenzione.setVisible(true);
		etichetta_attenzione.setLocation(0,0);
		pannello.add(etichetta_attenzione);

		finestra_zero_risultati.add(pannello);

	}
	
}
