package entryPoint;

import java.io.File;

import elaborazioneTragitto.ElaborazioneTragitti;

/**
 * Questa classe � l'entry point del programma per l'elaborazione dei file della scatola nera.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */
public class CalcoloStatistiche {

	/**
	 * Questo metodo gestisce l'intero processo. Partendo dai file sensore,
	 * elabora le statistiche del tragitto compiuto e le scrive nel database OCB.
	 * Il parametro args contiene:
	 * 
	 * args[0] = Freematics/
	 * args[1] = Freematics_elaborati/
	 * args[2] = Tragitti_temporanei/
	 * args[3] = tragitti_noDB/
	 * args[4] = OsmCache/
	 * 
	 * @param args String[]
	 */
	public static void main(String[] args) {
		
		// Creazione delle cartelle ricevute come parametro se non esistono
				for (int i=0; i< args.length;i++){
					File dir = new File(args[i]);
					if(!dir.exists()){
						dir.mkdir();
					}
				}
		ElaborazioneTragitti elabTragitti = new ElaborazioneTragitti();
		
		//Se sono presenti nuovi file sensori, li elabora
		elabTragitti.elaboraDatiSensori(args);
	}

}
