package dataMapper;

import util.Assicurato;
import util.DBConnection;
import util.Polizza;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.Ignore;

/**
 * Questa classe funge da dataMapper per l'oggetto polizza.
 */

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class PolizzaMapper {

	/**
	 * Questo metodo interroga il database OCB per ricercare un assicurato
	 * tramite nome o cognome o entrambi. La ricerca potenzialmente pu�
	 * restituire pi� assicurati. Viene istanziato un oggetto Assicurato per
	 * ogni risultato della query e viene restituito un ArrayList di assicurati.
	 * 
	 * @param nome String
	 * @param cognome String
	 * @return assicurati ArrayList di Assicurato
	 */

	public ArrayList<Assicurato> find(String nome, String cognome) {

		ArrayList<Assicurato> assicurati = new ArrayList<Assicurato>();

		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getOcbConnection();
			
			// query al database OCB
			String query = "SELECT id_assicurato, nome, cognome FROM ocb.assicurato";
			boolean n = false;
			boolean c = false;

			if (!(cognome.equals(""))) {
				// se viene inserito solo il cognome dell'assicurato
				query = query + " where cognome = ?";
				c = true;
				if (!(nome.equals(""))) {
					// se viene inserito sia il nome sia il cognome
					// dell'assicurato
					query = query + " AND nome= ?";
					n = true;
				}
			} else {
				if (!(nome.equals(""))) {
					// se viene inserito solo il nome dell'assicurato
					query = query + " where nome= ?";
					n = true;
				}
			}
			preparedStatement = conn.prepareStatement(query);

			if (c == true) {
				preparedStatement.setString(1, cognome);
				if (n == true) {
					preparedStatement.setString(2, nome);
				}
			} else {
				if (n == true) {
					preparedStatement.setString(1, nome);
				}
			}

			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				assicurati.add(new Assicurato(rs.getInt("id_assicurato"),
											  rs.getString("cognome"), rs.getString("nome")));
			}
			conn.close();
			if (assicurati.size() == 0)
				assicurati = null;
			return assicurati;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Questo medodo interroga OCB ed istanzia un oggetto contente le
	 * informazioni (id, data del contratto, targa del veicolo) associate alla
	 * polizza il cui id corrisponde al parametro in ingresso al metodo.
	 * 
	 * @param idPolizza String
	 * @return polizza Polizza
	 */
	public Polizza find(String idPolizza) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getOcbConnection();
			Polizza polizza = null;
			
			//se l'identificativo della polizza � presente
			if (!(idPolizza.equals(""))) {
				//query al database OCB
				String query = "select ocb.infopolizza.id_polizza, ocb.infopolizza.dataContratto,"
						+ "ocb.infopolizza.targa from ocb.infopolizza where infopolizza.id_polizza = ?";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setString(1, idPolizza);
				ResultSet rs = preparedStatement.executeQuery();

				while (rs.next()) {
					polizza = new Polizza(idPolizza, rs.getString("dataContratto"), rs.getString("targa"));
				}
			}
			conn.close();
			return polizza;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Questo medodo interroga OCB e ricerca tutte le polizze associtate ad un
	 * determinato assicurato. La ricerca pu� potenzialmente restituire pi�
	 * polizze. Viene istanziato un oggetto contente le informazioni (id, data
	 * del contratto, targa del veicolo) per ogni polizza associtato
	 * all'assicurato in ingresso al metodo.
	 * 
	 * @param assicurato Assicurato
	 * @return polizze ArrayList di Polizza
	 */
	public ArrayList<Polizza> find(Assicurato assicurato) {

		int id_assicurato = assicurato.getIdAssicurato();
		
		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getOcbConnection();
			ArrayList<Polizza> polizze = new ArrayList<Polizza>();

			if (id_assicurato!=-1) { //se l'identificativo dell'assicurato � presente
				//query al database OCB
				String query = "select ocb.infopolizza.id_polizza, ocb.infopolizza.dataContratto,"
						+ "ocb.infopolizza.targa from ocb.infopolizza where infopolizza.id_assicurato = ?";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setInt(1, id_assicurato);
				ResultSet rs = preparedStatement.executeQuery();

				while (rs.next()) {
					polizze.add(new Polizza(rs.getString("id_polizza"), rs.getString("dataContratto"),
											rs.getString("targa")));
				}
			}
			conn.close();
			return polizze;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * Questo metodo cancella la polizza dal database OCB corrispondente alla polizza
	 * passata in ingresso al metodo.
	 * 
	 * @param polizza Polizza
	 */
	public void delete(Polizza polizza) {
		
		//se l'identificativo della polizza � presente
		if (polizza.getIdPolizza() != null || polizza.getIdPolizza() != "") { 
			Connection conn = null;
			
			try {
				conn = new DBConnection().getOcbConnection();
				
				//query di cancellazione di una polizza dal database OCB
				String query = "DELETE FROM ocb.polizza WHERE ocb.polizza.id_polizza = ?";
				PreparedStatement preparedStmt = conn.prepareStatement(query);
				preparedStmt.setInt(1, Integer.parseInt(polizza.getIdPolizza()));
				preparedStmt.execute();

				conn.close();
			} catch (SQLException e) {
				System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
				System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
				System.err.println("Attenzione: stato SQL = " + e.getSQLState());
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.err.println("Attenzione: non � stato inserito l'identificativo della polizza!");
		}

	}

}
