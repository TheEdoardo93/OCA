package dataMapper;
import util.DBConnection;
import util.InfoPolizza;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.Ignore;

/**
 * Questa classe funge da dataMapper per l'oggetto InfoPolizza.
 */

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class InfoPolizzaMapper {

	/**
	 * Questo metodo interroga il database OCB per ricercare tutte le 
	 * informazioni relative alla polizza selezionata. Istanzia l'oggetto
	 * rappresentante queste informazioni e lo restituisce.
	 * 
	 * @param id_polizza String
	 * @return ip Infopolizza
	 */
	public InfoPolizza find (String id_polizza) {
		PreparedStatement preparedStatement = null;
		Connection conn = null;
		
		try {
			conn = new DBConnection().getOcbConnection();

			//query al database OCB
			String query = "select kmTotali, kmPercorsiInPrudenza, velocitaMediaSuUrbana,"
					+ "velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada, infopolizza.id_infopolizza,"
					+ "assicurato.nome, assicurato.cognome, targa from ocb.infopolizza JOIN ocb.assicurato "
					+" ON ocb.infopolizza.id_assicurato = ocb.assicurato.id_assicurato "
					+ " where infopolizza.id_polizza= ?";
			
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, id_polizza);
			ResultSet rs = preparedStatement.executeQuery();
			
			if (rs.next()) {
				InfoPolizza ip = new InfoPolizza(rs.getString("cognome") + " " + rs.getString("nome"),
						rs.getString("targa"), rs.getString("kmTotali"), rs.getString("kmPercorsiInPrudenza"),
						rs.getString("velocitaMediaSuUrbana"), rs.getString("velocitaMediaSuExtraurbana"),
						rs.getString("velocitaMediaSuAutostrada"));
				conn.close();
				return ip;
			}
			
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Ignore
	public void update (InfoPolizza ip) {

	}
	
	@Ignore
	public void delete (InfoPolizza ip) {

	}

}
