package dataMapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

import util.DBConnection;
import util.Tragitto;



/**
* Questa classe funge da dataMapper per l'oggetto Tragitto.

* @author Casiraghi Edoardo
* @author Palmiero Marco
* @author Terragni Silvia
*
*/
public class TragittoMapper {
	
	
	/** Questo metodo serve per inserire un nuovo tragitto nel database OCB.
	 * @param tragitto Tragitto
	 * @param file_tragitto_temp File
	 * @param tragitti_noDB String
	 * @return boolean
	 */
	public boolean insert(Tragitto tragitto, File file_tragitto_temp, String tragitti_noDB) {
		Connection conn = null;
		boolean salvataggio_avvenuto = false;
		System.out.println("Salvataggio di un tragitto nel database OCB " + file_tragitto_temp.getName());

		try {
			//connessione al database OCB
			conn = new DBConnection().getOcbConnection();
			
			//query
			String query_select = "select ocb.infopolizza.id_infopolizza from ocb.infopolizza where attivo = 1 AND "
					+ "targa = ? order by ocb.infopolizza.dataContratto DESC limit 1";
			
			PreparedStatement preparedStmtSelect = conn.prepareStatement(query_select);
			
			 //la targa del veicolo che ha effettuato il tragitto � il parametro della query
			preparedStmtSelect.setString(1, tragitto.getTarga());
			
			ResultSet rs = preparedStmtSelect.executeQuery(); //risultato ottenuto tramite la query
			
			int id_infopolizza = -1;
			while (rs.next()) {
				id_infopolizza = rs.getInt("id_infopolizza");
			}

			if (id_infopolizza != -1) {
				//query che inserisce nel database OCB il tragitto con tutte le sue statistiche calcolate in precedenza
				String query_insert = "insert into tragitto (data, kmPercorsi, kmPrudenti, velocitaMediaSuUrbana, "
						+ "velocitaMediaSuExtraurbana, velocitaMediaSuAutostrada, id_infopolizza)"
						+ " values (?,?,?,?,?,?,?)";

				caricaTragittiCSVsuOCB(conn, tragitti_noDB, query_insert);

				java.sql.Date data = new java.sql.Date(tragitto.getData().getTime());

				/*Impostare i parametri della query*/
				
				PreparedStatement preparedStmtInsert = conn.prepareStatement(query_insert);
				preparedStmtInsert.setDate(1, data);
				preparedStmtInsert.setDouble(2, tragitto.getChilometriTotaliPercorsi());
				preparedStmtInsert.setDouble(3, tragitto.getChilometriPercorsiInPrudenza());

				if(tragitto.getVelocit�MediaUrbana() != -1)
					preparedStmtInsert.setDouble(4, tragitto.getVelocit�MediaUrbana());
				else
					preparedStmtInsert.setNull(4,java.sql.Types.DOUBLE);

				if(tragitto.getVelocit�MediaExtraurbana() != -1)
					preparedStmtInsert.setDouble(5, tragitto.getVelocit�MediaExtraurbana());
				else
					preparedStmtInsert.setNull(5, java.sql.Types.DOUBLE);

				if(tragitto.getVelocit�MediaAutostrada() != -1)
					preparedStmtInsert.setDouble(6, tragitto.getVelocit�MediaAutostrada());
				else preparedStmtInsert.setNull(6, java.sql.Types.DOUBLE);

				preparedStmtInsert.setInt(7, id_infopolizza);
				preparedStmtInsert.execute();
			} else {
				System.err.println("Attenzione: nessuna polizza associata alla targa!");
			}
			
			conn.close(); //chiusura della connessione al database OCB
			salvataggio_avvenuto = true;
		
			
		} catch (ClassNotFoundException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		} catch (CommunicationsException e){
			System.err.println("Tragitti salvati temporaneamente su file");
			salvaTragitto_noDB(tragitto, file_tragitto_temp.getName(), tragitti_noDB);
			file_tragitto_temp.delete();
		} catch (SQLException e) {
			System.err.println("Errore nel salvataggio in OCB. Il tragitto viene salvato temporaneamente su file .csv!");
			file_tragitto_temp.delete();
			salvataggio_avvenuto = false;
		}
		return salvataggio_avvenuto;
	}
	
	/** Questo metodo carica i file csv temporanei contenenti le statistiche dei tragitti 
	 * sul database OCB e rimuove i file csv.
	 * 
	 * @param conn Connection 
	 * @param tragitti_noDB String
	 * @param query String
	 * @throws SQLException
	 */
	private void caricaTragittiCSVsuOCB(Connection conn, String tragitti_noDB, String query) throws SQLException {
		
		//directory dove vengono memorizzati i tragitti che non sono stati memorizzati nel database OCB
		File dir = new File(tragitti_noDB);
		
		for (File file : dir.listFiles()) {
			Scanner s;
			try {
				s = new Scanner(file);

				String[] linea_tragitto = s.nextLine().split(";");
				s.close();
				
				//connessione al database OCB
				conn = new DBConnection().getOcbConnection();
				
				//query al database OCB
				String query_select = "select ocb.infopolizza.id_infopolizza from ocb.infopolizza where attivo = 1 AND "
						+ " targa = ? order by ocb.infopolizza.dataContratto DESC limit 1";
				
				PreparedStatement preparedStmtSelect = conn.prepareStatement(query_select);
				preparedStmtSelect.setString(1, linea_tragitto[6]);
				ResultSet rs = preparedStmtSelect.executeQuery(); //risultato della query eseguita sul database OCB
				
				int id_infopolizza = -1;
				while(rs.next()) {
					id_infopolizza = rs.getInt("id_infopolizza");
				}

				if (id_infopolizza != -1) {	
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					Date date = null;
					try {
						date = formatter.parse(linea_tragitto[0]);
					} catch (ParseException e) {
						System.err.println("Attenzione: problemi nel formato della data!");
						System.err.println(e.getMessage());
					}

					java.sql.Date data = new java.sql.Date(date.getTime());

					/*Impostare i parametri della query*/
					PreparedStatement preparedStmt = conn.prepareStatement(query);
					preparedStmt.setDate(1, data);
					preparedStmt.setDouble(2, Double.parseDouble(linea_tragitto[1]));
					preparedStmt.setDouble(3, Double.parseDouble(linea_tragitto[2]));

					if(Double.parseDouble(linea_tragitto[3]) != -1)
						preparedStmt.setDouble(4, Double.parseDouble(linea_tragitto[3]));
					else
						preparedStmt.setNull(4,java.sql.Types.DOUBLE);

					if(Double.parseDouble(linea_tragitto[4]) != -1)
						preparedStmt.setDouble(5, Double.parseDouble(linea_tragitto[4]));
					else
						preparedStmt.setNull(5, java.sql.Types.DOUBLE);

					if(Double.parseDouble(linea_tragitto[5]) != -1)
						preparedStmt.setDouble(6, Double.parseDouble(linea_tragitto[5]));
					else 
						preparedStmt.setNull(6, java.sql.Types.DOUBLE);

					preparedStmt.setInt(7, id_infopolizza);
					preparedStmt.execute();
				}
			} catch (FileNotFoundException e) {
				System.err.println("Attenzione: non � stato trovato il file!");
				System.err.println(e.getMessage());
			} catch (ClassNotFoundException e1) {
				System.err.println("Attenzione: ClassNotFoundException!");
				System.err.println(e1.getMessage());
			} catch (IOException e2) {
				System.err.println("Attenzione: IOException!");
				System.err.println(e2.getMessage());
			}

			/*cancellazione del file .csv che indica un tragitto non salvato sul
			 * database OCB perch� ora salvato sul database OCB*/
			file.delete();
		}
	}

	/**
	 * Questo metodo salva in un file con formato .csv le statistiche finali del tragitto
	 * ricevuto come parametro in ingresso.
	 * 
	 * @param tragitto Tragitto 
	 * @param nomeFile_tragitto_temp String 
	 * @param tragitti_noDB 
	 */
	private void salvaTragitto_noDB(Tragitto tragitto, String nomeFile_tragitto_temp, String tragitti_noDB) { 

		java.sql.Date data = new java.sql.Date(tragitto.getData().getTime());

		String file_csv = tragitti_noDB + nomeFile_tragitto_temp;
		try {
			FileWriter writer = new FileWriter(file_csv, true);
			
			//informazioni di un tragitto da salvare nel file in formato .csv
			String line = data.toString() + ";" + tragitto.getChilometriTotaliPercorsi() + ";" 
					+ tragitto.getChilometriPercorsiInPrudenza() + ";" + tragitto.getVelocit�MediaUrbana() 
					+ ";" + tragitto.getVelocit�MediaExtraurbana() + ";"
					+ tragitto.getVelocit�MediaAutostrada() + ";"+ tragitto.getTarga() + "\n";
			
			writer.append(line); //scrittura su file .csv del tragitto
			writer.close();
		} catch (IOException e) {
			System.err.println("Attenzione: problemi di I/O nella scrittura di un tragitto sul file .csv!");
			System.err.println(e.getMessage());
		}
	}


}
