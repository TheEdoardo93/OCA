package dataMapper;
import util.Assicurato;
import util.DBConnection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Questa classe funge da dataMapper per l'oggetto Assicurato.
 */

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class AssicuratoMapper {

	/**
	 * Questo metodo interroga il database OCB per ricercare un assicurato
	 * tramite nome o cognome o entrambi. La ricerca potenzialmente pu� restituire 
	 * pi� assicurati. Viene istanziato un oggetto Assicurato per ogni risultato 
	 * della query e viene restituito un ArrayList di assicurati.
	 * 
	 * @param nome String
	 * @param cognome String
	 * @return assicurati ArrayList di Assicurato
	 */
	public ArrayList<Assicurato> find(String nome, String cognome) {

		ArrayList<Assicurato> assicurati = new ArrayList<Assicurato>();

		PreparedStatement preparedStatement = null;
		Connection conn = null;
		try {
			conn = new DBConnection().getOcbConnection();
			
			//query al database OCB
			String query = "SELECT id_assicurato, nome, cognome FROM ocb.assicurato";
			boolean n = false;
			boolean c = false;

			if (!(cognome.equals(""))) {
				//se viene inserito solo il cognome dell'assicurato
				query = query + " where cognome = ?";
				c = true;
				if (!(nome.equals(""))) {
					//se viene inserito sia il nome sia il cognome dell'assicurato
					query = query + " AND nome= ?"; 
					n = true;
				}
			} else {
				if (!(nome.equals(""))) {
					//se viene inserito solo il nome dell'assicurato
					query = query + " where nome= ?";
					n = true;
				}
			}
			
			preparedStatement = conn.prepareStatement(query);

			if (c == true) {
				preparedStatement.setString(1, cognome);
				if (n == true) {
					preparedStatement.setString(2, nome);
				}
			} else {
				if (n == true) {
					preparedStatement.setString(1, nome);
				}
			}
			
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				//i risultati della query (Assicurati) vengono aggiunti all'ArrayList
				assicurati.add(new Assicurato(rs.getInt("id_assicurato"),
											  rs.getString("cognome"), rs.getString("nome")));
			}
			conn.close();
			if (assicurati.size() == 0)
				assicurati = null;
			return assicurati;
			
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Questo metodo interroga il database OCB per ricercare un assicurato
	 * tramite il suo identificativo. Viene istanziato l'oggetto Assicurato
	 * se trovato e viene restituito.
	 * 
	 * @param idAssicurato String
	 * @return assicurato Assicurato 
	 */
	public Assicurato find(String idAssicurato) {
		
		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getOcbConnection();
			Assicurato assicurato = null;

			if (!(idAssicurato.equals(""))) { //se l'identificativo dell'assicurato � presente
				//query al database OCB
				String query = "select nome, cognome from ocb.assicurato where assicurato.id_assicurato = ?";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setString(1, idAssicurato);
				ResultSet rs = preparedStatement.executeQuery();

				while (rs.next()) {
					assicurato = new Assicurato(Integer.parseInt(idAssicurato), rs.getString("cognome"), rs.getString("nome"));
				}
			}
			conn.close();
			return assicurato;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Questo metodo, una volta che dall'interfaccia grafica utente viene selezionato
	 * l'assicurato di interesse, recupera dai database OCB e BDA tutte le 
	 * informazioni anangrafiche associate all'assicurato. Istanzia e restituisce
	 * l'oggetto rappresentante l'assicurato, completo delle informazioni reperite.
	 * 
	 * @param ultimo_assicurato_selezionato int
	 * @return assicurato Assicurato
	 */
	public Assicurato getInfo(int ultimo_assicurato_selezionato ) {
		
		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getOcbConnection();
			Assicurato assicurato = null;

			if (!(ultimo_assicurato_selezionato == -1)) { //se l'identificativo dell'assicurato � presente
				//query al database BDA ed al database OCB per recuperare le informazioni dell'assicurato
				String query = "select ocb.assicurato.id_assicurato, ocb.assicurato.nome,"
						+ " ocb.assicurato.cognome, ocb.assicurato.livelloRischio, bda.assicurato.dataNascita,"
						+ " bda.assicurato.id_residenza, comune, provincia, regione, cap, telefono"
						+ " from ocb.assicurato join bda.assicurato"
						+ " on bda.assicurato.id_assicurato = ocb.assicurato.id_assicurato,"
						+ " bda.comune where ocb.assicurato.id_assicurato = ? AND"
						+ " bda.assicurato.id_residenza=comune.id_comune";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setInt(1, ultimo_assicurato_selezionato );
				ResultSet rs = preparedStatement.executeQuery();

				while (rs.next()) {
					assicurato = new Assicurato(ultimo_assicurato_selezionato , rs.getString("cognome"), rs.getString("nome"),
							rs.getString("dataNascita"), rs.getInt("livelloRischio"), rs.getString("comune"),
							rs.getString("provincia"), rs.getString("regione"), rs.getString("cap"),
							rs.getString("telefono"));
				}
			}
			conn.close();
			return assicurato;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	
	}

	/**
	 * Questo metodo effettua un aggiornamento sul database OCB delle informazioni dell'assicurato
	 * contenute nell'oggetto Assicurato ricevuto come parametro in input al metodo.
	 * 
	 * @param assicurato Assicurato
	 */
	public void update(Assicurato assicurato) {
		
		if (assicurato.getIdAssicurato() != -1) {
			Connection conn = null;
			
			try {
				conn = new DBConnection().getOcbConnection();
				//query di aggiornamento delle informazioni dell'assicurato nel database OCB
				String query = "UPDATE ocb.assicurato SET ocb.assicurato.telefono = ? "
						+ "WHERE ocb.assicurato.id_assicurato";
				PreparedStatement preparedStmt = conn.prepareStatement(query);
				preparedStmt.setString(1, assicurato.getTelefono());
				preparedStmt.setInt(2, assicurato.getIdAssicurato());
				preparedStmt.execute();

				conn.close();
			} catch (SQLException e) {
				System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
				System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
				System.err.println("Attenzione: stato SQL = " + e.getSQLState());
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.err.println("Attenzione: non � stato inserito l'identificativo dell'assicurato!");
		}

	}

	/**
	 * Questo metodo cancella da OCB le informazioni relative all'assicurato ricevuto
	 * in input a questo metodo.
	 * 
	 * @param assicurato Assicurato
	 */
	public void delete(Assicurato assicurato) {
		
		if (assicurato.getIdAssicurato() != -1) {
			Connection conn = null;
			
			try {
				conn = new DBConnection().getOcbConnection();
				//query di cancellazione al database OCB
				String query = "DELETE FROM ocb.assicurato WHERE ocb.assicurato.id_assicurato = ?";
				PreparedStatement preparedStmt = conn.prepareStatement(query);
				preparedStmt.setInt(1, assicurato.getIdAssicurato());
				preparedStmt.execute();

				conn.close();
			} catch (SQLException e) {
				System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
				System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
				System.err.println("Attenzione: stato SQL = " + e.getSQLState());
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.err.println("Attenzione: non � stato inserito l'identificativo dell'assicurato!");
		}

	}

}
