package dataMapper;
import util.DBConnection;
import util.InformazioniAssicurato;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Questa classe funge da dataMapper per il livello di rischio.

/**
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 *
 */

public class LivelloDiRischioMapper {
	
	/** Questo metodo restituisce le informazioni associate ad un asssicurato
	 * prelevandole dai database BDA e OCB
	 * @param idAssicurato int
	 * @return informazioniAssicurato
	 */
	public InformazioniAssicurato findInformazioniAssicurato(int idAssicurato) {
		InformazioniAssicurato ia = new InformazioniAssicurato(idAssicurato);
		ia = updateInformazioniAssicuratoBda(ia);
		ia = updateInformazioniAssicuratoOcb(ia);
		
		return ia;
	}

	/**
	 * Questo metodo si occupa di cercare e aggiornare le informazioni associate ad un assicurato,
	 * ricevuto in input come parametro, presenti nel database BDA.
	 * 
	 * @param ia InformazioniAssicurato
	 * @return informazioniAssicurato
	 */
	public InformazioniAssicurato updateInformazioniAssicuratoBda(InformazioniAssicurato ia) {

		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getBdaConnection();
			String query_bda = "SELECT bda.assicurato.id_assicurato as id, COUNT(*) as c,"
					+ " dataNascita as data, provincia as prov"
					+ " FROM bda.assicurato left JOIN bda.denunciasinistro ON"
					+ " bda.assicurato.id_assicurato = bda.denunciasinistro.id_assicurato,"
					+ " bda.comune WHERE bda.assicurato.id_assicurato = ? AND"
					+ " bda.assicurato.id_residenza = bda.comune.id_comune"
					+ "	AND bda.denunciasinistro.colpa > 0;";

			preparedStatement = conn.prepareStatement(query_bda);
			preparedStatement.setInt(1, ia.getIdAssicurato());
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				ia.setNumeroDenunce(rs.getInt("c"));
				ia.setDataNascita(rs.getString("data"));
				ia.setSiglaProvincia(rs.getString("prov"));
			}
			conn.close();
			return ia;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Questo metodo si occupa di cercare e aggiornare le informazioni associate ad un assicurato,
	 * ricevuto in input come parametro, presenti nel database OCB.
	 * 
	 * @param ia InformazioniAssicurato
	 * @return informazioniAssicurato 
	 */
	public InformazioniAssicurato updateInformazioniAssicuratoOcb(InformazioniAssicurato ia) {

		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query_ocb = "SELECT SUM(infopolizza.kmTotali) as k,"
					+ " SUM(ocb.infopolizza.kmPercorsiInPrudenza) as kp,"
					+ " AVG(ocb.infopolizza.velocitaMediaSuUrbana) as vu,"
					+ " AVG(ocb.infopolizza.velocitaMediaSuExtraurbana) as ve,"
					+ " AVG(ocb.infopolizza.velocitaMediaSuAutostrada) as va,"
					+ " ANY_VALUE(ocb.rischioprovincia.rischio) as rischio"
					+ "	FROM ocb.infopolizza, ocb.rischioprovincia"
					+ "	WHERE infopolizza.id_assicurato = ? AND"
					+ " ocb.rischioprovincia.siglaProvincia = ?;";
			preparedStatement = conn.prepareStatement(query_ocb);
			preparedStatement.setInt(1, ia.getIdAssicurato());
			preparedStatement.setString(2, ia.getSiglaProvincia());
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {			
					ia.setChilometriTotali(Math.round(rs.getDouble("k") * 1000) / 1000.0);
					
					double kp = Math.round(rs.getDouble("kp") * 1000) / 1000.0;
				
					ia.setVelocitÓMediaSuUrbana(Math.round(rs.getDouble("vu") * 1000) / 1000.0);
				
					ia.setVelocitÓMediaSuExtraurbana(Math.round(rs.getDouble("ve") * 1000) / 1000.0);
				
					ia.setVelocitÓMediaSuAutostrada(Math.round(rs.getDouble("va") * 1000) / 1000.0);
				
				if(ia.getChilometriTotali() != 0)
					ia.setLivelloPrudenza(Math.round(kp/ia.getChilometriTotali() * 100) / 100.0);
				else
					ia.setLivelloPrudenza(100);
				ia.setRischioProvincia(rs.getInt("rischio"));
			}

			conn.close();
			return ia;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Questo metodo si occupa di salvare il livello di rischio di un assicurato
	 * in modo persistente nel database OCB.
	 * 
	 * @param ia InformazioniAssicurato
	 * @return risultatoSalvataggio boolean
	 */
	public boolean saveLivelloRischioOcb(InformazioniAssicurato ia) {

		boolean risultatoSalvataggio = false;
		PreparedStatement preparedStatement = null;
		Connection conn = null;

		try {
			conn = new DBConnection().getOcbConnection();
			String query = "UPDATE ocb.assicurato SET livelloRischio = " + ia.getRischio() +
					" WHERE ocb.assicurato.id_assicurato = " + ia.getIdAssicurato();
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.execute();
			conn.close();
			risultatoSalvataggio = true;
			return risultatoSalvataggio;
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();
		}
		risultatoSalvataggio = false;
		return risultatoSalvataggio;
	}

	/**
	 * Questo metodo si occupa di aggiornare i parametri utili per il calcolo del livello di rischio
	 * di un assicurato nel database OCB.
	 * 
	 * @param idAssicurato int
	 * @return risultatoAggiornamento boolean
	 */
	public boolean updateParametri(int idAssicurato) {

		boolean risultatoAggiornamento = false;

		PreparedStatement preparedStatement = null;
		Connection conn_ocb = null;
		try {
			conn_ocb = new DBConnection().getOcbConnection();
			String query_ocb = "SELECT targa, infopolizza.id_infopolizza, SUM(ocb.tragitto.kmPercorsi) as k, "
					+ " SUM(ocb.tragitto.kmPrudenti) as kp, AVG(ocb.tragitto.velocitaMediaSuUrbana) as vu, " 
					+ " AVG(ocb.tragitto.velocitaMediaSuExtraurbana) as ve, AVG(ocb.tragitto.velocitaMediaSuAutostrada) as va"
					+ "	FROM ocb.tragitto JOIN ocb.infopolizza on tragitto.id_infopolizza = infopolizza.id_infopolizza"
					+ "	WHERE infopolizza.id_assicurato = ? "
					+ " GROUP BY targa, infopolizza.id_infopolizza";

			preparedStatement = conn_ocb.prepareStatement(query_ocb);
			preparedStatement.setInt(1, idAssicurato);
			ResultSet rs = preparedStatement.executeQuery();

			String query_update = "UPDATE ocb.infopolizza SET ocb.infopolizza.kmTotali = ?,"
					+ " ocb.infopolizza.kmPercorsiInPrudenza = ?,"
					+ " ocb.infopolizza.velocitaMediaSuUrbana = ?,"
					+ " ocb.infopolizza.velocitaMediaSuExtraurbana = ?,"
					+ " ocb.infopolizza.velocitaMediaSuAutostrada = ?"
					+ " WHERE ocb.infopolizza.id_infopolizza = ?";

			while (rs.next()) {
				PreparedStatement preparedStmt = conn_ocb.prepareStatement(query_update);

				preparedStmt.setDouble(1, Math.round(Double.parseDouble(rs.getString("k")) * 1000) / 1000.0);
				preparedStmt.setDouble(2, Math.round(Double.parseDouble(rs.getString("kp")) * 1000) / 1000.0);

				if (rs.getString("vu") == null) {
					preparedStmt.setNull(3, java.sql.Types.DOUBLE);
				} else {
					preparedStmt.setDouble(3, Math.round(Double.parseDouble(rs.getString("vu")) * 1000) / 1000.0);
				}

				if (rs.getString("ve") == null) {
					preparedStmt.setNull(4, java.sql.Types.DOUBLE);
				}
				else {
					preparedStmt.setDouble(4, Math.round(Double.parseDouble(rs.getString("ve")) * 1000) / 1000.0);
				}

				if (rs.getString("va") == null) {
					preparedStmt.setNull(5, java.sql.Types.DOUBLE);
				} else {
					preparedStmt.setDouble(5, Math.round(Double.parseDouble(rs.getString("va")) * 1000) / 1000.0);
				}	

				preparedStmt.setInt(6, Integer.parseInt(rs.getString("infopolizza.id_infopolizza")));
				preparedStmt.execute();	
				conn_ocb.close();
				risultatoAggiornamento = true;
				return risultatoAggiornamento;
			}
		} catch (SQLException e) {
			System.err.println("Attenzione: messaggio SQL = " + e.getMessage());
			System.err.println("Attenzione: codice di errore SQL = " + e.getErrorCode());
			System.err.println("Attenzione: stato SQL = " + e.getSQLState());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();
		}
		risultatoAggiornamento = false;
		return risultatoAggiornamento;
	}
}
