package elaborazioneTronchi;
import java.util.ArrayList;

import util.DatiSensori;
import util.Tronco;

/**
 * Questa classe si occupa di elaborare i tronchi calcolando la velocitÓ media, i chilometri percorsi
 * ed i chilometri percorsi in prudenza.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */
public class ElaborazioneTronchi {

	/** Questo metodo riceve in ingresso un ArrayList contente i tronchi individuati
	 * e ne calcola la velocitÓ media, i chilometri totali percorsi e i chilometri 
	 * totali percorsi in prudenza attraverso i rispettivi metodi dedicati.
	 * 
	 * @param tronchi ArrayList di Tronchi
	 * @return tronchi ArrayList di Tronchi
	 */
	public ArrayList<Tronco> calcolaParametriTronchi(ArrayList<Tronco> tronchi) {
		
		/* Per ogni tronco facente parte di un tragitto percorso da un veicolo, vengono
		 * calcolate le statistiche.
		 */
		for (Tronco tronco : tronchi) {
			tronco.setVelocitÓMediaTronco(calcolaVelocitÓMedia(tronco));
			tronco.setChilometriTotaliTronco(calcolaChilometri(tronco));
			tronco.setChilometriInPrudenzaTronco(calcolaChilometriPrudenza(tronco));
		}
		return tronchi;
	}

	/**
	 * Questo metodo calcola i chilometri totali relativi ad ogni Tronco t ricevuto 
	 * come parametro di ingresso. 
	 * @param tronco Tronco
	 * @return chilometriTronco double
	 */
	public double calcolaChilometri(Tronco tronco) {
		
		double chilometriTronco = 0;

		if (tronco.getDatiSensori().size() > 1) {
			for (int i = 1; i < tronco.getDatiSensori().size(); i++) {
				//calcolo la distanza in chilometrica tra due coppie di coordinate geografiche
				chilometriTronco += calcolaDistanzaTraCoordinate(tronco.getDatiSensori().get(i - 1),
													 			 tronco.getDatiSensori().get(i));
			}
		}
		return chilometriTronco;
	}

	/**
	 * Questo metodo calcola i chilometri totali percorsi in prudenza relativi ad ogni 
	 * Tronco t ricevuto come parametro di ingresso. 
	 * @param tronco Tronco
	 * @return chilometriInPrudenza double
	 */
	public double calcolaChilometriPrudenza(Tronco tronco) {
		
		double velocitaMassima = tronco.getVelocitÓMassima() + 10; // tolleranza di 10km/h
		double velocitaCorrente = -1;
		double velocitaPrecedente = -1;
		double kmCount = 0;
		double chilometriInPrudenza = 0;

		if (tronco.getDatiSensori().size() > 1) {
			for (int i = 1; i < tronco.getDatiSensori().size(); i++) {
				DatiSensori datoSensorePrecedente = tronco.getDatiSensori().get(i - 1);
				DatiSensori datoSensoreCorrente = tronco.getDatiSensori().get(i);
				velocitaCorrente = datoSensoreCorrente.getVelocitÓ();
				velocitaPrecedente = datoSensorePrecedente.getVelocitÓ();
				
				if (velocitaPrecedente <= velocitaMassima) {
					if (velocitaCorrente <= velocitaMassima) {
						chilometriInPrudenza +=
							calcolaDistanzaTraCoordinate(datoSensorePrecedente, datoSensoreCorrente);
					} else {
						chilometriInPrudenza +=
							calcolaTrattoPrudenza(datoSensorePrecedente, datoSensoreCorrente, velocitaMassima, kmCount);
					}
				} else {
					if (velocitaCorrente < velocitaMassima) {
						chilometriInPrudenza +=
								calcolaTrattoPrudenza(datoSensorePrecedente, datoSensoreCorrente, velocitaMassima, kmCount);
					}
				}
				
				kmCount += calcolaDistanzaTraCoordinate(datoSensorePrecedente, datoSensoreCorrente);
			}
		}
		return chilometriInPrudenza;
	}

	/**
	 * Questo metodo calcola la velocitÓ media relativa a un Tronco t, ricevuto 
	 * come parametro di ingresso. Se la tipologia di tronco fosse "non_disponibile",
	 * la velocitÓ media viene inizializzata al valore "-1" di default. Se la velocitÓ
	 * rilevata in una rilevazione Ŕ minore o uguale a zero, non viene considerata 
	 * nel calcolo della velocitÓ media 
	 * 
	 * @param tronco Tronco
	 * @return velocitÓMediaTronco double
	 */
	public double calcolaVelocitÓMedia(Tronco tronco) {
		
		double velocitaMediaTronco = 0;
		if (!(tronco.getTipologia().equals("non_disponibile"))) {
			int count = 0;
			
			for (int i = 0; i < tronco.getDatiSensori().size() - 1; i++) {
				double vel = tronco.getDatiSensori().get(i).getVelocitÓ();
				if (vel > 0) { // si considerano solo le velocitÓ istantanee > 0
					velocitaMediaTronco += tronco.getDatiSensori().get(i).getVelocitÓ();
					count++;
				}
			}
			
			if (count > 0) {
				velocitaMediaTronco /= count;
			} else {
				velocitaMediaTronco = -1;
			}
		} else {
			//se la tipologia del tronco considerato Ŕ "non disponibile", imposto come velocitÓ media il valore '-1'
			velocitaMediaTronco = -1;
		}

		return velocitaMediaTronco;
	}

	/**
	 * Questo metodo determina la distanza (in metri) tra due coppie di coordinate. I dati sensore
	 * ds1 e ds2 contengono le coppie di coordinate di cui si vuole calcolare lo scostamento. 
	 * 
	 * @param datoSensore_1 DatiSensori
	 * @param datoSensore_2 DatiSensori
	 * @return distanza tra coordinate double
	 */
	private double calcolaDistanzaTraCoordinate(DatiSensori datoSensore_1, DatiSensori datoSensore_2) {
		
		//considero latitudine e longitudine delle due coppie di coordinate geografiche che si considerano
		double latitudine_precedente = datoSensore_1.getLatitudine()*2*Math.PI/360;
		double longitudine_precedente = datoSensore_1.getLongitudine()*2*Math.PI/360;
		double latitudine_corrente = datoSensore_2.getLatitudine()*2*Math.PI/360;
		double longitudine_corrente = datoSensore_2.getLongitudine()*2*Math.PI/360;
		
		/*Calcolo della distanza tra due coppie di coordinate geografiche*/
		double distanza = Math.acos(Math.sin(latitudine_precedente) * Math.sin(latitudine_corrente) + 
						  Math.cos(latitudine_precedente) * Math.cos(latitudine_corrente)
						  	* Math.cos(Math.max(longitudine_corrente, longitudine_precedente)
						  - Math.min(longitudine_precedente, longitudine_corrente))) * 6371;

		return distanza;
	}

	/**
	 * Questo metodo viene invocato quando le velocitÓ instantanee di due dati sensore consecutivi 
	 * risultano essere una minore e una maggiore rispetto al limite di velocitÓ vMax (o viceversa).
	 * Si determina la retta passante per due i punti (le velocitÓ istantaee di ds1 e di ds2). 
	 * Sostituendo vMax si ottiene il valore di chilometro in cui le velocitÓ dei sensori e la vMax sono uguali. 
	 * Il metodo restituisce la frazione di chilometri percorsi in prudenza da sommare ai chilometri cumulativi
	 * in prudenza percorsi fino a ds1 (kmPrev).
	 * Questo procedimento consente di determinare in modo preciso l'ammontare dei chilometri percorsi in 
	 * prudenza. Nel caso in cui la velocitÓ di ds1 fosse maggiore della velocitÓ di ds2, si procede applicando 
	 * specularmente il procedimento appena illustrato. 
	 * 
	 * @param datoSensore_1 DatiSensori
	 * @param datoSensore_2 DatiSensori
	 * @param velocitaMassima double
	 * @param kmPrecedenti double
	 * @return trattoPrudenza double 
	 */
	private double calcolaTrattoPrudenza(DatiSensori datoSensore_1, DatiSensori datoSensore_2,
			double velocitaMassima, double kmPrecedenti) {
		
		double trattoPrudenza = 0;
		
		double velocitaCorrente = datoSensore_2.getVelocitÓ();
		double velocitaPrecedente = datoSensore_1.getVelocitÓ();
		double kmCorrenti = calcolaDistanzaTraCoordinate(datoSensore_1, datoSensore_2) + kmPrecedenti;
		double intersezione = (velocitaMassima - velocitaCorrente) * (kmPrecedenti - kmCorrenti) /
				(velocitaPrecedente - velocitaCorrente) + kmCorrenti;

		if (velocitaCorrente > velocitaPrecedente) {
			trattoPrudenza = intersezione - kmPrecedenti;
			return trattoPrudenza;
		} else {
			trattoPrudenza = kmCorrenti - intersezione;
			return trattoPrudenza;
		}
	}

}
