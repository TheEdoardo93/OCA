package elaborazioneTragitto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Calendar;

import javax.management.MalformedObjectNameException;

/**
 * A partire dai dati sensori grezzi, crea un file in formato .csv con la descrizione delle
 * informazioni di ogni dato sensore.
 * Il formato di output avr� come prima riga la targa del veicolo e le righe successive,
 * dove ogni riga rappresenta una rilevazione sensoristica, sono formate dai seguenti campi:
 * 
 * 	data;latitudine;longitudine;velocit� rilevata;indirizzo;velocit� massima;tipologia di strada 
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */

public class Freematics {

	/**
	 * Questo metodo legge tutti i file presenti all'interno della cartella Freematics,
	 * li elabora e li salva.
	 *
	 * @see salva_csv_tragitto
	 * 
	 * @param percorso_freematics String
	 * @param percorso_freematics_elaborati String
	 * @param percorso_tragitti_temporanei String
	 * @param percorso_osmCache String
	 */
	public void scansioneCartellaFreematics(String percorso_freematics, String percorso_freematics_elaborati,
			String percorso_tragitti_temporanei, String percorso_osmCache) {

		File dir = new File(percorso_freematics);

		//scansione di tutti i file presenti nella cartella /Freematics che rappresentano i file Freematics
		for (File file_freematics : dir.listFiles()) {
			salva_csv_tragitto(file_freematics.getPath(),percorso_tragitti_temporanei, 
					percorso_freematics_elaborati, percorso_osmCache);
		}
	}

	/**
	 * Questo metodo elabora il contenuto di un file Freematics 
	 * (@see http://freematics.com/pages/software/freematics-data-logging-format/)
	 * e restituisce una lista di array di stringhe. 
	 * 
	 * Ogni array contiene:
	 * Pos 0:data
	 * Pos 1: ora
	 * Pos 2: latitudine
	 * Pos 3: longitudine
	 * Pos 4: Velocit�
	 * Pos 5: targa
	 * 
	 * @param percorso_file_freematics String
	 * @return rilevazioni ArrayList di String[]
	 */
	private ArrayList<String[]> letturaCSVFreematics(String percorso_file_freematics) {
		String file_freematics = new File(percorso_file_freematics).getName();
		ArrayList<String[]> rilevazioni = new ArrayList<>();

		String targa = file_freematics.replaceAll("(^[a-z A-Z 0-9]{3,10})_.+", "$1");

		// se targa malformata viene ritornato l'arraylist rilevazioni vuoto
		if(targa.equals(file_freematics)){
			try {
				throw new MalformedObjectNameException();
			} catch (MalformedObjectNameException e) {
				new File(percorso_file_freematics).delete();
				System.err.println("Attenzione: targa malformata!");
			}
			return rilevazioni;
		}
			
		LineNumberReader lnr = null;
		String line = "";
		String csvSplitBy = ",";
		String[] out = new String[6];
		String data = "";

		try {
			lnr = new LineNumberReader(new FileReader(percorso_file_freematics));
			boolean trovato = false;

			//Lettura del file freematics
			while ((line = lnr.readLine()) != null) {
				String[] linea = line.split(csvSplitBy);
				String type = linea[1].toLowerCase();

				switch (type) {
				//tag usato da Freematics per indicare l'ora della rilevazione
				case "tim":
					if (!trovato) {
						out[1] = linea[2].substring(0, 2) + "-" + linea[2].substring(2, 4) + "-"
								+ linea[2].substring(4, 6) + "-" + linea[2].substring(6);
						trovato = true;
					} else {
						out[5] = targa;
						if (Double.parseDouble(out[2]) != 999 && Double.parseDouble(out[3]) != 999)
							rilevazioni.add(out);
						out = new String[6];
						out[1] = linea[2].substring(0, 2) + "-" + linea[2].substring(2, 4) + "-"
								+ linea[2].substring(4, 6) + "-" + linea[2].substring(6);
						out[0] = data;
					}
					break;

					//tag usato da Freematics per indicare la velocit� istantanea
				case "spd":
					out[4] = linea[2];
					break;

					//tag usato da Freematics per indicare la latitudine (senza il punto)
				case "lat":
					double lat = Double.parseDouble(linea[2])/1000000; //6 cifre decimali
					if (lat >=-90 && lat <=90)
						out[2] = Double.toString(lat);
					else out[2] = Double.toString(999); //errore formattazione coordinata
					break;

					//tag usato da Freematics per indicare la longitudine (senza il punto)
				case "lng":
					double lon = Double.parseDouble(linea[2])/1000000; //6 cifre decimali
					if(lon >=-90 && lon <=90)
						out[3] = Double.toString(lon);
					else out[3] = Double.toString(999); //errore formattazione coordinata					
					break;

					//tag usato da Freematics per indicare la data della rilevazione
				case "dte":

					data = linea[2].substring(0, 2) + "-" + linea[2].substring(2, 4) + "-" + linea[2].substring(4);
					if (!data.matches("[0-9]{2,2}-[0-9]{2,2}-[0-9]{2,4}")) { //se la data � malformata
						Calendar c = Calendar.getInstance();	
						data = c.get(Calendar.DAY_OF_MONTH) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + (c.get(Calendar.YEAR)-2000);
					}
					out[0] = data;
					break;
				}
			}
			out[5] = targa;

			//se le coordinate sono corrette, viene aggiunta la lista delle rilevazioni all'ArrayList
			if (Double.parseDouble(out[2]) != 999 && Double.parseDouble(out[3]) != 999)
				rilevazioni.add(out);

		} catch (FileNotFoundException e) {
			System.err.println("Attenzione: file non trovato!");
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println("Attenzione: problemi di I/O con il file!");
			System.err.println(e.getMessage());
		} catch (NullPointerException e){
			try {
				lnr.close();
			} catch (IOException e1) {
				System.err.println("Attenzione: IOException!");
				e1.printStackTrace();
			}
			new File(percorso_file_freematics).delete();
			System.err.println("Attenzione: file vuoto!");
			System.err.println(e.getMessage());

		} finally {
			if (lnr != null) {
				try {
					lnr.close(); //chiusura del LineNumberReader
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		}
		return rilevazioni;
	}

	/**
	 * Questo metodo si occupa di scrivere su file le rilevazioni, estratte dal
	 * metodo @see lettura_csv, dopo averle elaborate.
	 * Il formato della stringa scritta nel csv �:
	 * 
	 * data;latitudine;longitudine;velocit�_rilevata;via,citt�;vel_max;tipologia
	 * 
	 * @param percorso_file_freematics String
	 * @param percorso_file_tragitti_temporanei String
	 * @param percorso_file_freematics_elaborati String
	 * @param osm_cache String
	 */
	private void salva_csv_tragitto(String percorso_file_freematics, String percorso_file_tragitti_temporanei, 
			String percorso_file_freematics_elaborati, String osm_cache) {

		ArrayList<String[]> rilevazioni = letturaCSVFreematics(percorso_file_freematics);

		if (!rilevazioni.isEmpty()) {
			ReverseGeocoding o = new ReverseGeocoding();
			String data = rilevazioni.get(0)[0];
			String ora = rilevazioni.get(0)[1];
			String targa = rilevazioni.get(0)[5];
			String file_name = percorso_file_tragitti_temporanei + "tragitto"+ data + "_"+ ora +".csv";

			try {
				//Cancellazione del contenuto del file (se pre-esistente)
				FileWriter fileWriter = new FileWriter(file_name, true);

				fileWriter.append(targa + "\n");

				for (String[] rilevazione : rilevazioni) {
					/*Chiamata a OpenStreetMap per la conversione da coordinate ad indirizzo 
					per ogni rilevazione identificata*/

					String[] riga_rilevazione = o.getInformazioniTronco(rilevazione[2], rilevazione[3], osm_cache);
					if (riga_rilevazione != null) {
						//Formato della riga_csv:  data;lat;long;vel_rilevata;via,citt�;vel_max;tipologia

						String riga_csv = rilevazione[0] + ";" + rilevazione[2] + ";" + rilevazione[3] + ";" +
								rilevazione[4] + ";" + riga_rilevazione[0] + ", " + riga_rilevazione[1] + ";" +
								riga_rilevazione[2] + ";" + riga_rilevazione[3] + "\n";
						fileWriter.append(riga_csv);
					}
				}
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.err.println("Attenzione: errore nella chiusura del fileWriter!");
				System.err.println(e.getMessage());
			} catch (InterruptedException e) {
				System.err.println(e.getMessage());
			}

			/*Terminata la conversione delle coordinate, il file viene spostato nella cartella /Freematics_elaborati*/

			File csvFile = new File(percorso_file_freematics);
			if (!csvFile.renameTo(new File(percorso_file_freematics_elaborati + csvFile.getName()))) {
				System.err.println("Errore: il file non � stato spostato!");
			}
		}
	}
}
