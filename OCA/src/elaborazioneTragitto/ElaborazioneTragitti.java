package elaborazioneTragitto;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dataMapper.TragittoMapper;
import elaborazioneTronchi.ElaborazioneTronchi;
import util.DatiSensori;
import util.Tragitto;
import util.Tronco;

/**
 * Questa classe si occupa di creare e salvare su database OCB le statistiche dei tragitti
 * a partire dai dati sensori rilevati tramite la scatola nera posizionata su un veicolo.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */
public class ElaborazioneTragitti {

	/**
	 * Questo metodo si occupa di leggere i dati sensore registrati dal dispositivo
	 * Freematics, li converte e salva il risultato su file. 
	 * Per ogni file attiva la procedura di elaborazione dei tragitti.
	 * 
	 * @param args String[] 
	 */
	public void elaboraDatiSensori(String[] args) {

		// ricezione file dalla scatola nera
		Freematics freem = new Freematics();

		freem.scansioneCartellaFreematics(args[0], args[1], args[2],args[4]);
		System.out.println("Elaborazione Freematics completata!");

		File cartella_tragitti_temp = new File(args[2]); 
		String targa = "";

		for (File file_tragitto_temp : cartella_tragitti_temp.listFiles()) {
			BufferedReader br = null;

			try {
				br = new BufferedReader(new FileReader(file_tragitto_temp.getPath()));

				//estrazione della targa memorizzata nella prima riga del file
				targa = br.readLine();

				br.close();
				gestioneTragitto(file_tragitto_temp, targa, args[3]);

			} catch (IOException e) { //problemi di I/O nella lettura del file
				System.err.println(e.getMessage());
			}
		}
	}

	/**
	 * Questo metodo si occupa di gestire i dati sensore, creando, elaborando e salvando 
	 * il tragitto finale associato.
	 *  
	 * @param file_tragitto_temp File
	 * @param targa String
	 * @param percorso_tragitti_noDB String
	 */
	private void gestioneTragitto(File file_tragitto_temp, String targa, String percorso_tragitti_noDB) {

		ElaborazioneTronchi elabTronchi = new ElaborazioneTronchi();

		//divisione dei dati sensori in tronchi
		ArrayList <Tronco> tronchi = dividiInTronchi(lettura_csv_tragitti_temporanei(file_tragitto_temp.getPath()));

		//calcolo dei parametri dei tronchi
		tronchi = elabTronchi.calcolaParametriTronchi(tronchi);

		//Salvataggio dell'elaborazione 
		Tragitto tragitto = elaboraTragitto(tronchi, targa);
		if (tragitto != null)
				new TragittoMapper().insert(tragitto, file_tragitto_temp, percorso_tragitti_noDB);
				file_tragitto_temp.delete();
	}

	/**
	 * Questo metodo legge i dati elaborati dei sensori (data, latitudine,
	 * longitudine, velocitÓ istantanea, indirizzo, velocitÓ massima e tipologia 
	 * di strada). Per ogni riga letta, crea un oggetto di tipo DatiSensori, contenente 
	 * le informazioni estratte e normalizzate ( @see controllaLinea(String[] linea)) 
	 * per maggiori dettagli. Infine, istanzia l'arrayList tragitto, e lo restituisce. 
	 *
	 * @param percorso_tragitti_temp String
	 * @return tragitto ArrayList di DatiSensori
	 */
	private ArrayList<DatiSensori> lettura_csv_tragitti_temporanei(String percorso_tragitti_temp) {

		ArrayList<DatiSensori> tragitto = new ArrayList<>();
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";";
		try {
			//lettura del file .csv
			br = new BufferedReader(new FileReader(percorso_tragitti_temp));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] linea_rilevazione = line.split(csvSplitBy);

				//controllo e normalizzazione della riga letta
				DatiSensori d = controllaRilevazione(linea_rilevazione);

				if (d != null) // stringa ben formata 
					tragitto.add(d);
			}
		} catch (FileNotFoundException e) { //nel caso in cui non viene trovato il file
			System.err.println("Attenzione: non Ŕ stato trovato il file al percorso '"
					+ percorso_tragitti_temp + "' !");
			e.printStackTrace();
		} catch (IOException e) { //problemi di I/O nella lettura del file
			System.err.println("Attenzione: problemi di I/O nella lettura del file!");
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Attenzione: problemi con la chiusura del BufferedReader!");
					e.printStackTrace();
				}
			}
		}
		return tragitto;
	}

	/**
	 * Questo metodo controlla se sono presenti campi vuoti per l'arrary linea di tipo String.
	 * Se un campo Ŕ vuoto, lo inizializza al valore "-1". Ogni campo rappresenta un dato
	 * catturato dal sensore. Viene inizializzato a "-1" per uniformare gli attributi della 
	 * classe DatiSensori. Se la linea Ŕ corrotta (contiene un numero di campi diverso da 7 o
	 * i campi non sono nel formato corretto), allora viene restituito null.
	 *
	 * @param linea_rilevazione String[] 
	 * @return datiSensori DatiSensori
	 */
	private DatiSensori controllaRilevazione(String[] linea_rilevazione) {

		//controllo numero di campi della linea
		if(linea_rilevazione.length != 7) 
			return null;

		//normalizzazione degli attributi della linea
		for (int i = 0; i < linea_rilevazione.length; i++) { 
			if (linea_rilevazione[i].isEmpty())
				linea_rilevazione[i] = "-1";
		}

		//riempimento dei valori mancanti in una linea che indica una singola rilevazione
		linea_rilevazione = riempiValoriMancanti(linea_rilevazione);

		try {
			//creazione di un oggetto di classe DatiSensori
			DatiSensori datiSensori = new DatiSensori(linea_rilevazione[0],Double.parseDouble(linea_rilevazione[1]),
					Double.parseDouble(linea_rilevazione[2]), Double.parseDouble(linea_rilevazione[3]),
					linea_rilevazione[4], Integer.parseInt(linea_rilevazione[5]), linea_rilevazione[6]);
			return datiSensori;
		} catch(NumberFormatException e) {
			//problemi di formato
			System.err.println(e.getMessage());
			return null;
		}		

	}

	/**
	 * Questo metodo inizializza correttamente gli eventuali valori mancanti.
	 * Se la velocitÓ massima non fosse presente, ma fosse presente la tipologia del tronco
	 * si inizializza la variabile a 50, 90, e 130 rispettivamente per la tipologia urbana, 
	 * extraurbana, e autostradale. Nel caso opposto si procede specularmente all'operazione.
	 * 
	 * @param linea_rilevazione String[]
	 * @return linea_rilevazioone String[]
	 */
	private String[] riempiValoriMancanti(String [] linea_rilevazione){

		//standardizzazione della tipologia di un tronco stradale
		linea_rilevazione[6] = convertiTipologia(linea_rilevazione[6]);

		//riempimento della velocitÓ a partire dalla tipologia del tronco stradale
		if (linea_rilevazione[5].equals("-1")) {
			switch (linea_rilevazione[6]) {
			case "urbana":
				linea_rilevazione[5] = "50";
				break;
			case "extraurbana":
				linea_rilevazione[5] = "90";
				break;
			case "autostrada":
				linea_rilevazione[5] = "130";
				break;
			case "non_disponibile":
			default:
				//nessuna operazione da eseguire in questi due casi
			}
		}

		//riempimento della tipologia del tronco stradale a partire dalla velocitÓ
		if (linea_rilevazione[6].equals("non_disponibile")) {

			if (!linea_rilevazione[5].equals("-1")) {
				switch (linea_rilevazione[5]) {
				case "50":
					linea_rilevazione[6] = "urbana";
					break;
				case "90":
					linea_rilevazione[6] = "extraurbana";
					break;
				case "130":
					linea_rilevazione[6] = "autostrada";
					break;
				default:
					//nessuna operazione da eseguire in questo caso
				}
			}
		}
		return linea_rilevazione;
	}

	/**
	 * Questo metodo si occupa di "standardizzare" la tipologia di tronco stradale,
	 * riducendo i casi possibili a 4 categorie: urbana, extraurbana, autostrada e non_disponibile. 
	 * 
	 * @param tipologia String
	 * @return tipologia String
	 */
	private String convertiTipologia(String tipologia) {

		switch (tipologia) {
		case "motorway":
		case "motorway_link":
		case "trunk":
		case "trunk_link":
		case "motorway_junction":
		case "services":
			tipologia = "autostrada";
			break;
		case "primary":
		case "primary_link":
		case "secondary":
		case "secondary_link":
			tipologia = "extraurbana";
			break;
		case "residential":
		case "living_street":
		case "tertiary":
		case "tertiary_link":
		case "service":
		case "pedestrian":
		case "bus_guideway":
		case "footway":
		case "sidewalk":
		case "cicleway":
		case "lane":
		case "opposite":
		case "crossing":
		case "bus_stop":
		case "stop":
			tipologia = "urbana";
			break;
		case "unclassified":
		case "track":
		case "escape":
		case "road":
		case "steps":
		case "bridleway":
		case "path":
		case "give_way":
		default: 
			tipologia = "non_disponibile";
		}
		return tipologia;
	}

	/**
	 * Questo metodo divide il tragitto composto dai dati sensore in tronchi in base 
	 * all'indirizzo stradale e associa ad ogni tronco una lista di DatiSensori con lo 
	 * stesso indirizzo. Il metodo restituisce la corrispondente lista di tronchi.
	 * 
	 * @param lista_datiSensori ArrayList di DatiSensori
	 * @return tronchi ArrayList di Tronco
	 */
	public ArrayList<Tronco> dividiInTronchi(ArrayList<DatiSensori> lista_datiSensori) {

		ArrayList<Tronco> tronchi = new ArrayList<>();
		ArrayList<DatiSensori> datiSensori_singolo_tronco = new ArrayList<>();
		String nome_corrente = "";
		String tipologia_corrente = "";
		int velocitÓ_corrente = -1;

		for (DatiSensori ds : lista_datiSensori) {

			/* Alla prima iterazione, viene estratto il nome, la tipologia
			 * e la velocitÓ massima della prima rilevazione e il dato sensore
			 * viene aggiunto alla lista ds_tronchi che contiene i datiSensori
			 * di uno stesso tronco
			 */

			if (datiSensori_singolo_tronco.size() == 0) {
				nome_corrente = ds.getNome();
				tipologia_corrente = ds.getTipologia();
				velocitÓ_corrente = ds.getVelocitÓMassima();
				datiSensori_singolo_tronco.add(ds);
			} else {

				/* Si controlla se il dato sensore successivo appartiene allo stesso
				 * tronco controllando che il nome della via sia uguale al nome della
				 * via del dato sensore precedente. 
				 */

				if (nome_corrente.equals(ds.getNome())) {
					datiSensori_singolo_tronco.add(ds);
				} else {

					/* Nel caso in cui il nome della via sia diverso, viene aggiunto alla
					 * lista dei tronchi il tronco con tipologia, nome, velocitÓ e dati
					 * sensori trovati fino al momento corrente
					 */

					datiSensori_singolo_tronco.add(ds);
					tronchi.add(new Tronco(tipologia_corrente, nome_corrente, velocitÓ_corrente,
							datiSensori_singolo_tronco));

					/* Vengono riaggiornate le variabili con i dati appartenenti al dato
					 * sensore del nuovo tronco trovato
					 */

					nome_corrente = ds.getNome();
					tipologia_corrente = ds.getTipologia();
					velocitÓ_corrente = ds.getVelocitÓMassima();
					datiSensori_singolo_tronco = new ArrayList<>();
					datiSensori_singolo_tronco.add(ds);
				}
			}
		}
		//Viene aggiunto l'ultimo tronco e i tronchi vengono associati al tragitto t
		tronchi.add(new Tronco(tipologia_corrente, nome_corrente, velocitÓ_corrente,
				datiSensori_singolo_tronco));
		return tronchi;
	}

	/**
	 * Questo metodo si occupa di elaborare i dati cumulativi per ogni tipologia di tronco.
	 * Preso in input una lista di tronchi, calcola i chilometri totali, i chilometri 
	 * totati percorsi in prudenza, ed effettua una media pesata delle velocitÓ medie per 
	 * ogni tipologia di tronco. Infine, il metodo restituisce il tragitto elaborato.  
	 * 
	 * @param tronchi ArrayList di Tronchi 
	 * @param targa String 
	 * @return tragitto Tragitto
	 */
	public Tragitto elaboraTragitto(ArrayList<Tronco> tronchi, String targa) {

		//inizializzazione
		double chilometriTotali = 0;
		double chilometriInPrudenza = 0;
		double velocitÓMediaUrbana = 0;
		double velocitÓMediaExtraurbana = 0;
		double velocitÓMediaAutostrada = 0;
		double chilometriUrbana = 0;
		double chilometriExtraurbana = 0;
		double chilometriAutostrada = 0;

		Tragitto tragitto = new Tragitto(tronchi, targa);

		//conversione stringa in data
		String str_date = tronchi.get(0).getDatiSensori().get(0).getData();
		DateFormat formatter; 
		Date date = null; 
		formatter = new SimpleDateFormat("dd-MM-yy"); //formato della data
		try {
			date = formatter.parse(str_date);
		} catch (ParseException e) {
			System.err.println("Attenzione: problemi nel formato della data nell'elaborazione del tragitto!");
			System.err.println(e.getMessage());
		}
		tragitto.setData(date);

		/*Calcolo delle statistiche relative ai tronchi di un tragitto*/
		for (Tronco tr : tronchi) {

			//calcolo dei chilometri totali percorsi
			chilometriTotali += tr.getChilometriTotaliTronco();

			//calcolo dei chilometri percorsi in prudenza
			chilometriInPrudenza += tr.getChilometriInPrudenzaTronco();

			//calcolo della velocitÓ media per tipologia di tronco
			switch (tr.getTipologia()) {
			case "urbana":
				velocitÓMediaUrbana += tr.getVelocitÓMediaTronco() * tr.getChilometriTotaliTronco();
				chilometriUrbana += tr.getChilometriTotaliTronco();
				break;
			case "extraurbana":
				velocitÓMediaExtraurbana += tr.getVelocitÓMediaTronco() * tr.getChilometriTotaliTronco();
				chilometriExtraurbana += tr.getChilometriTotaliTronco();
				break;
			case "autostrada":
				velocitÓMediaAutostrada += tr.getVelocitÓMediaTronco() * tr.getChilometriTotaliTronco();
				chilometriAutostrada += tr.getChilometriTotaliTronco();
				break;
			default:
				//non effettuare nessuna operazione
			}
		}

		/*Impostare le statistiche relative ad un tragitto, dopo aver calcolato le statistiche sui tronchi
		 * facenti parte al tragitto*/

		/*La velocitÓ media di una data tipologia viene settata a "-1"
		 * se non sono presenti tronchi di quella tipologia */

		//impostare la velocitÓ media di un tragitto per i tronchi con tipologia "urbana"
		if (chilometriUrbana <= 0) {
			tragitto.setVelocitÓMediaUrbana(-1);
		} else {
			tragitto.setVelocitÓMediaUrbana(
					Math.round((velocitÓMediaUrbana / chilometriUrbana) * 1000) / 1000.0);
		}

		//impostare la velocitÓ media di un tragitto per i tronchi con tipologia "extra-urbana"
		if (chilometriExtraurbana <= 0) {
			tragitto.setVelocitÓMediaExtraurbana(-1);
		} else {
			tragitto.setVelocitÓMediaExtraurbana(
					Math.round((velocitÓMediaExtraurbana / chilometriExtraurbana) * 1000) / 1000.0);
		}

		//impostare la velocitÓ media di un tragitto per i tronchi con tipologia "autostrada"
		if (chilometriAutostrada <= 0) {
			tragitto.setVelocitÓMediaAutostrada(-1);
		} else {
			tragitto.setVelocitÓMediaAutostrada(
					Math.round((velocitÓMediaAutostrada / chilometriAutostrada) * 1000) / 1000.0);
		}

		//impostare i chilometri totali percorsi per un tragitto
		tragitto.setChilometriTotaliPercorsi(Math.round(chilometriTotali * 1000) / 1000.0);

		//impostare i chilometri totali percorsi in prudenza per un tragitto
		tragitto.setChilometriPercorsiInPrudenza(Math.round(chilometriInPrudenza * 1000) / 1000.0);

		return tragitto;
	}

}
