package elaborazioneTragitto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;

/**
 * Questa classe si occupa di effettuare la chiamata a Nominatim per ottenere la pagina
 * contenente l'indirizzo, il place_id e la velocit� massima della strada corrispondente
 * alla coppia di coordinate.
 * Effettua una seconda chiamata ad OpenStreetMap per ottenere la tipologia di strada
 * a partire dal place_id.
 * Allo scopo di non sovraccaricare di richieste il server di OpenStreetMap, effettua
 * prima una ricerca in locale nella cache delle pagine richieste.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */

public class OSM {

	/**
	 * Questo metodo restituisce una stringa in formato json contenente le informazioni della strada
	 * fornite da OpenStreetMap. 
	 * Scansiona la cartella OsmCache per riutilizzare l'informazione, se presente. In caso contrario
	 * effettua una chiamata ad OpenStreetMap e salva la nuova informazione in questa cartella.
	 * 
	 * @param indirizzo String
	 * @param percorso_osmCache String
	 * @return info_strada String
	 * 
	 * @throws InterruptedException osmJSONrequest
	 */
	public String getStrada(String indirizzo, String percorso_osmCache) throws InterruptedException {

		String info_strada = "";
		String nomeFile_cache = percorso_osmCache + indirizzo.replaceAll("/|:|\\?", "_") + ".json";

		//Ricerca del file richiesto
		try {
			info_strada = leggiFileCache(nomeFile_cache);
		} catch (FileNotFoundException e) {
			//Se il file non viene trovato, si chiama Nominatim
			info_strada = osmJSONrequest(indirizzo,nomeFile_cache);
		} catch (IOException e) {
			//Nel caso in cui il file viene trovato ma � corrotto, si cancella il file e si chiama Nominatim
			File file = new File(nomeFile_cache); 
			file.delete();
			info_strada = osmJSONrequest(indirizzo,nomeFile_cache);
		}
		return info_strada;
	}

	/**
	 * Questo metodo si occupa di effettuare la chiamata al servizio Nominatim di reverse geocoding.
	 * Restituisce e salva su file la stringa json ottenuta contenente le informazioni sulla strada.
	 * 
	 * @param url_string String
	 * @param nomeFileCompleto String
	 * @return json_output String
	 * @throws InterruptedException TimeUnit.MINUTES.sleep
	 */
	private String osmJSONrequest(String url_string, String nomeFileCompleto) throws InterruptedException {

		URL url;
		String json_output = "";

		try {
			//connessione al sito con URL passato come primo parametro del metodo
			url = new URL(url_string);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			boolean is_error = conn.getResponseCode() != 200;

			if (is_error == true) {
				System.err.println("Nominatim HTTP error");
				System.err.println(new Timestamp(System.currentTimeMillis()));
				TimeUnit.MINUTES.sleep(5); //attendi 5 minuti prima di ri-inviare una richiesta
				System.err.println(new Timestamp(System.currentTimeMillis()));
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				json_output = json_output + br.readLine();
				conn.disconnect(); //chiusura della connessione

				//Scrittura della nuova stringa json nella cache
				scriviFile_cache(nomeFileCompleto,json_output); 
			}

		} catch(UnknownHostException e) {
			System.err.println("Nominatim non risponde (UnknownHostException)");
			TimeUnit.MINUTES.sleep(3); //attendi 3 minuti prima di ri-inviare una richiesta
			json_output = osmJSONrequest(url_string, nomeFileCompleto);
		} catch (IOException e) {
			System.err.println("Nominatim HTTP error (IOException)");
			System.err.println(new Timestamp(System.currentTimeMillis()));
			TimeUnit.MINUTES.sleep(5); //attendi 5 minuti prima di ri-inviare una richiesta
			System.err.println(new Timestamp(System.currentTimeMillis()));
			json_output = osmJSONrequest(url_string, nomeFileCompleto);
		} catch (RuntimeException e1) {
			System.err.println("Nominatim HTTP error (RuntimeException)");
			TimeUnit.MINUTES.sleep(3);
			json_output = osmJSONrequest(url_string, nomeFileCompleto);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return json_output;
	}

	/**
	 * Questo metodo aggiorna la cache scrivendo i file contententi le nuove informazioni sulla strada.
	 *  
	 * @param nomeFileCompleto String nome del file da scrivere nella cache
	 * @param informazioniStrada String
	 */
	private void scriviFile_cache(String nomeFileCompleto, String informazioniStrada) {

		FileWriter writer;

		try {
			writer = new FileWriter(nomeFileCompleto, true);
			writer.append(informazioniStrada);
			writer.flush();
			writer.close();			
		} catch (IOException e) {
			System.err.println("Attenzione: problemi di I/O nella scrittura del"
					+ "file contenente le nuove informazioni nella cache!");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Questo metodo restituisce la tipologia della strada associata ad un place_id
	 * ricevuto come parametro.
	 * Il metodo controlla se l'informazione � presente nella cartella cache.
	 * In caso non lo sia, effettua una chiamata ad OpenStreetMap e salva
	 * la nuova informazione in questa cartella.
	 * 
	 * @param id String
	 * @param percorso_osmCache String
	 * @return tipologia_strada String
	 * @throws InterruptedException TimeUnit.MINUTES.sleep
	 */
	public String getOutputTipologia(String id, String percorso_osmCache) throws InterruptedException {

		//url da interrogare per ottenere la tipologia di strada utilizzando Nominatim
		String url_per_ottenere_la_tipologia_di_strada ="http://nominatim.openstreetmap.org/details.php?place_id="+id;
		String nomeFileCompleto_cache = percorso_osmCache +
				url_per_ottenere_la_tipologia_di_strada.replaceAll("/|:|\\?", "_") + ".html";
		String html = "";

		//Ricerca del file richiesto
		try {
			html = leggiFileCache(nomeFileCompleto_cache);	
		} catch (FileNotFoundException e) {
			//Se il file non viene trovato, si chiama Nominatim
			try {			
				//connessione tramite Jsoup all'url definito in precedenza
				html = Jsoup.connect(url_per_ottenere_la_tipologia_di_strada).get().toString();
			} catch (IOException e1) {
				//tempo di attesa per non sovraccaricare il server di richieste HTML
				System.err.println("Troppe richieste html, attendere 3 minuti di attesa");
				TimeUnit.MINUTES.sleep(3);
				html = getOutputTipologia(id, percorso_osmCache);
			}
			scriviFile_cache(nomeFileCompleto_cache, html);
		} catch (IOException e) {
			//Nel caso in cui il file viene trovato ma � corrotto, si cancella il file e si chiama Nominatim
			File file = new File(nomeFileCompleto_cache); 
			file.delete();
			try {
				html = Jsoup.connect(url_per_ottenere_la_tipologia_di_strada).get().toString();
			} catch (IOException e1) {
				//tempo di attesa per non sovraccaricare il server di richieste HTML
				System.out.println("Troppe richieste html, attendere 3 minuti di attesa");
				TimeUnit.MINUTES.sleep(3);
				html = getOutputTipologia(id,percorso_osmCache);
			}catch (Exception eccezione) {
				System.err.println(e.getMessage());
			}

			//Scrittura della nuova stringa html nella cache
			scriviFile_cache(nomeFileCompleto_cache, html);
		}
		return ottieni_tipo_strada(html);
	}

	/**
	 * Questo metodo restituisce l'informazione richiesta presente nella cache come stringa.
	 * 
	 * @param nomeFileCompleto String
	 * @return output String
	 * @throws FileNotFoundException nel caso di file mancante
	 * @throws IOException problemi durante la lettura tramite FileReader
	 */
	private String leggiFileCache(String nomeFileCompleto) throws FileNotFoundException, IOException {
		
		BufferedReader br = new BufferedReader(new FileReader(nomeFileCompleto));
		String output = "";
		String linea = "";
		while ((linea = br.readLine()) != null) {
			output += linea + "\n";
		}
		br.close();
		return output;
	}

	/**
	 * Questo metodo estrae la tipologia della strada effettuando un parsing della pagina html ricevuta.
	 * 
	 * @param html String
	 * @return tipologia_strada String
	 */
	private String ottieni_tipo_strada(String html) {
		
		Scanner scanner = new Scanner(html);
		String tipologia_strada = "";
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if (line.indexOf("highway") > 0)
				tipologia_strada = line.substring(line.indexOf("highway:") +8, line.indexOf("</td>"));
		}
		scanner.close();
		return tipologia_strada;
	}
}
