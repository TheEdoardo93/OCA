package elaborazioneTragitto;

import org.json.*;

/**
 * Questa classe si occupa di estrarre dalle pagine ottenute dalla chiamata ad OpenStreetMap l'indirizzo, la citt�, la velocit� massima e la tipologia di strada
 * corrispondenti alle coppie di coordinate in input.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */
public class ReverseGeocoding {
		
		private OSM cache = new OSM();

		/**
		 * Questo metodo effettua l'operazione di reverse geocoding date le coordinate in input e 
		 * restituisce tutte le informazioni relative al tronco a cui corrispondono le coordinate.
		 * 
		 * @param latitudine String
		 * @param longitudine String
		 * @param percorso_osmCache String
		 * @return informazioni_tronco String[] 
		 * @throws InterruptedException getStrada(indirizzo, osmCache), estrai_info_JSON(strada, osmCache)
		 */
		public String[] getInformazioniTronco(String latitudine, String longitudine, String percorso_osmCache)
				throws InterruptedException {
						
			String indirizzo_strada = "http://nominatim.openstreetmap.org/reverse.php?lat=" +
					latitudine + "&lon=" + longitudine + "&format=json&addressdetails=1&extratags=1";
			
			String strada = cache.getStrada(indirizzo_strada, percorso_osmCache);
			String[] informazioni_tronco = estrai_info_JSON(strada, percorso_osmCache);
			
			return informazioni_tronco;
		}

		/**
		 * Questo metodo estrae le informazioni di interesse dalla stringa JSON in input e
		 * restituisce un array di stringhe contenente le informazioni.
		 * 
		 * @param strada String
		 * @param percorso_osmCache String
		 * @return informazioni_strada String[]
		 * @throws InterruptedException getOutputTipologia(id, osmCache)
		 */
		private String[] estrai_info_JSON(String strada, String percorso_osmCache)
				throws InterruptedException {

			String[] informazioni_strada = new String[4];
			JSONObject stradaJSON = new JSONObject();
			
			try {
				stradaJSON = new JSONObject(strada); 
				/*
				 * si estrae il place_id dal file json per ottenere la tipologia di strada
				 * nel caso in cui il tag "place_id" non � presente, la tipologia di strada
				 * � settata a non disponibile
				 */
				
				if (stradaJSON.optString("error","").equals("error")) {
					return null;
				}
				
				String id = stradaJSON.optString("place_id", "");
				String tipo_strada;
				
				if (id.isEmpty()) {
					tipo_strada = "non_disponibile";
				} else {
					tipo_strada = cache.getOutputTipologia(id, percorso_osmCache);
				}

				//se un tag non � presente, viene settato di default a stringa vuota
				String citt� = stradaJSON.optJSONObject("address").optString("town", "");
				String indirizzo = stradaJSON.optJSONObject("address").optString("road", "");
				String max_speed = stradaJSON.optJSONObject("extratags").optString("maxspeed", "");

				informazioni_strada[0] = indirizzo;
				informazioni_strada[1] = citt�;
				informazioni_strada[2] = max_speed;
				informazioni_strada[3] = tipo_strada;

				return informazioni_strada;

			} catch (JSONException e) { 
				// caso in cui output non � in formato JSON
				System.err.println("Attenzione: il file di output non � in formato .json!");
				System.err.println(e.getMessage());
				return null;
			}
		}
	}