package util;

/**Questa classe descrive una polizza con gli attributi che la caratterizzano:
 * l'identificativo, la data di stipula del contratto e la targa del veicolo
 * a cui � associata la polizza.
 * Questa classe dispone di metodi get*() che permettono il reperimento dell'informazione
 * dell'assicurato voluta.
 * */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */

public class Polizza {
	
	/*Attributi della classe*/
	String idPolizza;
	String dataContratto;
	String targaVeicolo;
	
	/**Costruttore della classe parametrico (identificativo, data del contratto e targa del veicolo
	 * associata ad una polizza).
	 * 
	 * @param idPolizza String
	 * @param dataContratto String
	 * @param targaVeicolo String
	 * */
	public Polizza(String idPolizza, String dataContratto, String targaVeicolo) {
		super();
		this.idPolizza = idPolizza;
		this.dataContratto = dataContratto;
		this.targaVeicolo = targaVeicolo;
	}
	
	/*Metodi get()*/
	public String getIdPolizza() {
		return idPolizza;
	}

	public String getDataContratto() {
		return dataContratto;
	}

	public String getTargaVeicolo() {
		return targaVeicolo;
	}
}
