package util;
import java.util.ArrayList;
import java.util.Date;

/**
 * Questa classe racchiude le informazioni relative ad un tragitto che si compone dell'insieme
 * dei tronchi e delle statistiche calcolate.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 *
 */
public class Tragitto {
	
	/*Attributi della classe che descrivono un Tragitto percorso da un veicolo*/
	ArrayList<Tronco> tronchi;
	double velocitÓMediaUrbana;
	double velocitÓMediaExtraurbana;
	double velocitÓMediaAutostrada;
	double chilometriTotaliPercorsi;
	double chilometriPercorsiInPrudenza;
	Date data;
	String targa;
	
	
	/**Costruttore della classe
	 * @param targa String 
	 * @param tronchi ArrayList di Dati Sensore
	 */
	public Tragitto(ArrayList<Tronco> tronchi, String targa) {
		super();
		this.tronchi= tronchi;
		this.targa=targa;
	}

	
	/**
	 * @return tronchi ArrayList di Tronchi 
	 */
	public ArrayList<Tronco> getTronchi() {
		return tronchi;
	}

	/**
	 * @return velocitÓMediaUrbana double 
	 */
	public double getVelocitÓMediaUrbana() {
		return velocitÓMediaUrbana;
	}

	/**
	 * @return velocitÓMediaExtraurbana double
	 */
	public double getVelocitÓMediaExtraurbana() {
		return velocitÓMediaExtraurbana;
	}

	/**
	 * @return velocitÓMediaAutostrada double
	 */
	public double getVelocitÓMediaAutostrada() {
		return velocitÓMediaAutostrada;
	}

	/**
	 * @return chilometriTotaliPercorsi double
	 */
	public double getChilometriTotaliPercorsi() {
		return chilometriTotaliPercorsi;
	}

	/**
	 * @return chilometriPercorsiInPrudenza double 
	 */
	public double getChilometriPercorsiInPrudenza() {
		return chilometriPercorsiInPrudenza;
	}
	
	/**
	 * @return data Date 
	 */
	public Date getData() {
		return data;
	}
	
	/**
	 * @return targa String
	 */
	public String getTarga() {
		return targa;
	}


	/**
	 * @param tronchi ArrayList di tronchi 
	 */
	public void setTronchi(ArrayList<Tronco> tronchi) {
		this.tronchi = tronchi;
	}

	/**
	 * @param velocitÓMediaUrbana double 
	 */
	public void setVelocitÓMediaUrbana(double velocitÓMediaUrbana) {
		this.velocitÓMediaUrbana = velocitÓMediaUrbana;
	}

	/**
	 * @param velocitÓMediaExtraurbana double 
	 */
	public void setVelocitÓMediaExtraurbana(double velocitÓMediaExtraurbana) {
		this.velocitÓMediaExtraurbana = velocitÓMediaExtraurbana;
	}

	/**
	 * @param velocitÓMediaAutostrada double
	 */
	public void setVelocitÓMediaAutostrada(double velocitÓMediaAutostrada) {
		this.velocitÓMediaAutostrada = velocitÓMediaAutostrada;
	}

	/**
	 * @param chilometriTotaliPercorsi double 
	 */
	public void setChilometriTotaliPercorsi(double chilometriTotaliPercorsi) {
		this.chilometriTotaliPercorsi = chilometriTotaliPercorsi;
	}

	/**
	 * @param chilometriPercorsiInPrudenza double 
	 */
	public void setChilometriPercorsiInPrudenza(double chilometriPercorsiInPrudenza) {
		this.chilometriPercorsiInPrudenza = chilometriPercorsiInPrudenza;
	}

	/**
	 * @param data String
	 */
	public void setData(Date data) {
		this.data = data;
	}
	
	/**
	 * @param targa String 
	 */
	public void setTarga(String targa) {
		this.targa = targa;
	}

	@Override
	public String toString() {
		return "Tragitto [velocitÓMediaUrbana=" + velocitÓMediaUrbana + ", velocitÓMediaExtraurbana="
				+ velocitÓMediaExtraurbana + ", velocitÓMediaAutostrada=" + velocitÓMediaAutostrada
				+ ", chilometriTotaliPercorsi=" + chilometriTotaliPercorsi + " Dati Tronco=" + getTronchi() + "]";
	}

	
}
