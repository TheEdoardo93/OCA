package util;

/**Questa classe descrive un assicurato con gli attributi che lo caratterizzano:
 * l'identificativo, il nome, il cognome, la data di nascita, il livello di rischio,
 * il comune di residenza, la provincia, la regione, il cap (codice avviamento postale)
 * ed il telefono.
 * Questa classe dispone di metodi get*() che permettono il reperimento dell'informazione
 * dell'assicurato voluta.
 * */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */

public class Assicurato {
	
	/*Attributi della classe*/
	int idAssicurato;
	String nome;
	String cognome;
	String dataNascita;
	int livelloRischio;
	String comuneResidenza;
	String provincia;
	String regione;
	String cap;
	String telefono;
	
	
	/**Costruttore della classe parametrico (identificativo, nome, cognome, data di nascita, livello di rischio,
	 * comune di residenza, provincia, regione, cap, telefono dell'assicurato).
	 * 
	 * @param idAssicurato String
	 * @param cognome String
	 * @param nome String
	 * @param dataNascita String
	 * @param livelloRischio int
	 * @param comuneResidenza String
	 * @param provincia String
	 * @param regione String
	 * @param cap String
	 * @param telefono String
	 * */
	public Assicurato(int idAssicurato, String cognome, String nome, String dataNascita, int livelloRischio,
			String comuneResidenza, String provincia, String regione, String cap, String telefono) {
		super();
		this.idAssicurato = idAssicurato;
		this.nome = nome;
		this.cognome = cognome;
		this.dataNascita = dataNascita;
		this.livelloRischio = livelloRischio;
		this.comuneResidenza = comuneResidenza;
		this.provincia = provincia;
		this.regione = regione;
		this.cap = cap;
		this.telefono = telefono;
	}
	
	/**Costruttore della classe parametrico (identificativo, nome e cognome dell'assicurato).
	 * 
	 * @param id_assicurato int
	 * @param cognome String
	 * @param nome String
	 * */
	public Assicurato(int id_assicurato, String cognome, String nome) {
		super();
		this.idAssicurato = id_assicurato;
		this.nome = nome;
		this.cognome = cognome;
	}
	
	/**Costruttore della classe parametrico (identificativo dell'assicurato).
	 * 
	 * @param idAssicurato int
	 * */
	public Assicurato(int idAssicurato) {
		this.idAssicurato = idAssicurato;
	}
	
	/*Metodi get()*/
	
	public int getIdAssicurato() {
		return idAssicurato;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public String getDataNascita() {
		return dataNascita;
	}

	public int getLivelloRischio() {
		return livelloRischio;
	}
	
	public String getComuneResidenza() {
		return comuneResidenza;
	}
	
	public String getProvincia() {
		return provincia;
	}
	
	public String getRegione() {
		return regione;
	}
	
	public String getCap() {
		return cap;
	}
	
	public String getTelefono() {
		return telefono;
	}
}
