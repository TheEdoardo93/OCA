package util;

/**
 * Questa classe rappresenta le informazioni legate ad una rilevazione eseguita nello
 * stesso momento temporale a partire dai sensori posizionati nella scatola nera.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 *
 */
public class DatiSensori {
	
	/*Attributi della classe che descrivono un oggetto DatiSensori*/
	private String nome;
	private double velocitÓ;
	private String tipologia;
	private double latitudine;
	private double longitudine;
	private int velocitÓMassima;
	private String data;

	/**
	 * Costruttore della classe DatiSensori.
	 * 
	 * La stringa data Ŕ nel formato: gg-MM-aa.
	 * La latitudine e la longitudine sono espressi in gradi decimali.
	 *  
	 * @param data String
	 * @param latitudine double
	 * @param longitudine double
	 * @param velocitÓ double
	 * @param nome String
	 * @param velocitÓMassima int
	 * @param tipologia String
	 */
	public DatiSensori(String data, double latitudine, double longitudine, double velocitÓ, String nome,
			int velocitÓMassima, String tipologia) {
		super();
		this.data = data;
		this.nome = nome;
		this.velocitÓ = velocitÓ;
		this.tipologia = tipologia;
		this.latitudine = latitudine;
		this.longitudine = longitudine;
		this.velocitÓMassima = velocitÓMassima;
	}

	/**
	 * @return nome String
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome String
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return velocitÓ double
	 */
	public double getVelocitÓ() {
		return velocitÓ;
	}

	/**
	 * @param velocitÓ double
	 */
	public void setVelocitÓ(double velocitÓ) {
		this.velocitÓ = velocitÓ;
	}

	/**
	 * @return tipologia String
	 */
	public String getTipologia() {
		return tipologia;
	}

	/**
	 * @param tipologia String
	 */
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	/**
	 * @return latitudine double
	 */
	public double getLatitudine() {
		return latitudine;
	}

	/**
	 * @param latitudine double
	 */
	public void setLatitudine(double latitudine) {
		this.latitudine = latitudine;
	}

	/**
	 * @return longitudine double
	 */
	public double getLongitudine() {
		return longitudine;
	}

	/**
	 * @param longitudine double
	 */
	public void setLongitudine(double longitudine) {
		this.longitudine = longitudine;
	}

	/**
	 * @return velocitÓMassima int
	 */
	public int getVelocitÓMassima() {
		return velocitÓMassima;
	}

	/**
	 * @param velocitÓMassima int
	 */
	public void setVelocitÓMassima(int velocitÓMassima) {
		this.velocitÓMassima = velocitÓMassima;
	}
	
	/**
	 * @return data String
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data String
	 */
	public void setData(String data) {
		this.data = data;
	}
	
		
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DatiSensori [nome=" + nome + ", velocitÓ=" + velocitÓ + ", tipologia=" + tipologia + ", latitudine="
				+ latitudine + ", longitudine=" + longitudine + ", velocitÓMassima=" + velocitÓMassima + "]\n";
	}


	
	
}
