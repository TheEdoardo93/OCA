package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

/** Questa classe si occupa della gestione delle connessioni con
 * i database OCB e BDA.
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 */
public class DBConnection {
    private String HOST = "";
    private String DB = "";
    private String USER = "";
    private String PWD = "";
    private String VERIFYSERVERCERTIFICATE = "";
    private String USESSL = "";
    private static Connection conn = null;

	private static String fileOcbProperties = "configOcb.properties";
	private static String fileBdaProperties = "configBda.properties";
	
	 /** Questo metodo restituisce le informazioni utili per la connessione 
	  * al database 
	 * @param file String nome del file contenente le proprietÓ
	 * @return ArrayList di String
	 * @throws IOException
	 */
	public synchronized ArrayList<String> getProperties(String file) throws IOException {
	        ArrayList<String> list = new ArrayList<String>();
	        String result = "";
	        Object obj = null;
	        Properties props = new Properties();
	        props.load(new FileInputStream("FileConfigurazione/"+ file));
	        Enumeration<Object> e = props.keys();
	        while (e.hasMoreElements()) {
	            obj = e.nextElement();
	            result = props.getProperty(obj.toString());
	            list.add(result);
	        }
	        return list;
	    }
	
	/** Questo metodo restituisce la connessione del database BDA
	 * @return connessione BDA
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 */
	public Connection getBdaConnection() throws ClassNotFoundException, SQLException, IOException {
        ArrayList<String> list = getProperties(fileBdaProperties);
        String[] keys = (String[]) list.toArray(new String[list.size()]);
        USER = keys[0];
        PWD = keys[1];
        HOST = keys[3];
        DB = keys[5];
        VERIFYSERVERCERTIFICATE = keys[2];
        USESSL = keys[4];
        
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://" + HOST + "/" + DB + "?user=" + USER + "&password=" + PWD
        		+ "&verifyServerCertificate=" + VERIFYSERVERCERTIFICATE + "&useSSL=" + USESSL);

        return conn;
    }
	
	/** Questo metodo restituisce la connessione del database OCB
	 * @return connessione OCB
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 */
	public Connection getOcbConnection() throws ClassNotFoundException, SQLException, IOException {
        ArrayList<String> list = getProperties(fileOcbProperties);
        String[] keys = (String[]) list.toArray(new String[list.size()]);
        USER = keys[0];
        PWD = keys[1];
        HOST = keys[3];
        DB = keys[5];
        VERIFYSERVERCERTIFICATE = keys[2];
        USESSL = keys[4];
        
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://" + HOST + "/" + DB + "?user=" + USER + "&password=" + PWD
        		+ "&verifyServerCertificate=" + VERIFYSERVERCERTIFICATE + "&useSSL=" + USESSL);

        return conn;
    }	
}
