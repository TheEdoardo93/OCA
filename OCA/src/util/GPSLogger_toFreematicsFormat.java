package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Questa classe converte il file in formato .gpx ottenuto tramite l'applicazione GPSLogger
 * nel formato Freematics.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */

public class GPSLogger_toFreematicsFormat {

	/**
	 * Il main si occupa di convertire i dati forniti dall'applicazione GPSLogger nel 
	 * formato Freematics, ai fini di simulare il comportamento di un automobilista.
	 * 
	 * @param args String []
	 */
	public static void main(String[] args) {
		
		/*Cartella contenente file in formato .gpx che indicano tragitti rilevati
		 *tramite l'applicazione GPSLogger percorsi da veicoli*/
		File dir = new File("gpx/");
		
		for (File file : dir.listFiles()) {
			String gpx_path = file.getPath();
			
			try {
				BufferedReader buffer = new BufferedReader(new FileReader(gpx_path));
				buffer.readLine(); //eliminazione dell'header del documento in formato .gpx

				String doc_date = buffer.readLine().replaceAll("T", "_"); 
				doc_date= doc_date.replaceAll(":|Z|-", "");

				String document_date = doc_date.substring(doc_date.indexOf("<time>") + 6,
						doc_date.indexOf("<time>") + 16) + doc_date.substring(doc_date.indexOf("<time>") + 16,
						doc_date.indexOf("</time>"));

				document_date = document_date.substring(6,8) + document_date.substring(4,6) +
						document_date.substring(0,4) + "_"+ document_date.substring(9);
				String file_name="Freematics/AA111AA_"+document_date+".csv";

				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file_name, true));

				fileWriter.append("#"+ document_date + ",10D,00\n");

				String line = "";
				
				while ((line = buffer.readLine()) != null) {
					String lat = "";
					String lon = "";
					String timestamp = "";
					String velocit� = "";
					String data = "";
					String time = "";

					lat = line.substring(line.indexOf("lat=") + 5, line.indexOf("lon=") - 2);
					lon = line.substring(line.indexOf("lon=") + 5, line.indexOf("\">"));
					timestamp = line.substring(line.indexOf("<time>") + 6, line.indexOf("</time>")).replaceAll("T|:|Z", "");
					velocit� = line.substring(line.indexOf("<speed>") + 7, line.indexOf("</speed>"));

					String latitudine = lat.substring(0,lat.indexOf("."));
					latitudine += setString(lat.substring(lat.indexOf(".") + 1));
					latitudine = latitudine.replaceAll("\\.", "");

					String longitudine = lon.substring(0,lon.indexOf("."));
					longitudine += setString(lon.substring(lon.indexOf(".") + 1));
					longitudine = longitudine.replaceAll("\\.", "");

					data = timestamp.substring(8,10) + timestamp.substring(5,7) + timestamp.substring(2,4);
					time = timestamp.substring(10) + "00";

					double vel = Double.parseDouble(velocit�) * 3.6;
					velocit� = (Double.toString(Math.round(vel * 100) / 100.0));

					fileWriter.append("0,ACC,-913,18,528" + "\n");
					fileWriter.append("0,GYR,-9,-22,17" + "\n");
					fileWriter.append("111,104,0" + "\n");
					fileWriter.append("0,ACC,-922,6,548" + "\n");
					fileWriter.append("0,GYR,1,-27,17" + "\n");
					fileWriter.append("30,111,16" + "\n");
					fileWriter.append("0,ACC,-922,-9,513" + "\n");
					fileWriter.append("0,GYR,-4,-36,24" + "\n");
					fileWriter.append("29,10F,32" + "\n");
					fileWriter.append("0,ACC,-927,14,573" + "\n");
					fileWriter.append("0,GYR,-14,-11,16" + "\n");
					fileWriter.append("8,TIM,"+ time + "\n");
					fileWriter.append("0,DTE,"+ data + "\n");
					fileWriter.append("0,LAT,"+ latitudine + "\n");
					fileWriter.append("0,LNG,"+ longitudine + "\n");
					fileWriter.append("0,ALT,16200" + "\n");
					fileWriter.append("0,SPD,"+ velocit� + "\n");
					fileWriter.append("0,SAT,5" + "\n");

					fileWriter.flush();
				}
				buffer.close();
				fileWriter.close();
				
				System.out.println("Il file " + gpx_path + " in formato .gpx � stato elaborato!");

			} catch (IOException e) {
				System.err.println("Attenzione: il file " + gpx_path + " in formato .gpx non � stato trovato!");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Il metodo si occupa di uniformare le cifre decimali delle coordinate
	 * ricevute in ingresso. Se ha pi� di 6 cifre decimali, viene troncata a 
	 * 6 cifre. Se ne ha meno, vengono aggiunti degli zeri in fondo fino a 6 cifre.
	 * 
	 * @param coordinata String
	 * @return coordinata String
	 */
	private static String setString(String coordinata) {

		int lunghezza = coordinata.length();
		
		if (lunghezza >= 6) {
			return coordinata.substring(0, 6);
		} else {
			while(lunghezza < 6) {
				coordinata = coordinata + "0";
				lunghezza++;
			}
		}
		return coordinata;
	}
}


