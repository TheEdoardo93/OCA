package util;
import java.util.ArrayList;

/**
 * Questa classe racchiude le informazioni relative ad un tronco.
 * Un tronco Ŕ inteso come una via percorsa dal veicolo.
 * I tronchi si distinguono dal nome della via.
 */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 * 
 *
 */
public class Tronco {
	
	/*Attributi della classe che descrivono un Tronco*/
	double velocitÓMediaTronco;
	double chilometriTotaliTronco;
	double chilometriInPrudenzaTronco;
	String tipologia;
	String nome_tronco;
	int velocitÓMassima;
	ArrayList<DatiSensori> datiSensori;

	/**
	 * Costrutture della classe
	 * @param tipologia String
	 * @param nome String
	 * @param velocitÓMassima int
	 * @param ds ArrayList di dati sensore
	 */
	public Tronco(String tipologia, String nome, int velocitÓMassima, ArrayList<DatiSensori> ds) {
		super();
		this.tipologia = tipologia;
		this.velocitÓMassima = velocitÓMassima;
		this.datiSensori = ds;
		this.nome_tronco = nome;
	}


	@Override
	public String toString() {
		return "Tronco [tipologia=" + tipologia + ", velocitÓMassima=" + velocitÓMassima + ", nome della via="
				+ nome_tronco + ", velocitÓMedia=" + velocitÓMediaTronco + ", chilometriTotali="
				+ chilometriTotaliTronco + ", chilometriPrudenza=" + chilometriInPrudenzaTronco  +
				"datiSensori="+ datiSensori.toString()+ "]\n";
	}

	/**
	 * @return velocitÓMediaTronco double
	 */
	public double getVelocitÓMediaTronco() {
		return velocitÓMediaTronco;
	}

	/**
	 * @param velocitÓMediaTronco double
	 */
	public void setVelocitÓMediaTronco(double velocitÓMediaTronco) {
		this.velocitÓMediaTronco = velocitÓMediaTronco;
	}

	/**
	 * @return chilometriTotaliTronco double
	 */
	public double getChilometriTotaliTronco() {
		return chilometriTotaliTronco;
	}

	/**
	 * @param chilometriTotaliTronco double
	 */
	public void setChilometriTotaliTronco(double chilometriTotaliTronco) {
		this.chilometriTotaliTronco = chilometriTotaliTronco;
	}

	/**
	 * @return chilometriInPrudenzaTronco double
	 */
	public double getChilometriInPrudenzaTronco() {
		return chilometriInPrudenzaTronco;
	}

	/**
	 * @param chilometriInPrudenzaTronco double
	 */
	public void setChilometriInPrudenzaTronco(double chilometriInPrudenzaTronco) {
		this.chilometriInPrudenzaTronco = chilometriInPrudenzaTronco; 
	}

	/**
	 * @return tipologia String
	 */
	public String getTipologia() {
		return tipologia;
	}

	/**
	 * @param tipologia String
	 */
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	/**
	 * @return nome_tronco String
	 */
	public String getNome_tronco() {
		return nome_tronco;
	}

	/**
	 * @param nome_tronco String
	 */
	public void setNome_tronco(String nome_tronco) {
		this.nome_tronco = nome_tronco; 
	}

	/**
	 * @return velocitÓMassima int
	 */
	public int getVelocitÓMassima() {
		return velocitÓMassima;
	}

	/**
	 * @param velocitÓMassima int
	 */
	public void setVelocitÓMassima(int velocitÓMassima) {
		this.velocitÓMassima = velocitÓMassima;
	}

	/**
	 * @return datiSensori ArrayList di dati sensore
	 */
	public ArrayList<DatiSensori> getDatiSensori() {
		return datiSensori;
	}

	/**
	 * @param datiSensori ArrayList di dati sensore
	 */
	public void setDatiSensori(ArrayList<DatiSensori> datiSensori) {
		this.datiSensori = datiSensori;
	}

}
