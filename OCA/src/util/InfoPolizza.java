package util;

/**Questa classe descrive un'infopolizza con gli attributi che la caratterizzano:
 * il nome ed il cognome dell'assicurato intestatario della polizza associata, la targa del veicolo
 * a cui Ŕ associata la polizza, il numero di chilometri totali percorsi dal veicolo,
 * il numero di chilometri totali percorsi in prudenza dal veicolo, la velocitÓ media tenuta
 * dal veicolo su strade urbane, la velocitÓ media tenuta dal veicolo su strade extraurbana, 
 * la velocitÓ media tenuta dal veicolo su autostrade.
 * Questa classe dispone di metodi get*() che permettono il reperimento dell'informazione
 * dell'assicurato voluta.
 * */

/**
 * @author Edoardo Casiraghi
 * @author Marco Palmiero
 * @author Silvia Terragni
 */

public class InfoPolizza {
	
	/*Attributi della classe*/
	String cognomeNome;
	String targa;
	String chilometriTotali;
	String chilometriTotaliPrudenza;
	String velocitÓMediaUrbana;
	String velocitÓMediaExtraUrbana;
	String velocitÓMediaAutostrada;
	
	/**Costruttore della classe parametrico (cognome e nome dell'assicurato, targa del veicolo, chilometri totali,
	 * chilometri totali in prudenza, velocitÓ media su urbana, velocitÓ media su extraurbana,
	 * velocitÓ media su autostrada dell'infopolizza associata alla polizza dell'assicurato).
	 * 
	 * @param cognomeNome String
	 * @param targa String
	 * @param chilometriTotali String
	 * @param chilometriTotaliPrudenza String
	 * @param velocitÓMediaUrbana String
	 * @param velocitÓMediaExtraurbana String
	 * @param velocitÓMediaAutostrada String
	 */
	public InfoPolizza(String cognomeNome, String targa, String chilometriTotali, String chilometriTotaliPrudenza,
			String velocitÓMediaUrbana, String velocitÓMediaExtraurbana, String velocitÓMediaAutostrada) {
		super();
		this.cognomeNome = cognomeNome;
		this.targa = targa;
		this.chilometriTotali = chilometriTotali;
		this.chilometriTotaliPrudenza = chilometriTotaliPrudenza;
		this.velocitÓMediaUrbana = velocitÓMediaUrbana;
		this.velocitÓMediaExtraUrbana = velocitÓMediaExtraurbana;
		this.velocitÓMediaAutostrada = velocitÓMediaAutostrada;
	}
	
	/*Metodi get()*/
	
	public String getCognomeNome() {
		return cognomeNome;
	}

	public String getTarga() {
		return targa;
	}

	public String getChilometriTotali() {
		return chilometriTotali;
	}

	public String getChilometriTotaliPrudenza() {
		return chilometriTotaliPrudenza;
	}

	public String getVelocitÓMediaUrbana() {
		return velocitÓMediaUrbana;
	}

	public String getVelocitÓMediaExtraUrbana() {
		return velocitÓMediaExtraUrbana;
	}

	public String getVelocitÓMediaAutostrada() {
		return velocitÓMediaAutostrada;
	}
}
