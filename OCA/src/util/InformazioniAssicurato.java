package util;


/** Questa classe descrive le informazioni associate a un assicurato. 
 * @author Casiraghi Edoardo
 * @author Palmiero Marco
 * @author Terragni Silvia
 */
public class InformazioniAssicurato {
	
	private int idAssicurato;
	private String dataNascita;
	private int et�;
	private String comuneResidenza;
	private double velocit�MediaSuUrbana;
	private double velocit�MediaSuExtraurbana;
	private double velocit�MediaSuAutostrada;
	private double chilometriTotali;
	private double livelloPrudenza;
	private int rischioProvincia;
	private String siglaProvincia;
	private int numeroDenunce;
	private int rischio;

	
	
	/** Costruttore
	 * @param idAssicurato
	 */
	public InformazioniAssicurato(int idAssicurato) {
		super();
		this.velocit�MediaSuUrbana = 0;
		this.velocit�MediaSuExtraurbana = 0;
		this.velocit�MediaSuAutostrada = 0;
		this.livelloPrudenza = 0;
		this.chilometriTotali = 0;
		this.dataNascita = "";
		this.et� = 0;
		this.rischioProvincia = 0;
		this.siglaProvincia = "";
		this.comuneResidenza = "";
		this.numeroDenunce = 0;
		this.idAssicurato = idAssicurato;
	}
	
	/**
	 * @return
	 */
	public int getRischio() {
		return rischio;
	}

	/**
	 * @param rischio
	 */
	public void setRischio(int rischio) {
		this.rischio = rischio;
	}

	/**
	 * @return
	 */
	public String getSiglaProvincia() {
		return siglaProvincia;
	}
	
	/**
	 * @param siglaProvincia
	 */
	public void setSiglaProvincia(String siglaProvincia) {
		this.siglaProvincia = siglaProvincia;
	}

	/**
	 * @return
	 */
	public int getRischioProvincia() {
		return rischioProvincia;
	}

	/**
	 * @return
	 */
	public double getLivelloPrudenza() {
		return livelloPrudenza;
	}

	/**
	 * @return
	 */
	public double getChilometriTotali() {
		return chilometriTotali;
	}

	/**
	 * @return
	 */
	public String getDataNascita() {
		return dataNascita;
	}

	
	/**
	 * @return
	 */
	public int getIdAssicurato() {
		return idAssicurato;
	}

	/**
	 * @return
	 */
	public double getVelocit�MediaSuUrbana() {
		return velocit�MediaSuUrbana;
	}

	/**
	 * @return
	 */
	public double getVelocit�MediaSuExtraurbana() {
		return velocit�MediaSuExtraurbana;
	}

	/**
	 * @return
	 */
	public double getVelocit�MediaSuAutostrada() {
		return velocit�MediaSuAutostrada;
	}

	/**
	 * @return
	 */
	public int getEt�() {
		return et�;
	}

	/**
	 * @return
	 */
	public String getComuneResidenza() {
		return comuneResidenza;
	}

	/**
	 * @return
	 */
	public int getNumeroDenunce() {
		return numeroDenunce;
	}


	/**
	 * @param velocit�MediaSuUrbana
	 */
	public void setVelocit�MediaSuUrbana(double velocit�MediaSuUrbana) {
		this.velocit�MediaSuUrbana = velocit�MediaSuUrbana;
	}

	/**
	 * @param velocit�MediaSuExtraurbana
	 */
	public void setVelocit�MediaSuExtraurbana(double velocit�MediaSuExtraurbana) {
		this.velocit�MediaSuExtraurbana = velocit�MediaSuExtraurbana;
	}

	/**
	 * @param velocit�MediaSuAutostrada
	 */
	public void setVelocit�MediaSuAutostrada(double velocit�MediaSuAutostrada) {
		this.velocit�MediaSuAutostrada = velocit�MediaSuAutostrada;
	}

	/**
	 * @param livelloPrudenza
	 */
	public void setLivelloPrudenza(double livelloPrudenza) {
		this.livelloPrudenza = livelloPrudenza;
	}

	/**
	 * @param chilometriTotali
	 */
	public void setChilometriTotali(double chilometriTotali) {
		this.chilometriTotali = chilometriTotali;
	}

	/**
	 * @param dataNascita
	 */
	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}

	/**
	 * @param et�
	 */
	public void setEt�(int et�) {
		this.et� = et�;
	}

	/**
	 * @param rischioProvincia
	 */
	public void setRischioProvincia(int rischioProvincia) {
		this.rischioProvincia = rischioProvincia;
	}

	/**
	 * @param comuneResidenza
	 */
	public void setComuneResidenza(String comuneResidenza) {
		this.comuneResidenza = comuneResidenza;
	}

	/**
	 * @param numeroDenunce
	 */
	public void setNumeroDenunce(int numeroDenunce) {
		this.numeroDenunce = numeroDenunce;
	}

	/**
	 * @param idAssicurato
	 */
	public void setIdAssicurato(int idAssicurato) {
		this.idAssicurato = idAssicurato;
	}

}
